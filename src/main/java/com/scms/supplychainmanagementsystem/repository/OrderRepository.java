package com.scms.supplychainmanagementsystem.repository;

import com.scms.supplychainmanagementsystem.dto.OrderResponse;
import com.scms.supplychainmanagementsystem.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query(value = "select sum(d.quantity * d.currentPrice) from Order o " +
            " inner join OrderDetails d on o.orderId = d.order.orderId and o = :order")
    Double sumTotalAmount(Order order);

    @Query(value = "select o.orderId as orderId, o.createdDate as createdDate" +
            " , o.orderCode as orderCode, o.orderStatus.status as status, o.orderStatus.orderStatusID as orderStatusId" +
            " , o.contactDelivery.contactName as contactName, o.warehouse.warehouseName as warehouseName" +
            " , o.contactDelivery.customer.customerName as customerName " +
            " , o.contactDelivery.phone as phone, o.note as note " +
            " from Order o where o.warehouse.warehouseID =:warehouseId " +
            " and (:orderCode is null or o.orderCode like %:orderCode%) " +
            " and (:customerName is null or o.contactDelivery.customer.customerName like %:customerName%) " +
            " and (:orderStatusId is null or o.orderStatus.orderStatusID = :orderStatusId) " +
            " group by o.orderId " +
            " order by o.orderId desc")
    Page<OrderResponse> filterInOneWarehouse(@Param("orderCode") String orderCode, @Param("customerName") String customerName,
                                             @Param("orderStatusId") Long orderStatusId, @Param("warehouseId") Long warehouseId,
                                             Pageable pageable);

    @Query(value = "select o.orderId as orderId, o.createdDate as createdDate" +
            " , o.orderCode as orderCode, o.orderStatus.status as status, o.orderStatus.orderStatusID as orderStatusId" +
            " , o.contactDelivery.contactName as contactName, o.warehouse.warehouseName as warehouseName" +
            " , o.contactDelivery.customer.customerName as customerName " +
            " , o.contactDelivery.phone as phone, o.note as note " +
            " from Order o where (:warehouseId is null or o.warehouse.warehouseID = :warehouseId) " +
            " and (:orderCode is null or o.orderCode like %:orderCode%) " +
            " and (:customerName is null or o.contactDelivery.customer.customerName like %:customerName%) " +
            " and (:orderStatusId is null or o.orderStatus.orderStatusID = :orderStatusId) " +
            " order by o.orderId desc")
    Page<OrderResponse> filterAllWarehouses(@Param("orderCode") String orderCode, @Param("customerName") String customerName,
                                            @Param("orderStatusId") Long orderStatusId, @Param("warehouseId") Long warehouseId,
                                            Pageable pageable);
}
