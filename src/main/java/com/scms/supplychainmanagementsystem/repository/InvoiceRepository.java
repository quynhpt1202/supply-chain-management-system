package com.scms.supplychainmanagementsystem.repository;


import com.scms.supplychainmanagementsystem.dto.DashboardResponse;
import com.scms.supplychainmanagementsystem.dto.InvoiceInfoMail;
import com.scms.supplychainmanagementsystem.entity.Invoice;
import com.scms.supplychainmanagementsystem.entity.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;


@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

    @Query(value = "select cu.customer_name as customerName, cu.email as email" +
            " , i.invoice_code as invoiceCode, o.order_code as orderCode " +
            " , cast(i.total_amount as char)+0 as totalAmount " +
            " , cast(i.total_amount_paid as char)+0 as totalAmountPaid" +
            " , date_format(i.payment_due_date, '%d-%m-%Y') as paymentDueDate " +
            " from invoice i inner join `order` o on i.orderid = o.order_id " +
            " inner join contact_delivery c on c.contactid = o.contactid " +
            " inner join customer cu on c.customer_id = cu.customer_id " +
            " where o.order_statusid in (3,4)" +
            " and (:warehouseId is null or i.warehouseid = :warehouseId) " +
            " order by cu.email ", nativeQuery = true)
    List<InvoiceInfoMail> getAllInvoiceWhenOrderNotClosed(@Param("warehouseId") Long warehouseId);

    @Query(value = "select cu.customer_name as customerName, cu.email as email" +
            " , i.invoice_code as invoiceCode, o.order_code as orderCode " +
            " , cast(i.total_amount as char)+0 as totalAmount " +
            " , cast(i.total_amount_paid as char)+0 as totalAmountPaid" +
            " , date_format(i.payment_due_date, '%d-%m-%Y') as paymentDueDate " +
            " from invoice i inner join `order` o on i.orderid = o.order_id " +
            " inner join contact_delivery c on c.contactid = o.contactid " +
            " inner join customer cu on c.customer_id = cu.customer_id " +
            " where o.order_statusid in (3,4)" +
            " and (:warehouseId is null or i.warehouseid = :warehouseId) " +
            " and i.payment_due_date - curdate() = 1" +
            " order by cu.email ", nativeQuery = true)
    List<InvoiceInfoMail> getAllInvoiceNearDueDate(@Param("warehouseId") Long warehouseId);

    @Query(value = "select coalesce(sum(total_amount), 0) as data" +
            " , year(created_date) as year" +
            " , month(created_date) as month" +
            " from invoice " +
            " where (:warehouseId is null or warehouseid = :warehouseId) " +
            " and created_date between :fromDate and :toDate " +
            " group by  month(created_date), year(created_date)", nativeQuery = true)
    List<DashboardResponse> getRevenue(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("warehouseId") Long warehouseId);

    @Query(value = "select coalesce(sum(total_amount), 0) as total" +
            " from invoice " +
            " where (:warehouseId is null or warehouseid = :warehouseId)" +
            " and created_date between :fromDate and :toDate ", nativeQuery = true)
    Double getSumRevenue(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("warehouseId") Long warehouseId);

    @Query(value = "with a as (select sum(total_amount) as revenue" +
            "                , year(created_date)  as year" +
            "                , month(created_date) as month" +
            "           from invoice" +
            "           where (:warehouseId is null or warehouseid = :warehouseId) " +
            "               and created_date between :fromDate and :toDate " +
            "           group by month(created_date), year(created_date)), " +
            "     b as (select sum(stock_in_quantity * unit_cost_price) as expense " +
            "                , year(created_date)                       as year " +
            "                , month(created_date)                      as month " +
            "           from stock_history " +
            "           where (:warehouseId is null or warehouseid = :warehouseId) " +
            "               and created_date between :fromDate and :toDate " +
            "           group by month(created_date), year(created_date)) " +
            "select (t.revenue - t.expense) as data, t.month, t.year " +
            "from (" +
            "         select coalesce(revenue, 0) as revenue, coalesce(expense, 0) as expense, a.year, a.month " +
            "         from a " +
            "                  left outer join b " +
            "                                  on a.month = b.month and a.year = b.year " +
            "         union " +
            "         select coalesce(revenue, 0) as revenue, coalesce(expense, 0) as expense, b.year, b.month " +
            "         from a " +
            "                  right outer join b " +
            "                                   on a.month = b.month and a.year = b.year) as t", nativeQuery = true)
    List<DashboardResponse> getGrossProfit(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("warehouseId") Long warehouseId);

    @Query(value = "with a as (select sum(total_amount) as revenue " +
            "           from invoice " +
            "           where (:warehouseId is null or warehouseid = :warehouseId) " +
            "             and created_date between :fromDate and :toDate), " +
            "     b as (select sum(stock_in_quantity * unit_cost_price) as expense " +
            "           from stock_history " +
            "           where (:warehouseId is null or warehouseid = :warehouseId) " +
            "             and created_date between :fromDate and :toDate) " +
            "select coalesce(a.revenue, 0) - coalesce(b.expense, 0) " +
            "from a join b ", nativeQuery = true)
    Double getSumGrossProfit(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("warehouseId") Long warehouseId);

    @Modifying
    @Query(value = "delete from Invoice i where i.order = :order")
    void deleteInvoiceByOrder(Order order);


    @Query(value = "SELECT u FROM Invoice  u where u.invoiceId = :invoiceId and u.order.warehouse.warehouseID= :warehouseid")
    Invoice findByinvoiceIdAnhInWarehouse(@Param("invoiceId") Long invoiceId, @Param("warehouseid") Long warehouseId);

    @Query(value = "SELECT u FROM Invoice  u where u.order.orderId = :orderId and u.order.warehouse.warehouseID= :warehouseid")
    Invoice findByOrderIdAnhInWarehouse(@Param("orderId") Long orderId, @Param("warehouseid") Long warehouseId);

    @Query(value = "SELECT u FROM Invoice  u where u.order.orderId = :orderId ")
    Invoice findByOrderId(@Param("orderId") Long orderId);

    @Query(value = "select u from Invoice u where u.order.warehouse.warehouseID =:warehouseId " +
            " and (:invoiceCode is null or u.invoiceCode like %:invoiceCode%) " +
            " order by u.lastModifiedDate desc")
    Page<Invoice> filterInOneWarehouse(@Param("warehouseId") Long warehouseId,
                                       @Param("invoiceCode") String invoiceCode, Pageable pageable);

    @Query(value = "select u from Invoice u where (:warehouseId is null or u.order.warehouse.warehouseID = :warehouseId) " +
            " and (:invoiceCode is null or u.invoiceCode like %:invoiceCode%) " +
            " order by u.lastModifiedDate desc")
    Page<Invoice> filterAllWarehouses(@Param("warehouseId") Long warehouseId,
                                      @Param("invoiceCode") String invoiceCode, Pageable pageable);
}
