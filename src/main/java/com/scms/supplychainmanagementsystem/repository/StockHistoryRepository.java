package com.scms.supplychainmanagementsystem.repository;

import com.scms.supplychainmanagementsystem.dto.DashboardResponse;
import com.scms.supplychainmanagementsystem.entity.StockHistory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface StockHistoryRepository extends JpaRepository<StockHistory, Long> {

    @Query(value = "select coalesce(sum(stock_in_quantity * unit_cost_price), 0) as data" +
            " , year(created_date) as year" +
            " , month(created_date) as month" +
            " from stock_history " +
            " where (:warehouseId is null or warehouseid = :warehouseId)" +
            " and created_date between :fromDate and :toDate " +
            " group by month(created_date), year(created_date)", nativeQuery = true)
    List<DashboardResponse> getProductCost(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("warehouseId") Long warehouseId);

    @Query(value = "select coalesce(sum(stock_in_quantity * unit_cost_price), 0) as data" +
            " from stock_history " +
            " where (:warehouseId is null or warehouseid = :warehouseId)" +
            " and created_date between :fromDate and :toDate ", nativeQuery = true)
    Double getSumProductCost(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("warehouseId") Long warehouseId);

    @Query(value = "SELECT u FROM StockHistory u where u.stockHistoryID= :stockHistoryId  and u.product.warehouse.warehouseID= :warehouseid")
    StockHistory findByStockHistoryId(@Param("stockHistoryId") Long stockHistoryId, @Param("warehouseid") Long warehouseid);

    @Query(value = "SELECT u FROM StockHistory u where u.stockHistoryID= :stockHistoryId ")
    StockHistory findByStockHistoryIdAdmin(@Param("stockHistoryId") Long stockHistoryId);


    @Modifying
    @Transactional
    @Query(value = "Delete  FROM StockHistory u where u.stockHistoryID= :stockHistoryId  and u.product.warehouse.warehouseID= :warehouseid")
    void deleteStockHistory(@Param("stockHistoryId") Long stockHistoryId, @Param("warehouseid") Long warehouseid);

    @Modifying
    @Transactional
    @Query(value = "Delete  FROM StockHistory u where u.stockHistoryID= :stockHistoryId ")
    void deleteStockHistoryAdmin(@Param("stockHistoryId") Long stockHistoryId);

//    @Query(value = "select u from StockHistory u where u.product.productId=:productId and u.product.warehouse.warehouseID=:warehouseid " +
//            " order by u.createdDate desc")
//    Page<StockHistory> filterInOneWarehouseAndProduct(@Param("productId") Long productId, @Param("warehouseid") Long warehouseid, Pageable pageable);
//
//    @Query(value = "select u from StockHistory u where  u.product.productId=:productId and (:warehouseId is null or u.product.warehouse.warehouseID= :warehouseId) " +
//            " order by u.createdDate desc")
//    Page<StockHistory> filterAllWarehousesAndProduct(@Param("productId") Long productId,@Param("warehouseId") Long warehouseId, Pageable pageable);

    @Query(value = "select u from StockHistory u where  (:productName is null or u.product.productName like %:productName% )" +
            "and u.product.warehouse.warehouseID=:warehouseid " +
            " order by u.stockHistoryID desc")
    Page<StockHistory> filterInOneWarehouse(@Param("productName") String productName, @Param("warehouseid") Long warehouseid, Pageable pageable);

    @Query(value = "select u from StockHistory u where   (:productName is null or u.product.productName like %:productName% ) and (:warehouseId is null or u.product.warehouse.warehouseID= :warehouseId) " +
            " order by u.stockHistoryID desc")
    Page<StockHistory> filterAllWarehouses(@Param("productName") String productName, @Param("warehouseId") Long warehouseId, Pageable pageable);

}

