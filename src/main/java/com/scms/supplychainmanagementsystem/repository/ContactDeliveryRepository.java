package com.scms.supplychainmanagementsystem.repository;

import com.scms.supplychainmanagementsystem.entity.ContactDelivery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface ContactDeliveryRepository extends JpaRepository<ContactDelivery, Long> {


    @Query(value = "select u FROM ContactDelivery u where u.contactID = :contactid ")
    ContactDelivery findByContactIDManager(@Param("contactid") Long contactid);

    @Query(value = "SELECT u FROM ContactDelivery u where u.contactID = :contactid")
    ContactDelivery findByContactIDAdmin(@Param("contactid") Long contactid);

    @Modifying
    @Transactional
    @Query(value = "Delete  FROM ContactDelivery u where u.contactID= :contactid ")
    void deleteContactDeliverie(@Param("contactid") Long contactId);


    @Query(value = "select u from ContactDelivery u where u.customer.customerId =:customerid " +
            " and (:contactName is null or u.contactName like %:contactName%) " +
            " order by u.createedDate desc")
    Page<ContactDelivery> filterInOneCustomer(@Param("customerid") Long customerid,
                                              @Param("contactName") String contactName, Pageable pageable);

    @Query(value = "select u from ContactDelivery u where (:customerid is null or u.customer.customerId = :customerid) " +
            " and (:contactName is null or u.contactName like %:contactName%) " +
            " order by u.createedDate desc")
    Page<ContactDelivery> filterAllCustomer(@Param("customerid") Long customerid,
                                            @Param("contactName") String contactName, Pageable pageable);

    boolean existsByContactName(String contactName);

    @Query(value = "SELECT p FROM ContactDelivery p where p.contactName  like :name and p.customer.warehouse.warehouseID=:warehouseId")
    ContactDelivery getByContactNameAndWarehouseWarehouseID(@Param("name") String name, @Param("warehouseId") Long warehouseId);

    @Query(value = "SELECT u FROM ContactDelivery u where  u.contactName like  :contactName  ")
    ContactDelivery getByContactNameAndCustomerIdExcel(@Param("contactName") String contactName);

    @Query(value = "SELECT u FROM ContactDelivery u where  u.contactName like  :contactName  and u.customer.customerId =:customerId")
    ContactDelivery getByContactNameAndCustomerId(@Param("contactName") String contactName, @Param("customerId") Long customerId);

    @Query(value = "select u from ContactDelivery u where  u.customer.customerId = :customerid ")
    List<ContactDelivery> getContactDeliveryByCustomerId(@Param("customerid") Long customerid);

}
