package com.scms.supplychainmanagementsystem.repository;

import com.scms.supplychainmanagementsystem.entity.Supplier;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.lang.annotation.Native;
import java.util.List;

@Repository
public interface WarehouseRepository extends JpaRepository<Warehouse, Long> {




    boolean existsByWarehouseName(String warehouseName);

    @Query(value = "select u from Warehouse u where u.warehouseID >0 and  (:warehouseName is null or u.warehouseName like %:warehouseName%) " +
            "and   (:adress is null or u.address like %:adress%) ")
    Page<Warehouse> filterAll(@Param("warehouseName") String warehouseName, @Param("adress") String adress, Pageable pageable);

    @Query(value = "select u from Warehouse u where u.warehouseName like :warehouseName ")
    Warehouse getByWarehouseName(@Param("warehouseName") String warehouseName);

}
