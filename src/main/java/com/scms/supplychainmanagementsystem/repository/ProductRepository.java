package com.scms.supplychainmanagementsystem.repository;

import com.scms.supplychainmanagementsystem.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {


    @Query(value = "select p from Product p where p.warehouse.warehouseID =:warehouseId " +
            " and (:productName is null or p.productName like %:productName%) " +
            " and (:categoryId is null or p.category.categoryId = :categoryId) " +
            " and (:isActive is null or p.isActive = :isActive) " +
            " order by p.productId desc")
    Page<Product> filterInOneWarehouse(@Param("productName") String productName, @Param("categoryId") Long categoryId,
                                       @Param("warehouseId") Long warehouseId, @Param("isActive") Boolean isActive,
                                       Pageable pageable);

    @Query(value = "select p from Product p where (:warehouseId is null or p.warehouse.warehouseID = :warehouseId) " +
            " and (:productName is null or p.productName like %:productName%) " +
            " and (:categoryId is null or p.category.categoryId = :categoryId) " +
            " and (:isActive is null or p.isActive = :isActive) " +
            " order by p.productId desc")
    Page<Product> filterAllWarehouses(@Param("productName") String productName, @Param("categoryId") Long categoryId,
                                      @Param("warehouseId") Long warehouseId, @Param("isActive") Boolean isActive,
                                      Pageable pageable);

    @Query(value = "SELECT p FROM Product p where p.productName  like :name and p.warehouse.warehouseID=:warehouseId")
    Product getByProductNameAndWarehouseWarehouseID(@Param("name") String name, @Param("warehouseId") Long warehouseId);

    Product findByProductName(String name);

    @Query(value = "select case when count(p) > 0 then true else false end" +
            " from Product p where p.productName = :productName and p.warehouse.warehouseID =  :warehouseId")
    boolean existsProductByProductNameInWarehouse(String productName, Long warehouseId);

    @Query(value = "SELECT p FROM Product p where p.warehouse.warehouseID=:warehouseId")
    List<Product> findAllByWarehouseID(@Param("warehouseId") Long warehouseId);


}
