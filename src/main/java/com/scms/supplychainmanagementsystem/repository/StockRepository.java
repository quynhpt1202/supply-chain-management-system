package com.scms.supplychainmanagementsystem.repository;

import com.scms.supplychainmanagementsystem.entity.Product;
import com.scms.supplychainmanagementsystem.entity.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends JpaRepository<Stock, Long> {

    @Query(value = "select s.availableQuantity from Stock s where s.product.productId = :productId")
    Double getAvailableQuantityByProductId(Long productId);

    @Query(value = "select case when s.availableQuantity >= :quantity then true else false end" +
            " from Stock s where s.product.productId = :productId")
    boolean checkQtyAvailable(Long productId, Double quantity);

    @Modifying
    @Query(value = "delete from Stock where product.productId = :productId")
    void deleteByProductId(Long productId);

    @Modifying
    @Query(value = "update Stock s set s.availableQuantity = s.availableQuantity - :quantity, s.lastModifiedDate = current_timestamp " +
            "where s.product.productId = :productId")
    void minusStockQuantityByOrder(Double quantity, Long productId);

    @Modifying
    @Query(value = "update Stock s set s.availableQuantity = s.availableQuantity + :quantity, s.lastModifiedDate = current_timestamp " +
            "where s.product.productId = :productId")
    void plusStockQuantityByOrder(Double quantity, Long productId);

    Stock findByProduct(Product product);

    @Query(value = "SELECT u FROM Stock u where u.product.productId= :productid ")
    Stock findByProductId(@Param("productid") Long productid);

    @Query(value = "select u from Stock u where u.product.productId=:productid and u.product.warehouse.warehouseID =:warehouseId ")
    Page<Stock> filterInOneWarehouse(@Param("productid") Long productid, @Param("warehouseId") Long warehouseId, Pageable pageable);

    @Query(value = "select u from Stock u where u.product.productId=:productid and (:warehouseId is null or u.product.warehouse.warehouseID = :warehouseId) ")
    Page<Stock> filterAllWarehouses(@Param("productid") Long productid, @Param("warehouseId") Long warehouseId, Pageable pageable);


    @Query(value = "select u from Stock u where (:productName is null or u.product.productName like %:productName%) " +
            " and (:warehouseId is null or u.product.warehouse.warehouseID = :warehouseId) ")
    Page<Stock> filterAllWarehousesAdmin(@Param("productName") String productName, @Param("warehouseId") Long warehouseId, Pageable pageable);

    @Query(value = "select u from Stock u where  u.product.warehouse.warehouseID = :warehouseId " +
            " and (:productName is null or u.product.productName like %:productName%) ")
    Page<Stock> filteroneWarehouses(@Param("productName") String productName, @Param("warehouseId") Long warehouseId, Pageable pageable);
}
