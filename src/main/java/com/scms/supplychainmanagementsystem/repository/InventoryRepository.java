package com.scms.supplychainmanagementsystem.repository;

import com.scms.supplychainmanagementsystem.dto.DashboardResponse;
import com.scms.supplychainmanagementsystem.entity.Inventory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory, Long> {
    @Query(value = "select coalesce(sum(i.shortage_quantity * p.price * 0.5), 0) as data " +
            "     , year(date_check)   as year " +
            "     , month(date_check)  as month " +
            "from inventory i " +
            "         inner join price_book_entry p on i.product_id = p.product_id " +
            "         inner join price_book b on p.price_book_id = b.price_book_id " +
            "where (:warehouseId is null or i.warehouseid = :warehouseId) " +
            "  and b.is_standard_price_book = true " +
            "  and date_check between :fromDate and :toDate " +
            "group by month(date_check), year(date_check) ", nativeQuery = true)
    List<DashboardResponse> getDefectiveProductCost(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("warehouseId") Long warehouseId);

    @Query(value = "select coalesce(sum(i.shortage_quantity * p.price * 0.5), 0) as data " +
            "from inventory i " +
            "         inner join price_book_entry p on i.product_id = p.product_id " +
            "         inner join price_book b on p.price_book_id = b.price_book_id " +
            "where b.is_standard_price_book = true " +
            "  and (:warehouseId is null or i.warehouseid = :warehouseId) " +
            "  and date_check between :fromDate and :toDate ", nativeQuery = true)
    Double getSumDefectiveProductCost(@Param("fromDate") LocalDate fromDate, @Param("toDate") LocalDate toDate, @Param("warehouseId") Long warehouseId);

    @Query(value = "SELECT u FROM Inventory  u where u.warehouse.warehouseID = :warehouseid")
    List<Inventory> findAllByWarehouse(@Param("warehouseid") Long warehouseId);

    @Query(value = "SELECT u FROM Inventory  u where u.inventoryId = :inventoryId and u.warehouse.warehouseID= :warehouseid")
    Inventory findByinventoryIdAnhInWarehouse(@Param("inventoryId") Long inventoryId, @Param("warehouseid") Long warehouseId);


    @Modifying
    @Transactional
    @Query(value = "Delete  FROM Inventory u where u.inventoryId= :inventoryId and u.warehouse.warehouseID= :warehouseid")
    void deleteinventory(@Param("inventoryId") Long inventoryId, @Param("warehouseid") Long warehouseId);

    @Modifying
    @Transactional
    @Query(value = "Delete  FROM Inventory u where u.inventoryId= :inventoryId ")
    void deleteinventoryAdmin(@Param("inventoryId") Long inventoryId);


    @Query(value = "select u from Inventory u where u.warehouse.warehouseID =:warehouseId " +
            " and (:productName is null or u.product.productName like %:productName%) " +
            " order by u.createdDate desc")
    Page<Inventory> filterInOneWarehouse(@Param("warehouseId") Long warehouseId,
                                         @Param("productName") String productName, Pageable pageable);

    @Query(value = "select u from Inventory u where (:warehouseId is null or u.warehouse.warehouseID = :warehouseId) " +
            " and (:productName is null or u.product.productName like %:productName%) " +
            " order by u.createdDate desc")
    Page<Inventory> filterAllWarehouses(@Param("warehouseId") Long warehouseId,
                                        @Param("productName") String productName, Pageable pageable);
}
