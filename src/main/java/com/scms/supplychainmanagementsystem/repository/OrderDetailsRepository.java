package com.scms.supplychainmanagementsystem.repository;

import com.scms.supplychainmanagementsystem.dto.OrderDetailsResponse;
import com.scms.supplychainmanagementsystem.entity.Order;
import com.scms.supplychainmanagementsystem.entity.OrderDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderDetailsRepository extends JpaRepository<OrderDetails, Long> {

    @Query(value = "select o from OrderDetails o where o.product.productId = :productId" +
            " and o.order.orderStatus.orderStatusID in (1, 2) and o.orderDetailsStatus.orderDetailStatusID = 1 " +
            " and o.quantity <= :quantity" +
            " order by o.order.orderStatus.orderStatusID desc, o.order.createdDate asc")
    List<OrderDetails> getOrderDetailsPendingByProductId(Long productId, Double quantity);

    @Query(value = "select o from OrderDetails o where o.order.warehouse.warehouseID = :warehouseId " +
            " and o.orderDetailsStatus.orderDetailStatusID in (1, 2) and o.order.orderId <> :orderId")
    List<OrderDetails> getAllOrderDetailWhenOrderNotConfirm(Long warehouseId, Long orderId);

    @Query(value = "select case when count(o.orderDetailId) > 0 then true else false end" +
            " from OrderDetails o where o.order.orderId = :orderId " +
            " and o.product.productId = :productId " +
            " and (:oldProductId is null or o.product.productId <> :oldProductId)")
    boolean existsByOrderIdAndProductId(Long orderId, Long productId, Long oldProductId);

    void deleteAllByOrder(Order order);

    @Query(value = "select o from OrderDetails o where o.product.productId = :productid  order by o.order.createdDate asc ")
    List<OrderDetails> getOrderDetailsByProductId(Long productid);

    @Query(value = "select o from OrderDetails o where o.product.productId = :productid  order by o.order.createdDate desc ")
    List<OrderDetails> getOrderDetailsByProductIdDesc(Long productid);

    @Query(value = "select o from OrderDetails o where o.order.orderId = :orderId")
    List<OrderDetails> findAllByOrderId(Long orderId);

    boolean existsByOrder(Order order);

    @Query(value = "select case when count(o.orderDetailId) > 0 then true else false end" +
            " from OrderDetails o inner join Stock s on o.product.productId = s.product.productId " +
            " where o.order.orderId = :orderId " +
            " and o.quantity > s.availableQuantity ")
    boolean existsByOrderIdAndNotEnoughStock(Long orderId);

    @Query(value = "select new com.scms.supplychainmanagementsystem.dto.OrderDetailsResponse(o.orderDetailId, o.product.productId, o.product.productName " +
            " ,o.quantity, o.order.orderId, o.priceBook.priceBookId, o.priceBook.priceBookName, o.priceBook.isStandardPriceBook " +
            ", o.orderDetailsStatus, o.currentPrice, coalesce(o.currentPrice * o.quantity, 0)) " +
            " from OrderDetails o where o.order.orderId = :orderId " +
            " and :productName is null or o.product.productName like %:productName% " +
            " and :orderDetailsStatusId is null or o.orderDetailsStatus.orderDetailStatusID = :orderDetailsStatusId ")
    Page<OrderDetailsResponse> getAllOrderDetailsByOrderId(@Param("orderId") Long orderId,
                                                           @Param("productName") String productName,
                                                           @Param("orderDetailsStatusId") Long orderDetailsStatusId,
                                                           Pageable pageable);

}
