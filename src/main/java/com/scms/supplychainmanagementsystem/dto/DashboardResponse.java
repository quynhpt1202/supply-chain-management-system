package com.scms.supplychainmanagementsystem.dto;

public interface DashboardResponse {
     Double getData();

     Integer getYear();

     Integer getMonth();
}
