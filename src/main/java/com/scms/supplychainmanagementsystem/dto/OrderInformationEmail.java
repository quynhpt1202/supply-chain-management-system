package com.scms.supplychainmanagementsystem.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderInformationEmail {
    private String subject;
    private String recipient;
    private String customerName;
    private String orderCode;
    private String content;
    private Double totalAmount;
    List<OrderInformationDto> orderInformationDtoList;
}
