package com.scms.supplychainmanagementsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderResponseDetails extends OrderRequest {
    private Long orderId;

    private Instant createdDate;

    private Instant lastModifiedDate;

    private String orderCode;

    private Long orderStatusId;

    private String createdBy;

    private String lastModifiedBy;

    private Long customerId;

    private Long warehouseId;

    private Long contactId;

    private String note;

    private Double totalAmount;
}
