package com.scms.supplychainmanagementsystem.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductResponse {
    private Long productId;
    private String productName;
    private String warehouseName;
    private String categoryName;
    private String quantityUnitOfMeasure;
    private Boolean isActive;
    private Double availableQuantity;
}
