package com.scms.supplychainmanagementsystem.dto;

import com.scms.supplychainmanagementsystem.entity.OrderDetailsStatus;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDetailsResponse {

    private Long orderDetailId;

    private Long productId;

    private String productName;

    private Double quantity;

    private Long orderId;

    private Long priceBookId;

    private String priceBookName;

    private Boolean isStandardPriceBook;

    private OrderDetailsStatus orderDetailsStatus;

    private Double price;

    private Double amount;
}
