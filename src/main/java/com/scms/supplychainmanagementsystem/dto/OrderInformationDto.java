package com.scms.supplychainmanagementsystem.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderInformationDto {
    private String productName;
    private String category;
    private String unit;
    private String quantity;
    private Double price;
    private Double amount;

}
