package com.scms.supplychainmanagementsystem.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.scms.supplychainmanagementsystem.entity.Role;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import lombok.*;

import java.time.Instant;
import java.time.LocalDate;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResponse {
    private Long userId;

    private String username;

    private String email;

    private Role role;

    private Warehouse warehouse;

    private String firstName;

    private String lastName;

    private boolean isActive;

    private String phone;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;

    private String streetAddress;

    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;
}
