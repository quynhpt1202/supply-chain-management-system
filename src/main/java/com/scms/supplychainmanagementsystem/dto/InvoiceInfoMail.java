package com.scms.supplychainmanagementsystem.dto;

public interface InvoiceInfoMail {
    String getCustomerName();

    String getEmail();

    String getInvoiceCode();

    String getOrderCode();

    Double getTotalAmount();

    Double getTotalAmountPaid();

    String getPaymentDueDate();
}
