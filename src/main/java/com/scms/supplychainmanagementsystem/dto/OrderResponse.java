package com.scms.supplychainmanagementsystem.dto;

import java.time.Instant;

public interface OrderResponse {
    Long getOrderId();

    Instant getCreatedDate();

    String getOrderCode();

    String getStatus();

    Long getOrderStatusId();

    String getContactName();

    String getWarehouseName();

    String getCustomerName();

    String getPhone();

    String getNote();
}
