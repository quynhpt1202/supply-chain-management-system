package com.scms.supplychainmanagementsystem.entity;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "`Order`")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;


    private Instant createdDate;

    private Instant lastModifiedDate;

    private String note;

    private String orderCode;

    @ManyToOne
    @JoinColumn(name = "WarehouseID", referencedColumnName = "WarehouseId")
    private Warehouse warehouse;

    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "ContactID", referencedColumnName = "ContactID")
    private ContactDelivery contactDelivery;


    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "OrderStatusID", referencedColumnName = "orderStatusID")
    private OrderStatus orderStatus;


    @ManyToOne
    @JoinColumn(name = "createdBy", referencedColumnName = "userId")
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "lastModifiedBy", referencedColumnName = "userId")
    private User lastModifiedBy;

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", createdDate=" + createdDate +
                ", lastModifiedDate=" + lastModifiedDate +
                ", note='" + note + '\'' +
                ", orderCode='" + orderCode + '\'' +
                ", warehouse=" + warehouse +
                ", contactDelivery=" + contactDelivery +
                ", orderStatus=" + orderStatus +
                ", createdBy=" + createdBy +
                ", lastModifiedBy=" + lastModifiedBy +
                '}';
    }
}
