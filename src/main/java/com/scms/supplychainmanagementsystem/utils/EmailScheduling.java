package com.scms.supplychainmanagementsystem.utils;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@AllArgsConstructor
@Slf4j
public class EmailScheduling {
    private SendMailJob sendMailJob;
    public static final String jobSendEveryMonth = "send_invoice_mail_last_day_every_month";
    public static final String jobSendEveryday = "send_invoice_mail_near_due_everyday";

    @Scheduled(cron = "${interval-in-cron-month}")
    @SchedulerLock(name = jobSendEveryMonth)
    public void sendInvoiceMailEveryMonth() {
        log.info("Start job send mail " + jobSendEveryMonth);
        HashMap<String, Boolean> resultMailSent = sendMailJob.sendInvoiceMail(null, jobSendEveryMonth);
        log.info("End job send mail result " + resultMailSent.toString());
    }

    @Scheduled(cron = "${interval-in-cron-day}")
    @SchedulerLock(name = jobSendEveryday)
    public void sendInvoiceMailEveryday() {
        log.info("Start job send mail " + jobSendEveryday);
        HashMap<String, Boolean> resultMailSent = sendMailJob.sendInvoiceMail(null, jobSendEveryday);
        log.info("End job send mail result " + resultMailSent.toString());
    }
}
