package com.scms.supplychainmanagementsystem.utils;

import com.scms.supplychainmanagementsystem.dto.ForgotPasswordEmail;
import com.scms.supplychainmanagementsystem.dto.InvoiceInfoMail;
import com.scms.supplychainmanagementsystem.dto.OrderInformationEmail;
import com.scms.supplychainmanagementsystem.exception.AppException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Component
@AllArgsConstructor
@Slf4j
public class MailService {

    private final JavaMailSender mailSender;
    private final MailContentBuilder mailContentBuilder;

    @Async
    public void sendMailForgotPassword(ForgotPasswordEmail forgotPasswordEmail) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setTo(forgotPasswordEmail.getRecipient());
            messageHelper.setSubject(forgotPasswordEmail.getSubject());
            messageHelper.setText(mailContentBuilder.buildMailForgotPassword(forgotPasswordEmail.getUsername(), forgotPasswordEmail.getContent()), true);
        };
        try {
            mailSender.send(messagePreparator);
            log.info("Request reset password email sent");
        } catch (MailException e) {
            log.error("Exception occurred when sending mail to " + forgotPasswordEmail.getRecipient(), e);
            throw new AppException("SEND_MAIL_FAIL");
        }
    }

    @Async
    public void sendMailOrderInformation(OrderInformationEmail orderMail) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
            messageHelper.setTo(orderMail.getRecipient());
            messageHelper.setSubject(orderMail.getSubject());
            messageHelper.setText(mailContentBuilder.buildMailOrderInformation(orderMail), true);
        };
        try {
            mailSender.send(messagePreparator);
            log.info("[Sent order information email successfully]");
        } catch (MailException e) {
            log.error("Exception occurred when sending mail to " + orderMail.getRecipient(), e);
            throw new AppException("SEND_MAIL_FAIL");
        }
    }

    public HashMap<String, Boolean> sendMailInvoiceInformation(Map<String, List<InvoiceInfoMail>> invoiceMap, String subject, String content) {
        HashMap<String, Boolean> resultMailSent = new HashMap<>();
        while (invoiceMap.size() != 0) {
            for (Iterator<Map.Entry<String, List<InvoiceInfoMail>>> it = invoiceMap.entrySet().iterator();
                 it.hasNext(); ) {
                Map.Entry<String, List<InvoiceInfoMail>> entry = it.next();
                //invoiceMap.forEach((k, v) -> {
                Double totalSumAmount = entry.getValue().stream()
                        .reduce(0.0, (partialAmountResult, invoice) -> partialAmountResult + invoice.getTotalAmount(), Double::sum);
                Double totalSumAmountPaid = entry.getValue().stream()
                        .reduce(0.0, (partialAmountResult, invoice) -> partialAmountResult + invoice.getTotalAmountPaid(), Double::sum);
                MimeMessagePreparator messagePreparator = mimeMessage -> {
                    MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
                    messageHelper.setTo(entry.getKey());
                    messageHelper.setSubject(subject);
                    messageHelper.setText(mailContentBuilder.buildMailInvoiceInformation(entry.getValue().get(0).getCustomerName()
                            , entry.getValue(), totalSumAmount, totalSumAmountPaid, content), true);
                };
                try {
                    mailSender.send(messagePreparator);
                    resultMailSent.put(entry.getKey(), true);
                    log.info("Sent invoice email to " + entry.getKey() + " successfully");
                } catch (MailException e) {
                    resultMailSent.put(entry.getKey(), false);
                    log.error("Exception occurred when sending mail to " + entry.getKey(), e);
                } finally {
                    it.remove();
                }
            }
        }
        return resultMailSent;
    }

}
