package com.scms.supplychainmanagementsystem.utils;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExcelElementCheck {
    private String customerCode;
    private String contact;
    private String product;
    private String priceBook;
    private int row;

}
