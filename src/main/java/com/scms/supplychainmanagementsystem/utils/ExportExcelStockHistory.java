package com.scms.supplychainmanagementsystem.utils;

import com.scms.supplychainmanagementsystem.entity.Product;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Component
public class ExportExcelStockHistory {

    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<Product> productList;


    public ExportExcelStockHistory(List<Product> productList) {
        workbook = new XSSFWorkbook();
        this.productList = productList;
    }


    private void writeHeaderLine() {
        sheet = workbook.createSheet("StockHistory");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(11);
        style.setFont(font);
        sheet.autoSizeColumn(0);
        sheet.setColumnWidth(0, 7000);
        sheet.setColumnWidth(1, 7000);
        sheet.setColumnWidth(2, 7000);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        createCell(row, 0, "Product Name (Tên sản Phẩm)", style);
        createCell(row, 1, "Stock In Quantity (Số Lượng)", style);
        createCell(row, 2, "Unit Cost Price (Giá)", style);


    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }

    private void writeDataLines() {
        sheet = workbook.createSheet("Infor");
        CellStyle styleelement = workbook.createCellStyle();
        CellStyle style = workbook.createCellStyle();
        CellStyle styleHeader = workbook.createCellStyle();
        styleHeader.setAlignment(HorizontalAlignment.CENTER);
        styleHeader.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);
        styleelement.setFont(font);

        int rowCount = 0;
        Row row = sheet.createRow(rowCount++);
        createCell(row, 0, "ProductName", styleHeader);
        createCell(row, 1, "IsActive", styleHeader);
        row = sheet.createRow(rowCount++);

        for (Product product : productList) {
            row = sheet.createRow(rowCount++);
            createCell(row, 0, product.getProductName(), styleelement);
            createCell(row, 1, product.getIsActive() == true ? "Active" : "InActive", styleelement);
        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();

        outputStream.close();

    }
}
