package com.scms.supplychainmanagementsystem.utils;

import com.scms.supplychainmanagementsystem.dto.InvoiceInfoMail;
import com.scms.supplychainmanagementsystem.dto.OrderInformationEmail;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.util.List;

@Service
@AllArgsConstructor
public class MailContentBuilder {

    private final TemplateEngine templateEngine;

    public String buildMailForgotPassword(String username, String content) {
        Context context = new Context();
        context.setVariable("username", username);
        context.setVariable("content", content);
        return templateEngine.process("mailForgotPassword", context);
    }

    public String buildMailOrderInformation(OrderInformationEmail orderMail) {
        Context context = new Context();
        context.setVariable("customerName", orderMail.getCustomerName());
        context.setVariable("orderCode", orderMail.getOrderCode());
        context.setVariable("content", orderMail.getContent());
        context.setVariable("orders", orderMail.getOrderInformationDtoList());
        context.setVariable("totalAmount", orderMail.getTotalAmount());
        return templateEngine.process("mailOrderTemplate", context);
    }

    public String buildMailInvoiceInformation(String customerName, List<InvoiceInfoMail> infoMailList
            , Double totalSumAmount, Double totalSumAmountPaid, String content) {
        Context context = new Context();
        context.setVariable("customerName", customerName);
        context.setVariable("invoices", infoMailList);
        context.setVariable("totalSumAmount", totalSumAmount);
        context.setVariable("totalSumAmountPaid", totalSumAmountPaid);
        context.setVariable("content", content);
        return templateEngine.process("mailScheduleInvoiceTemplate", context);
    }
}
