package com.scms.supplychainmanagementsystem.utils;

import com.scms.supplychainmanagementsystem.entity.ContactDelivery;
import com.scms.supplychainmanagementsystem.entity.Customer;
import com.scms.supplychainmanagementsystem.entity.PriceBook;
import com.scms.supplychainmanagementsystem.entity.PriceBookEntry;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Transactional
@Component
public class ExportExcelOrder {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<PriceBook> priceBookList;
    private List<Customer> customerList;
    private List<ContactDelivery> contactDeliveryList;
    private List<PriceBookEntry> priceBookEntryList;





    public ExportExcelOrder( List<PriceBook> priceBookList, List<Customer> customerList,List<ContactDelivery> contactDeliveryList, List<PriceBookEntry> priceBookEntryList) {
        workbook = new XSSFWorkbook();
        this.priceBookList = priceBookList;
        this.customerList = customerList;
        this.contactDeliveryList = contactDeliveryList;
        this.priceBookEntryList = priceBookEntryList;
    }

    private void writeHeaderLine() {
        sheet = workbook.createSheet("Order");

        Row row = sheet.createRow(0);

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(11);
        style.setFont(font);
        sheet.autoSizeColumn(0);
        sheet.setColumnWidth(0, 7000);
        sheet.setColumnWidth(1, 7000);
        sheet.setColumnWidth(2, 5000);
        sheet.setColumnWidth(3, 3000);
        sheet.setColumnWidth(4, 7000);
        sheet.setColumnWidth(5, 10000);
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        createCell(row, 0, "CustomerCode", style);
        createCell(row, 1, "Contact", style);
        createCell(row, 2, "Product", style);
        createCell(row, 3, "Quantity", style);
        createCell(row, 4, "PricebookName", style);
        createCell(row, 5, "Note", style);



    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {if(value instanceof String){
            cell.setCellValue((String) value);}
        else{
            cell.setCellValue((Double) value);
        }
        cell.setCellStyle(style);
    }}


        private void writeDataLines() {


        sheet = workbook.createSheet("Infor");

        CellStyle style = workbook.createCellStyle();
        CellStyle styleHeaderCustomer = workbook.createCellStyle();
        styleHeaderCustomer.setAlignment(HorizontalAlignment.CENTER);
        styleHeaderCustomer.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
        styleHeaderCustomer.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        int rowCount = 0;
        Row row = sheet.createRow(rowCount++);
            createCell(row, 0, "CustomerName", styleHeaderCustomer);
            createCell(row, 1, "CustomerCode", styleHeaderCustomer);
            row = sheet.createRow(rowCount++);

        for (Customer customer : customerList) {
             row = sheet.createRow(rowCount++);
            createCell(row, 0, customer.getCustomerName().toString(), styleHeaderCustomer);
            createCell(row, 1, customer.getCustomerCode().toString(), styleHeaderCustomer);

            row = sheet.createRow(rowCount++);
            createCell(row, 2, "ContactName", styleHeaderCustomer);
            createCell(row, 3, "Address", styleHeaderCustomer);

            for (ContactDelivery contactDelivery : contactDeliveryList) {
                if(contactDelivery.getCustomer().getCustomerId()==customer.getCustomerId()){
                row = sheet.createRow(rowCount++);
                createCell(row, 2, contactDelivery.getContactName().toString(), style);
                createCell(row, 3, contactDelivery.getAddress().toString(), style);
            }}
            row = sheet.createRow(rowCount++);
        }

//         //pricebook

            CellStyle styleHeaderPriceBook = workbook.createCellStyle();
            styleHeaderPriceBook.setAlignment(HorizontalAlignment.CENTER);
            styleHeaderPriceBook.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
            styleHeaderPriceBook.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            XSSFFont font1 = workbook.createFont();
            font.setFontHeight(14);
            style.setFont(font1);
            row = sheet.createRow(rowCount++);
            row = sheet.createRow(rowCount++);
            createCell(row, 0, "PriceBook Name", styleHeaderPriceBook);
            createCell(row, 1, "Is standard price book", styleHeaderPriceBook);
            row = sheet.createRow(rowCount++);

            for (PriceBook priceBook : priceBookList) {
                row = sheet.createRow(rowCount++);
                createCell(row, 0, priceBook.getPriceBookName().toString(), styleHeaderPriceBook);
                createCell(row, 1, priceBook.getIsStandardPriceBook()==true?"standard":"non-standard", styleHeaderPriceBook);

                row = sheet.createRow(rowCount++);
                createCell(row, 2, "ProductName", styleHeaderPriceBook);
                createCell(row, 3, "Price", styleHeaderPriceBook);

                for (PriceBookEntry priceBookEntry : priceBookEntryList) {
                    if(priceBookEntry.getPriceBook().getPriceBookId()==priceBook.getPriceBookId()){
                        row = sheet.createRow(rowCount++);
                        createCell(row, 2, priceBookEntry.getProduct().getProductName().toString(), style);
                        createCell(row, 3, priceBookEntry.getPrice().doubleValue(), style);
                    }}
                row = sheet.createRow(rowCount++);
            }

        }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();
        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
        workbook.close();
        outputStream.close();

    }
}
