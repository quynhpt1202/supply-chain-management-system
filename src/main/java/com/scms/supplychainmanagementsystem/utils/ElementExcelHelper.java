package com.scms.supplychainmanagementsystem.utils;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ElementExcelHelper {
    Long orderId;
    String customerCode;
    String contact;
    int Row;
}
