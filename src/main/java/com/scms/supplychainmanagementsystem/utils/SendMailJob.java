package com.scms.supplychainmanagementsystem.utils;

import com.scms.supplychainmanagementsystem.dto.InvoiceInfoMail;
import com.scms.supplychainmanagementsystem.repository.InvoiceRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.scms.supplychainmanagementsystem.utils.EmailScheduling.jobSendEveryMonth;
import static com.scms.supplychainmanagementsystem.utils.EmailScheduling.jobSendEveryday;

@AllArgsConstructor
@Transactional
@Slf4j
@Service
public class SendMailJob {
    private final InvoiceRepository invoiceRepository;
    private final MailService mailService;

    public HashMap<String, Boolean> sendInvoiceMail(Long warehouseId, String jobName) {
        log.info("[Start SendMailJob - Send Invoice Mail]");
        List<InvoiceInfoMail> invoiceListAll = new ArrayList<>();
        if (jobName.equals(jobSendEveryMonth)) {
            invoiceListAll = getAllInvoiceWhenOrderNotClosed(warehouseId);
        } else if (jobName.equals(jobSendEveryday)) {
            invoiceListAll = getAllInvoiceNearDueDate(warehouseId);
        }
        Map<String, List<InvoiceInfoMail>> invoiceMap = new HashMap<>();
        List<InvoiceInfoMail> invoices = new ArrayList<>();
        for (int i = 0; i < invoiceListAll.size(); i++) {
            if (i != 0 && !invoiceListAll.get(i - 1).getEmail().equals(invoiceListAll.get(i).getEmail())) {
                invoices = new ArrayList<>();
            }
            invoices.add(invoiceListAll.get(i));
            invoiceMap.put(invoiceListAll.get(i).getEmail(), invoices);
        }
        log.info("Get All Invoice Info To Send Mail Successfully");
        String subject = "";
        if (jobName.equals(jobSendEveryMonth)) {
            subject = "[SCMS] Thông tin hoá đơn và dư nợ tháng " + LocalDate.now().getMonthValue();
        } else if (jobName.equals(jobSendEveryday)) {
            subject = "[SCMS] Thông báo nhắc đến hạn thanh toán hoá đơn";
        }
        HashMap<String, Boolean> resultMailSent = mailService.sendMailInvoiceInformation(invoiceMap, subject, "");
        log.info("[End SendMailJob - Send Invoice Mail]");
        return resultMailSent;
    }

    private List<InvoiceInfoMail> getAllInvoiceWhenOrderNotClosed(Long warehouseId) {
        log.info("[Start SendMailJob - getAllInvoiceWhenOrderNotClosed]");
        List<InvoiceInfoMail> infoMailList = invoiceRepository.getAllInvoiceWhenOrderNotClosed(warehouseId);
        log.info("[End SendMailJob - getAllInvoiceWhenOrderNotClosed]");
        return infoMailList;
    }

    private List<InvoiceInfoMail> getAllInvoiceNearDueDate(Long warehouseId) {
        log.info("[Start SendMailJob - getAllInvoiceWhenOrderNotClosed]");
        List<InvoiceInfoMail> infoMailList = invoiceRepository.getAllInvoiceNearDueDate(warehouseId);
        log.info("[End SendMailJob - getAllInvoiceWhenOrderNotClosed]");
        return infoMailList;
    }
}
