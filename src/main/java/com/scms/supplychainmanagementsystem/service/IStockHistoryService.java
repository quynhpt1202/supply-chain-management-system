package com.scms.supplychainmanagementsystem.service;

import com.scms.supplychainmanagementsystem.dto.StockHistoryDto;
import com.scms.supplychainmanagementsystem.entity.StockHistory;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface IStockHistoryService {
    Page<StockHistory> getAllStockHistory(String productName, Long warehouseId, Pageable pageble);

    StockHistory getStockHistoryByIdInWarehouse(Long stockHistoryId);

    void updateStockHistory(Long stockHistoryId, StockHistoryDto stockHistoryDto);

    void saveStockHistory(StockHistoryDto stockHistoryDto);

    void deleteStockHistory(Long stockHistoryId);

    void mapReapExcelDatatoDB(MultipartFile reapExcelDataFile, Long warehouseId) throws IOException;

//    void export(HttpServletResponse response) throws IOException;

}
