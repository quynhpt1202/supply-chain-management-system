package com.scms.supplychainmanagementsystem.service;

import com.scms.supplychainmanagementsystem.entity.Stock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IStockService {
    Page<Stock> getAllStockAdmin(String productName, Long warehouseId, Pageable pageable);
}
