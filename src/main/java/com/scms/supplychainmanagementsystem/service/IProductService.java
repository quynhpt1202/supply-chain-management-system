package com.scms.supplychainmanagementsystem.service;

import com.scms.supplychainmanagementsystem.dto.ProductRequest;
import com.scms.supplychainmanagementsystem.dto.ProductResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IProductService {
    void updateProduct(ProductRequest productRequest);

    void createProduct(ProductRequest productRequest);

    void deleteProductByProductId(Long productId);

    ProductRequest getProductById(Long productId);

    Page<ProductResponse> getAllProducts(String productName, Long categoryId, Long warehouseId, Boolean isActive, Pageable pageable);

    boolean checkProductExist(Long productId);

    void updateProductActive(Long productId, Boolean isActive);

}
