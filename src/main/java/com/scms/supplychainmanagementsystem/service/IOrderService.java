package com.scms.supplychainmanagementsystem.service;

import com.scms.supplychainmanagementsystem.dto.OrderRequest;
import com.scms.supplychainmanagementsystem.dto.OrderResponse;
import com.scms.supplychainmanagementsystem.dto.OrderResponseDetails;
import com.scms.supplychainmanagementsystem.dto.OrderStatusDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface IOrderService {
    void updateOrder(OrderRequest orderRequest, Long orderId);

    Long createOrder(OrderRequest orderRequest);

    void deleteOrderByOrderId(Long orderId);

    OrderResponseDetails getOrderById(Long orderId);

    Page<OrderResponse> getAllOrders(String orderCode, String customerName, Long orderStatusId, Long warehouseId, Pageable pageable);

    List<OrderStatusDto> getAllOrderStatus();

    void updateOrderStatus(Long orderId, Long orderStatusId);

    void mapReapExcelDatatoDB(MultipartFile reapExcelDataFile,Long warehouseId) throws IOException;

    void export(HttpServletResponse response) throws IOException;
}
