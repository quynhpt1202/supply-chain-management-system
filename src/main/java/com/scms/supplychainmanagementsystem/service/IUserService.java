package com.scms.supplychainmanagementsystem.service;

import com.scms.supplychainmanagementsystem.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IUserService {
    void updateUser(UserRequest userRequest, Long userID);

    void saveUser(UserRequest userRequest);

    void deleteUser(Long userId);

    UserResponseDetails getUserById(Long userId);

    void changePassword(ChangePasswordRequest changePasswordRequest);

    List<RoleDto> getAllRoles();

    Page<UserResponse> getAllUsers(String username, Long roleId, Long warehouseId, Pageable pageble);

    void updateUserActive(Long userId, Boolean isActive);
}
