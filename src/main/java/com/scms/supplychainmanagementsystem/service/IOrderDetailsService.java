package com.scms.supplychainmanagementsystem.service;

import com.scms.supplychainmanagementsystem.dto.OrderDetailsRequest;
import com.scms.supplychainmanagementsystem.dto.OrderDetailsResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IOrderDetailsService {
    void updateOrderDetails(OrderDetailsRequest orderDetailsRequest, Long orderDetailId);

    void createOrderDetailsByOrderId(OrderDetailsRequest orderDetailsRequest, Long orderId);

    void deleteOrderDetailsById(Long orderDetailId);

    OrderDetailsResponse getOrderDetailsById(Long orderDetailId);

    Page<OrderDetailsResponse> getAllOrderDetailsByOrderId(Long orderId, String productName, Long orderDetailsStatusId, Pageable pageable);

}
