package com.scms.supplychainmanagementsystem.service;

import com.scms.supplychainmanagementsystem.dto.DashboardResults;

import java.time.LocalDate;

public interface IDashboardService {
    DashboardResults getProductCost(LocalDate fromDate, LocalDate toDate, Long warehouseId);

    DashboardResults getDefectiveProductCost(LocalDate fromDate, LocalDate toDate, Long warehouseId);

    DashboardResults getRevenue(LocalDate fromDate, LocalDate toDate, Long warehouseId);

    DashboardResults getGrossProfit(LocalDate fromDate, LocalDate toDate, Long warehouseId);
}
