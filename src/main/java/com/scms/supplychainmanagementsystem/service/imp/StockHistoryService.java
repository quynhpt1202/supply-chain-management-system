package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.StockHistoryDto;
import com.scms.supplychainmanagementsystem.entity.Product;
import com.scms.supplychainmanagementsystem.entity.StockHistory;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.ProductRepository;
import com.scms.supplychainmanagementsystem.repository.StockHistoryRepository;
import com.scms.supplychainmanagementsystem.repository.StockRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import com.scms.supplychainmanagementsystem.service.IStockHistoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


@AllArgsConstructor
@Transactional
@Slf4j
@Service
public class StockHistoryService implements IStockHistoryService {
    private final UserCommon userCommon;
    private StockHistoryRepository stockHistoryRepository;
    private StockService stockService;
    private StockRepository stockRepository;
    private ProductRepository productRepository;
    private final WarehouseRepository warehouseRepository;

    @Override
    public Page<StockHistory> getAllStockHistory(String productName, Long warehouseId, Pageable pageable) {
        log.info("[Start PurchaseService - Get All Purchase]");
        Page<StockHistory> stockHistoryPage;
        User current = userCommon.getCurrentUser();
        Warehouse wh = current.getWarehouse();
        Long userId = current.getUserId();
        if (current.getRole().getRoleID() == 1) {
            stockHistoryPage = stockHistoryRepository.filterAllWarehouses(productName, warehouseId, pageable);
        } else {
            stockHistoryPage = stockHistoryRepository.filterInOneWarehouse(productName, wh.getWarehouseID(), pageable);
        }
        log.info("[End PurchaseService - Get All Purchase]");
        return stockHistoryPage;
    }

    @Override
    public StockHistory getStockHistoryByIdInWarehouse(Long stockHistoryId) {
        User currentUser = userCommon.getCurrentUser();
        StockHistory stockHistory = new StockHistory();
        if (currentUser.getRole().getRoleID() != 1) {
            stockHistory = stockHistoryRepository.findByStockHistoryId(stockHistoryId, currentUser.getWarehouse().getWarehouseID());
        } else {
            stockHistory = stockHistoryRepository.findByStockHistoryIdAdmin(stockHistoryId);
        }
        return stockHistory;
    }

    @Override
    public void updateStockHistory(Long stockHistoryId, StockHistoryDto stockHistoryDto) {
        log.info("[Start PurchaseService - UpdatePurchase  to database ]");
        log.info("[Start get current user]");
        User currentUser = userCommon.getCurrentUser();
        log.info("[End get current user : " + currentUser.getUsername() + "]");
        StockHistory stockHistoryOld = stockHistoryRepository.getById(stockHistoryId);

        Product product = productRepository.getById(stockHistoryDto.getProductId());
        Double QuantityOld = stockHistoryRepository.getById(stockHistoryId).getStockInQuantity(); //35
        //35-30=5 >100
        if (QuantityOld - stockHistoryDto.getStockInQuantity() > stockRepository.findByProduct(productRepository.getById(stockHistoryDto.getProductId())).getAvailableQuantity()) {
            throw new AppException("THE_QUANTITY_REDUCED_EXCEEDS_THE_QUANTITY_IN_WAREHOUSE");
        }

        if (currentUser.getRole().getRoleID() != 1) {
            if (currentUser.getWarehouse().getWarehouseID() != stockHistoryRepository.findByStockHistoryIdAdmin(stockHistoryId).getProduct().getWarehouse().getWarehouseID()) {
                throw new AppException("CAN_NOT_UPDATE_IN_ANOTHER_WAREHOUSE");
            }
        }
        StockHistory stockHistory = StockHistory.builder()
                .stockHistoryID(stockHistoryId)
                .createdDate(stockHistoryOld.getCreatedDate())
                .stockInQuantity(stockHistoryDto.getStockInQuantity())
                .unitCostPrice(stockHistoryDto.getUnitCostPrice())
                .lastModifiedDate(Instant.now())
                .product(product)
                .createdBy(stockHistoryOld.getCreatedBy())
                .lastModifiedBy(currentUser)
                .warehouse(stockHistoryOld.getWarehouse())
                .build();
        log.info("[Start Update PurchaseService  to database]");
        stockHistoryRepository.save(stockHistory);
        stockService.updateStockQuantity(product.getProductId(), stockHistoryDto.getStockInQuantity() - QuantityOld);
        log.info("[Start PurchaseService - UpdatePurchase  to database ]");
        log.info("[End PurchaseService -UpdatePurchase ]");
    }

    @Override
    public void saveStockHistory(StockHistoryDto stockHistoryDto) {
        log.info("[Start StockHistoryService - saveStockHistory  to database ]");
        log.info("[Start get current user]");
        User currentUser = userCommon.getCurrentUser();
        log.info("[End get current user : " + currentUser.getUsername() + "]");

        Warehouse warehouse = new Warehouse();

        Product product = new Product();
        product = productRepository.getById(stockHistoryDto.getProductId());
        if (currentUser.getRole().getRoleID() != 1) {
            warehouse = currentUser.getWarehouse();
        } else {
            warehouse = warehouseRepository.getById(stockHistoryDto.getWarehouseId());
        }

        StockHistory stockHistory = StockHistory.builder()
                .createdDate(Instant.now())
                .stockInQuantity(stockHistoryDto.getStockInQuantity())
                .unitCostPrice(stockHistoryDto.getUnitCostPrice())
                .product(product)
                .createdBy(currentUser)
                .warehouse(warehouse)
                .build();
        log.info("[Start save PurchaseService  to database]");
        stockHistoryRepository.saveAndFlush(stockHistory);
        stockService.updateStockQuantity(product.getProductId(), stockHistoryDto.getStockInQuantity());
        log.info("[Start PurchaseService - savePurchase  to database ]");
        log.info("[End PurchaseService - savePurchase ]");
    }

    @Override
    public void deleteStockHistory(Long stockHistoryId) {
        Double quantityOld = stockHistoryRepository.getById(stockHistoryId).getStockInQuantity();
        Long productId = stockHistoryRepository.getById(stockHistoryId).getProduct().getProductId();
        Product product = stockHistoryRepository.getById(stockHistoryId).getProduct();
        User currentUser = userCommon.getCurrentUser();
        if (currentUser.getRole().getRoleID() != 1) {
            if (currentUser.getWarehouse().getWarehouseID() != stockHistoryRepository.getById(stockHistoryId).getProduct().getWarehouse().getWarehouseID()) {
                throw new AppException("CAN_NOT_DELETE_IN_ANOTHER_WAREHOUSE");
            } else {
                if (quantityOld > stockRepository.findByProduct(product).getAvailableQuantity()) {
                    throw new AppException("THE_QUANTITY_IN_WAREHOUSE_IS_LESS_THAN_THE_QUANTITY_YOU_WANT_TO_DELETE");
                } else {
                    stockHistoryRepository.deleteStockHistoryAdmin(stockHistoryId);
                    stockService.updateStockQuantity(productId, -quantityOld);
                }
            }
        } else {
            if (quantityOld > stockRepository.findByProduct(product).getAvailableQuantity()) {
                throw new AppException("THE_QUANTITY_IN_WAREHOUSE_IS_LESS_THAN_THE_QUANTITY_YOU_WANT_TO_DELETE");
            } else {
                stockHistoryRepository.deleteStockHistoryAdmin(stockHistoryId);
                stockService.updateStockQuantity(productId, -quantityOld);
            }
        }
    }

    @Override
    public void mapReapExcelDatatoDB(MultipartFile reapExcelDataFile, Long warehouseId) throws IOException {
        User currentUser = userCommon.getCurrentUser();
        log.info("[End get current user : " + currentUser.getUsername() + "]");

        if (currentUser.getRole().getRoleID() != 1) {
            if (currentUser.getWarehouse().getWarehouseID() != warehouseId) {
                throw new AppException("CAN_NOT_ADD_IN_ANOTHER_WAREHOUSE");
            }
        }


        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        System.out.println("so hang" + worksheet.getPhysicalNumberOfRows());
        List<StockHistory> stockHistoriesList = new ArrayList<>();

        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);

            Product product = new Product();
            Double stockInQuantity = null;
            Double unitCostPrice = null;

            if (productRepository.getByProductNameAndWarehouseWarehouseID(row.getCell(0).getStringCellValue(), warehouseId) != null) {
                product = productRepository.getByProductNameAndWarehouseWarehouseID(row.getCell(0).getStringCellValue(), warehouseId);

                if (row.getCell(1) != null) {
                    stockInQuantity = row.getCell(1).getNumericCellValue();
                } else {
                    ex(" stockInQuantity ", i + 1);
                }

                if (row.getCell(1) != null) {
                    unitCostPrice = row.getCell(2).getNumericCellValue();
                } else {
                    ex(" stockInQuantity ", i + 1);
                }
                StockHistory stockHistory = StockHistory.builder()
                        .stockInQuantity(stockInQuantity)
                        .unitCostPrice(unitCostPrice)
                        .product(product)
                        .createdBy(currentUser)
                        .createdDate(Instant.now())
                        .warehouse(warehouseRepository.getById(warehouseId))
                        .build();
                stockHistoriesList.add(stockHistory);
            } else {
                throw new AppException("PRODUCT_NOT_FOUND" + row.getCell(0).getStringCellValue() + "IN_WAREHOUSE");
            }

        }
        for (StockHistory stockHistory : stockHistoriesList) {
            stockHistoryRepository.saveAndFlush(stockHistory);
            stockService.updateStockQuantity(stockHistory.getProduct().getProductId(), stockHistory.getStockInQuantity());
        }
    }


    public String ex(String loi, int index) throws IOException {
        throw new AppException("ERROR_INPUT_DATA_" + loi + "_IN_ROW_" + index);
    }



}

