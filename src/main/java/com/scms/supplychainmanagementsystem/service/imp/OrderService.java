package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.GenerateCode;
import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.*;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.*;
import com.scms.supplychainmanagementsystem.service.IOrderService;
import com.scms.supplychainmanagementsystem.utils.ElementExcelHelper;
import com.scms.supplychainmanagementsystem.utils.ExcelElementCheck;
import com.scms.supplychainmanagementsystem.utils.MailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class OrderService implements IOrderService {
    private final OrderRepository orderRepository;
    private final UserCommon userCommon;
    private final WarehouseRepository warehouseRepository;
    private final ContactDeliveryRepository contactDeliveryRepository;
    private final OrderStatusRepository orderStatusRepository;
    private final OrderDetailsRepository orderDetailsRepository;
    private final StockRepository stockRepository;
    private final GenerateCode generateCode;
    private final InvoiceService invoiceService;
    private final InvoiceRepository invoiceRepository;
    private final MailService mailService;
    private final ProductRepository productRepository;
    private final PriceBookRepository priceBookRepository;
    private final PriceBookEntryRepository priceBookEntryRepository;
    private final OrderDetailsService orderDetailsService;
    private final OrderDetailSttRepository orderDetailSttRepository;
    private final CustomerRepository customerRepository;

    @Override
    public void updateOrder(OrderRequest orderRequest, Long orderId) {
        log.info("[Start OrderService - updateOrder ID = " + orderId + "]");
        if (checkAccessOrder(orderId)) {
            Order order = orderRepository.getById(orderId);
//            if (order.getOrderStatus().getOrderStatusID() == 5) {
//                throw new AppException("Can't update closed order");
//            }
            User current = userCommon.getCurrentUser();
            if (current.getRole().getRoleID() == 1) {
                order.setWarehouse(warehouseRepository.findById(orderRequest.getWarehouseId())
                        .orElseThrow(() -> new AppException("WAREHOUSE_NOT_FOUND")));
            }
            order.setContactDelivery(contactDeliveryRepository.findById(orderRequest.getContactId())
                    .orElseThrow(() -> new AppException("Contact Delivery not found")));
            order.setNote(orderRequest.getNote());
            order.setLastModifiedBy(current);
            order.setLastModifiedDate(Instant.now());
            log.info("[Start Save Order " + orderId + " to database]");
            orderRepository.saveAndFlush(order);
            log.info("[End Save Order " + orderId + " to database]");
            log.info("[End OrderService - updateOrder ID = " + orderId + "]");
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }

    }

    @Override
    public Long createOrder(OrderRequest orderRequest) {
        log.info("[Start OrderService - createOrder]");
        User current = userCommon.getCurrentUser();
        Order order = new Order();
        if (orderRequest.getContactId() == null) {
            throw new AppException("NOT_FILL_REQUIRED_FIELD");
        }
        order.setContactDelivery(contactDeliveryRepository.getById(orderRequest.getContactId()));

        if (current.getRole().getRoleID() == 1) {
            order.setWarehouse(warehouseRepository.findById(orderRequest.getWarehouseId())
                    .orElseThrow(() -> new AppException("WAREHOUSE_NOT_FOUND")));
        } else {
            order.setWarehouse(current.getWarehouse());
        }
        order.setNote(orderRequest.getNote());
        order.setCreatedDate(Instant.now());

        order.setOrderStatus(orderStatusRepository.getById(1L));
        order.setCreatedBy(current);
        log.info("[Start Save Order to database]");
        orderRepository.saveAndFlush(order);
        log.info("Generate Order Code");
        order.setOrderCode(generateCode.genCodeByDate("DH", order.getOrderId()));
        orderRepository.saveAndFlush(order);
        log.info("[End Save Order ID = " + order.getOrderId() + " to database]");
        log.info("[End OrderService - createOrder]");
        return order.getOrderId();
    }

    @Override
    public void deleteOrderByOrderId(Long orderId) {
        log.info("[Start OrderService - deleteOrderByOrderId = " + orderId + "]");
        if (checkAccessOrder(orderId)) {
            Order order = orderRepository.getById(orderId);
            if (order.getOrderStatus().getOrderStatusID() != 1) {
                throw new AppException("ORDER_STATUS_NOT_ALLOW");
            }
            orderDetailsRepository.deleteAllByOrder(order);
            orderRepository.deleteById(orderId);
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        log.info("[End OrderService - deleteOrderByOrderId = " + orderId + "]");

    }

    @Override
    public OrderResponseDetails getOrderById(Long orderId) {
        log.info("[Start OrderService - getOrderById = " + orderId + "]");
        OrderResponseDetails orderResponseDetails = new OrderResponseDetails();
        if (checkAccessOrder(orderId)) {
            Order order = orderRepository.getById(orderId);
            orderResponseDetails.setOrderId(orderId);
            orderResponseDetails.setOrderStatusId(order.getOrderStatus().getOrderStatusID());
            orderResponseDetails.setOrderCode(order.getOrderCode());
            orderResponseDetails.setWarehouseId(order.getWarehouse().getWarehouseID());
            orderResponseDetails.setContactId(order.getContactDelivery().getContactID());
            orderResponseDetails.setCustomerId(order.getContactDelivery().getCustomer().getCustomerId());
            orderResponseDetails.setCreatedDate(order.getCreatedDate());
            orderResponseDetails.setLastModifiedDate(order.getLastModifiedDate());
            orderResponseDetails.setNote(order.getNote());
            orderResponseDetails.setCreatedBy(order.getCreatedBy().getUsername());
            orderResponseDetails.setLastModifiedBy(order.getLastModifiedBy() != null ? order.getLastModifiedBy().getUsername() : null);
            orderResponseDetails.setTotalAmount(orderRepository.sumTotalAmount(order) == null ? 0 : orderRepository.sumTotalAmount(order));
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        log.info("[End OrderService - getOrderById = " + orderId + "]");
        return orderResponseDetails;
    }

    @Override
    public Page<OrderResponse> getAllOrders(String orderCode, String customerName, Long orderStatusId, Long warehouseId, Pageable pageable) {
        log.info("[Start OrderService - Get All Orders]");
        Page<OrderResponse> orderPage;
        User current = userCommon.getCurrentUser();
        Warehouse wh = current.getWarehouse();
        if (current.getRole().getRoleID() == 1) {
            orderPage = orderRepository.filterAllWarehouses(orderCode, customerName, orderStatusId, warehouseId, pageable);
        } else {
            orderPage = orderRepository.filterInOneWarehouse(orderCode, customerName, orderStatusId, wh.getWarehouseID(), pageable);
        }
//        Page<OrderResponse> orderResponseAllPage = orderPage.map(o -> OrderResponse.builder()
//                .orderId(o.getOrderId())
//                .contactName(o.getContactDelivery().getContactName())
//                .orderCode(o.getOrderCode())
//                .createdDate(o.getCreatedDate())
//                .phone(o.getContactDelivery().getPhone())
//                .customerName(o.getContactDelivery().getCustomer().getCustomerName())
//                .status(o.getOrderStatus().getStatus())
//                .orderStatusId(o.getOrderStatus().getOrderStatusID())
//                .warehouseName(o.getWarehouse().getWarehouseName())
//                .note(o.getNote())
//                .totalAmount(orderRepository.sumTotalAmount(o) == null ? 0 : orderRepository.sumTotalAmount(o))
//                .build());
        log.info("[End OrderService - Get All Orders]");
        return orderPage;
    }

    @Override
    public List<OrderStatusDto> getAllOrderStatus() {
        log.info("[Start OrderService - getAllOrderStatus]");
        List<OrderStatus> orderStatusList = orderStatusRepository.findAll();
        List<OrderStatusDto> orderStatusDtoList = orderStatusList.stream()
                .map(o -> new OrderStatusDto(o.getOrderStatusID(), o.getStatus())).collect(Collectors.toList());
        log.info("[Start OrderService - getAllOrderStatus]");
        return orderStatusDtoList;
    }

    @Override
    public void updateOrderStatus(Long orderId, Long orderStatusId) {
        log.info("[Start OrderService - updateOrderStatus Order ID = " + orderId + " to " + orderStatusId + "]");
        if (checkAccessOrder(orderId)) {
            Order order = orderRepository.getById(orderId);
            Long status = order.getOrderStatus().getOrderStatusID();
            if (order.getOrderStatus().getOrderStatusID().equals(orderStatusId)) {
                throw new AppException("ORDER_STATUS_HAS_NOT_UPDATED");
            }
            if (!checkAllowUpdateStt(status, orderStatusId)) {
                throw new AppException("INVALID_CHOOSE_ORDER_STATUS");
            }
            if (orderStatusId == 3) {
                checkAndUpdateOrderSuccess(order);
            } else if (orderStatusId == 1 && status == 3) {
                updateOrderBack(order);
            } else {
                if (orderStatusId == 5) {
                    updateInvoiceClosedOrder(order);
                }
                order.setOrderStatus(orderStatusRepository.getById(orderStatusId));
                orderRepository.saveAndFlush(order);
            }
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        log.info("[Start OrderService -  updateOrderStatus Order ID = " + orderId + " to " + orderStatusId + "]");
    }

    private void updateInvoiceClosedOrder(Order order) {
        Invoice invoice = invoiceRepository.findByOrderId(order.getOrderId());
        invoice.setTotalAmountPaid(invoice.getTotalAmount());
        invoiceRepository.saveAndFlush(invoice);
        log.info("Update TotalAmountPaid change to " + invoice.getTotalAmountPaid() + " when closed order successfully");
    }

    private boolean checkAccessOrder(Long orderId) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new AppException("ORDER_NOT_FOUND"));
        User current = userCommon.getCurrentUser();
        if (current.getRole().getRoleID() == 1) {
            return true;
        }
        if (order.getWarehouse() != null && current.getWarehouse() != null) {
            return order.getWarehouse().getWarehouseID().equals(current.getWarehouse().getWarehouseID());
        }
        return false;
    }

    @Async
    void checkAndUpdateOrderSuccess(Order order) {
        if (!orderDetailsRepository.existsByOrder(order)) {
            throw new AppException("ORDER_DETAILS_DOES_NOT_EXIST");
        }
        if (orderDetailsRepository.existsByOrderIdAndNotEnoughStock(order.getOrderId())) {
            order.setOrderStatus(orderStatusRepository.getById(2L));
        } else {
            List<OrderDetails> orderDetailsList = orderDetailsRepository.findAllByOrderId(order.getOrderId());
            for (OrderDetails o : orderDetailsList) {
                //Update Order Status Details from 1 to 2 When Submit Order
                if (o.getOrderDetailsStatus().getOrderDetailStatusID().equals(1L)) {
                    o.setOrderDetailsStatus(orderDetailSttRepository.getById(2L));
                    orderDetailsRepository.saveAndFlush(o);
                    log.info("[Update Order Status Details to 2 of Order Code = " + o.getOrder().getOrderCode() + "]");
                }
                stockRepository.minusStockQuantityByOrder(o.getQuantity(), o.getProduct().getProductId());
                log.info("[Minus Stock quantity]");
            }
            order.setOrderStatus(orderStatusRepository.getById(3L));
            orderRepository.saveAndFlush(order);
            // TODO:
            updateOrderDetailsStatusWhenOrderSubmit(order.getWarehouse().getWarehouseID(), order.getOrderId());
            Double totalAmount = orderRepository.sumTotalAmount(order);
            InvoiceDto invoiceDto = new InvoiceDto();
            invoiceDto.setOrderId(order.getOrderId());
            invoiceDto.setTotalAmount(totalAmount);
            invoiceDto.setTotalAmountPaid(0.0);
            invoiceDto.setWarehouseId(order.getWarehouse().getWarehouseID());
            invoiceService.saveInvoice(invoiceDto);
            log.info("Create Invoice Successfully");
            List<OrderInformationDto> orderInformationDtoList = convertListOrderDetailsEntityToDto(orderDetailsList);
            sendOrderMail(order, orderInformationDtoList, totalAmount);
        }
    }

    public void sendOrderMail(Order order, List<OrderInformationDto> orderInformationDtoList, Double totalAmount) {
        OrderInformationEmail orderInformationEmail = OrderInformationEmail.builder()
                .subject("[SCMS] Đặt hàng thành công")
                .recipient(order.getContactDelivery().getCustomer().getEmail())
                .orderCode(order.getOrderCode())
                .customerName(order.getContactDelivery().getCustomer().getCustomerName())
                .content("Cảm ơn bạn đã đặt sản phẩm của chúng tôi")
                .orderInformationDtoList(orderInformationDtoList)
                .totalAmount(totalAmount).build();
        mailService.sendMailOrderInformation(orderInformationEmail);
    }

    @Async
    void updateOrderBack(Order order) {
        List<OrderDetails> orderDetailsList = orderDetailsRepository.findAllByOrderId(order.getOrderId());
        for (OrderDetails o : orderDetailsList) {
            stockRepository.plusStockQuantityByOrder(o.getQuantity(), o.getProduct().getProductId());
            log.info("Back Stock quantity");
            Double tempQuantity = stockRepository.getAvailableQuantityByProductId(o.getProduct().getProductId());
            List<OrderDetails> listOrderDetailPending = orderDetailsRepository.getOrderDetailsPendingByProductId(o.getProduct().getProductId(), tempQuantity);
            //Update all order details status order by order status = 2 and create date asc if enough quantity
            int i = 0;
            while (i < listOrderDetailPending.size() && tempQuantity > 0) {
                OrderDetails o1 = listOrderDetailPending.get(i);
                o1.setOrderDetailsStatus(orderDetailSttRepository.getById(2L));
                orderDetailsRepository.saveAndFlush(o1);
                tempQuantity -= o1.getQuantity();
                i++;
            }
        }
        order.setOrderStatus(orderStatusRepository.getById(1L));
        orderRepository.saveAndFlush(order);
        log.info("Set order status = 1 successfully");
        // TODO:
        updateOrderDetailsStatusWhenOrderBack(order.getWarehouse().getWarehouseID(), order.getOrderId());
        log.info("Start Delete Invoice by order ID = " + order.getOrderId());
        invoiceRepository.deleteInvoiceByOrder(order);
        log.info("End Delete Invoice by order ID = " + order.getOrderId());
        List<OrderInformationDto> orderInformationDtoList = convertListOrderDetailsEntityToDto(orderDetailsList);
        OrderInformationEmail orderInformationEmail = OrderInformationEmail.builder()
                .subject("[SCMS] Huỷ đơn hàng")
                .recipient(order.getContactDelivery().getCustomer().getEmail())
                .orderCode(order.getOrderCode())
                .customerName(order.getContactDelivery().getCustomer().getCustomerName())
                .content("Huỷ đơn hàng thành công.")
                .orderInformationDtoList(orderInformationDtoList)
                .totalAmount(orderRepository.sumTotalAmount(order)).build();
        mailService.sendMailOrderInformation(orderInformationEmail);
    }

    void updateOrderDetailsStatusWhenOrderSubmit(Long warehouseId, Long orderId) {
        log.info("[Start updateOrderDetailsStatus]");
        List<OrderDetails> orderDetailsList = orderDetailsRepository.getAllOrderDetailWhenOrderNotConfirm(warehouseId, orderId);
        for (OrderDetails o : orderDetailsList) {
            if (!stockRepository.checkQtyAvailable(o.getProduct().getProductId(), o.getQuantity())) {
                //Not Enough stock
                o.setOrderDetailsStatus(orderDetailSttRepository.getById(1L));
                orderDetailsRepository.saveAndFlush(o);
                log.info("[Change Order Details Status to 1 of Order Code = " + o.getOrder().getOrderCode() + "]");
            }
        }
        log.info("[End updateOrderDetailsStatus]");
    }

    void updateOrderDetailsStatusWhenOrderBack(Long warehouseId, Long orderId) {
        log.info("[Start updateOrderDetailsStatus]");
        List<OrderDetails> orderDetailsList = orderDetailsRepository.getAllOrderDetailWhenOrderNotConfirm(warehouseId, orderId);
        for (OrderDetails o : orderDetailsList) {
            if (stockRepository.checkQtyAvailable(o.getProduct().getProductId(), o.getQuantity())) {
                //Enough stock
                o.setOrderDetailsStatus(orderDetailSttRepository.getById(2L));
                orderDetailsRepository.saveAndFlush(o);
                log.info("[Change Order Details Status to 2 of Order Code = " + o.getOrder().getOrderCode() + "]");
            }
        }
        log.info("[End updateOrderDetailsStatus]");
    }

    private List<OrderInformationDto> convertListOrderDetailsEntityToDto(List<OrderDetails> orderDetailsList) {
        return orderDetailsList.stream()
                .map(o -> new OrderInformationDto(o.getProduct().getProductName()
                        , o.getProduct().getCategory().getCategoryName()
                        , o.getProduct().getQuantityUnitOfMeasure()
                        , String.format("%.0f", o.getQuantity())
                        , o.getCurrentPrice()
                        , o.getCurrentPrice() * o.getQuantity())
                ).collect(Collectors.toList());
    }

    private boolean checkAllowUpdateStt(Long status, Long statusSelected) {
        if (status == 1 && statusSelected == 3) {
            return true;
        } else if (status == 2 && (statusSelected != 4 && statusSelected != 5)) {
            return true;
        } else if (status == 3 && (statusSelected != 2)) {
            return true;
        } else if (status == 4 && statusSelected == 5) {
            return true;
        }
        return false;
    }

    @Override
    public void mapReapExcelDatatoDB(MultipartFile reapExcelDataFile, Long warehouseId) throws IOException {

        User currentUser = userCommon.getCurrentUser();
        log.info("[End get current user : " + currentUser.getUsername() + "]");

        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);

        if (currentUser.getRole().getRoleID() != 1) {
            if (currentUser.getWarehouse().getWarehouseID() != warehouseId) {
                throw new AppException("YOU_ARE_NOT_IN_THIS_WAREHOUSE");
            }
        }
        Warehouse warehouse = warehouseRepository.getById(warehouseId);

        System.out.println("so hang" + worksheet.getPhysicalNumberOfRows());
        if(worksheet.getPhysicalNumberOfRows()<=1){
            throw new AppException("FILE_EMPTY");
        }
        List<Order> orderList = new ArrayList<>();
        List<OrderDetails> orderDetailsList = new ArrayList<>();
        List<ElementExcelHelper> listElement = new ArrayList<>();
        List<ExcelElementCheck> checkList = new ArrayList<>();



        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);

            //Contact
            ContactDelivery contact = null;
            Customer customer = null;
            Boolean check = false;
            if (row.getCell(0) == null) {
                ex("CUSTOMER", i + 1);
            } else {
                customer = customerRepository.getByCustomerCodeAndWarehouseID(row.getCell(0).getStringCellValue(), warehouseId);
                if (customer == null) {
                    ex("CUSTOMER_DOES_NOT_EXIST", i + 1);
                }
            }
            if (row.getCell(1) == null) {
                ex("CONTACT_NAME", i + 1);
            } else {
                contact = contactDeliveryRepository.getByContactNameAndCustomerId(row.getCell(1).getStringCellValue(), customer.getCustomerId());
                if (contact == null) {
                    ex("CONTACT_NAME_DOES_NOT_EXIST", i + 1);
                } else {
                    if (orderList.size() == 0) {
                        Order order = Order.builder()
                                .contactDelivery(contact)
                                .note(row.getCell(5) != null ? row.getCell(5).getStringCellValue() : ex("NOTE", i + 1))
                                .createdDate(Instant.now())
                                .createdBy(currentUser)
                                .orderStatus(orderStatusRepository.getById(1L))
                                .warehouse(warehouse)
                                .build();
                        orderList.add(order);
                        ElementExcelHelper elementExcelHelper = ElementExcelHelper.builder()
                                .customerCode(customer.getCustomerCode())
                                .contact(contact.getContactName())
                                .Row(i)
                                .build();
                        listElement.add(elementExcelHelper);

                    } else {
                        for (ElementExcelHelper element : listElement) {
                            if (row.getCell(0).getStringCellValue().equals(element.getCustomerCode()) & row.getCell(1).getStringCellValue().equals(element.getContact())) {
                                check = true;
                                if (check == true) {
                                    break;
                                }
                            }
                        }
                        if (check == false) {

                            Order order = Order.builder()
                                    .contactDelivery(contact)
                                    .note(row.getCell(5) != null ? row.getCell(5).getStringCellValue() : ex("NOTE", i + 1))
                                    .createdDate(Instant.now())
                                    .createdBy(currentUser)
                                    .orderStatus(orderStatusRepository.getById(1L))
                                    .warehouse(warehouse)
                                    .build();
                            orderList.add(order);

                            ElementExcelHelper elementExcelHelper = ElementExcelHelper.builder()
                                    .customerCode(customer.getCustomerCode())
                                    .contact(contact.getContactName())
                                    .Row(i)
                                    .build();
                            listElement.add(elementExcelHelper);

                            for (Order orderr : orderList) {
                                orderr.toString();
                            }

                        }

                    }

                }
            }
        }

        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);

            PriceBook priceBook = null;
            Product product = null;
            priceBook = priceBookRepository.getByPriceBookNameAndWarehouseWarehouseID(row.getCell(4) != null ? row.getCell(4).getStringCellValue() : ex("NOTE", i + 1), warehouseId);
            if (priceBook != null) {
                if (row.getCell(2) != null) {
                    product = productRepository.getByProductNameAndWarehouseWarehouseID(row.getCell(2).getStringCellValue(), warehouseId);
                    if (product != null) {
                        if (!priceBookEntryRepository.existProductActiveInPriceBook(product.getProductId(), priceBook.getPriceBookId())) {
                            ex("PRICEBOOK_DOES_NOT_CONTAIN_THIS_PRODUCT", i + 1);
                        }
                    } else {
                        ex("PRODUCT_DOES_NOT_EXIST", i + 1);
                    }
                } else {
                    ex("PRODUCT", i + 1);
                }
            } else {
                ex("PRICEBOOK_DOES_NOT_EXIST", i + 1);
            }

            Double quantity = Double.parseDouble(row.getCell(2) != null ? row.getCell(3).getRawValue() : ex(" QUANTITY ", i + 1));
            OrderDetailsStatus orderDetailsStatus = null;
            if (orderDetailsService.checkOrderItemQtyAvailable(product.getProductId(), quantity)) {
                //Enough stock
                orderDetailsStatus = orderDetailSttRepository.getById(2L);
            } else {
                //Not Enough stock
                orderDetailsStatus = orderDetailSttRepository.getById(1L);
            }
            OrderDetails orderDetails = OrderDetails.builder()
                    .priceBook(priceBook)
                    .quantity(Double.parseDouble(row.getCell(3) != null ? row.getCell(3).getRawValue() : ex(" QUANTITY ", i + 1)))
                    .product(product)
                    .orderDetailsStatus(orderDetailsStatus)
                    .currentPrice(priceBookEntryRepository.getPriceByPriceBookIdAndProductId(product.getProductId(), priceBook.getPriceBookId()))
                    .build();
            orderDetailsList.add(orderDetails);
            ExcelElementCheck excelElementCheck = ExcelElementCheck.builder()
                    .row(i)
                    .product(product.getProductName())
                    .priceBook(priceBook.getPriceBookName())
                    .build();
            checkList.add(excelElementCheck);

        }


        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);

            for (int j = 0; j < listElement.size(); j++) {
                if (row.getCell(1).getStringCellValue().equals(listElement.get(j).getContact()) & row.getCell(0).getStringCellValue().equals(listElement.get(j).getCustomerCode())) {
                    checkList.get(i-1).setCustomerCode(listElement.get(j).getCustomerCode());
                    checkList.get(i-1).setContact(listElement.get(j).getContact());
                }
            }
        }

        for (int i=0;i<checkList.size();i++) {
            for (int j=0;j<checkList.size();j++) {
                if(i==j){
                    continue;
                }
              if(checkList.get(i).getCustomerCode().equals(checkList.get(j).getCustomerCode()) && checkList.get(i).getContact().equals(checkList.get(j).getContact()) && checkList.get(i).getPriceBook().equals(checkList.get(j).getPriceBook()) && checkList.get(i).getProduct().equals(checkList.get(j).getProduct())){
                  exDuplicate("DUPLICATE_PRODUCT_IN_1_ORDER",checkList.get(i).getRow(),checkList.get(j).getRow());
              }
            }
        }


        for (int j = 0; j < orderList.size(); j++) {
            orderRepository.save(orderList.get(j));
            listElement.get(j).setOrderId(orderList.get(j).getOrderId());
            orderList.get(j).setOrderCode(generateCode.genCodeByDate("DH", orderList.get(j).getOrderId()));
            orderRepository.save(orderList.get(j));
        }
        for (int i = 1; i < worksheet.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = worksheet.getRow(i);

            for (int j = 0; j < orderList.size(); j++) {
                if (row.getCell(1).getStringCellValue().equals(orderList.get(j).getContactDelivery().getContactName()) & row.getCell(0).getStringCellValue().equals(listElement.get(j).getCustomerCode())) {
                    orderDetailsList.get(i - 1).setOrder(orderList.get(j));
                }
            }
        }
        for (OrderDetails orderDetails : orderDetailsList) {
            orderDetailsRepository.saveAndFlush(orderDetails);
        }
    }

    public String ex(String loi, int index) throws IOException {
        throw new AppException("ERROR_INPUT_DATA_" + loi + "_IN_ROW_" + index);
    }

    public String exDuplicate(String loi, int index,int index2) throws IOException {
        throw new AppException("ERROR_INPUT_DATA : " + loi + "_IN_ROW_" + index +"_AND_ROW_"+index2);
    }


    @Override
    public void export(HttpServletResponse response) throws IOException {
//        writeHeaderLine();
//        writeDataLines();
//
//        ServletOutputStream outputStream = response.getOutputStream();
//        workbook.write(outputStream);
//        workbook.close();
//
//        outputStream.close();

    }
}
