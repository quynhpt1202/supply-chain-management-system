package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.DashboardResponse;
import com.scms.supplychainmanagementsystem.dto.DashboardResults;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.repository.InventoryRepository;
import com.scms.supplychainmanagementsystem.repository.InvoiceRepository;
import com.scms.supplychainmanagementsystem.repository.StockHistoryRepository;
import com.scms.supplychainmanagementsystem.service.IDashboardService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class DashboardService implements IDashboardService {

    private final StockHistoryRepository stockHistoryRepository;
    private final InventoryRepository inventoryRepository;
    private final InvoiceRepository invoiceRepository;
    private final UserCommon userCommon;

    @Override
    public DashboardResults getProductCost(LocalDate fromDate, LocalDate toDate, Long warehouseId) {
        log.info("Start DashboardService - getProductCost " + fromDate + " to " + toDate);
        User currentUser = userCommon.getCurrentUser();
        List<DashboardResponse> response = stockHistoryRepository.getProductCost(fromDate, toDate
                , currentUser.getRole().getRoleID() != 1 ? currentUser.getWarehouse().getWarehouseID() : warehouseId);
        Double total = stockHistoryRepository.getSumProductCost(fromDate, toDate, warehouseId);
        DashboardResults results = new DashboardResults(response, total);
        log.info("End DashboardService - getProductCost " + fromDate + " to " + toDate);
        return results;
    }

    @Override
    public DashboardResults getDefectiveProductCost(LocalDate fromDate, LocalDate toDate, Long warehouseId) {
        log.info("Start DashboardService - getDefectiveProductCost " + fromDate + " to " + toDate);
        User currentUser = userCommon.getCurrentUser();
        List<DashboardResponse> response = inventoryRepository.getDefectiveProductCost(fromDate, toDate
                , currentUser.getRole().getRoleID() != 1 ? currentUser.getWarehouse().getWarehouseID() : warehouseId);
        Double total = inventoryRepository.getSumDefectiveProductCost(fromDate, toDate, warehouseId);
        DashboardResults results = new DashboardResults(response, total);
        log.info("End DashboardService - getDefectiveProductCost " + fromDate + "to" + toDate);
        return results;
    }

    @Override
    public DashboardResults getRevenue(LocalDate fromDate, LocalDate toDate, Long warehouseId) {
        log.info("Start DashboardService - getDefectiveProductCost " + fromDate + " to " + toDate);
        User currentUser = userCommon.getCurrentUser();
        List<DashboardResponse> response = invoiceRepository.getRevenue(fromDate, toDate
                , currentUser.getRole().getRoleID() != 1 ? currentUser.getWarehouse().getWarehouseID() : warehouseId);
        Double total = invoiceRepository.getSumRevenue(fromDate, toDate, warehouseId);
        DashboardResults results = new DashboardResults(response, total);
        log.info("End DashboardService - getDefectiveProductCost " + fromDate + " to " + toDate);
        return results;
    }

    @Override
    public DashboardResults getGrossProfit(LocalDate fromDate, LocalDate toDate, Long warehouseId) {
        log.info("Start DashboardService - getGrossProfit " + fromDate + " to " + toDate);
        User currentUser = userCommon.getCurrentUser();
        List<DashboardResponse> response = invoiceRepository.getGrossProfit(fromDate, toDate
                , currentUser.getRole().getRoleID() != 1 ? currentUser.getWarehouse().getWarehouseID() : warehouseId);
        Double total = invoiceRepository.getSumGrossProfit(fromDate, toDate, warehouseId);
        DashboardResults results = new DashboardResults(response, total);
        log.info("End DashboardService - getGrossProfit " + fromDate + " to " + toDate);
        return results;
    }
}
