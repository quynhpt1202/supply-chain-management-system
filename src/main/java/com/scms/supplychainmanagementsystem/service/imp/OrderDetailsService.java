package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.OrderDetailsRequest;
import com.scms.supplychainmanagementsystem.dto.OrderDetailsResponse;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.*;
import com.scms.supplychainmanagementsystem.service.IOrderDetailsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class OrderDetailsService implements IOrderDetailsService {
    private final OrderDetailsRepository orderDetailsRepository;
    private final UserCommon userCommon;
    private final ProductRepository productRepository;
    private final PriceBookRepository priceBookRepository;
    private final OrderDetailSttRepository orderDetailSttRepository;
    private final OrderRepository orderRepository;
    private final StockRepository stockRepository;
    private final PriceBookEntryRepository priceBookEntryRepository;

    @Override
    public void updateOrderDetails(OrderDetailsRequest orderDetailsRequest, Long orderDetailId) {
        log.info("[Start OrderDetailsService - updateOrderDetails ID = " + orderDetailId + "]");
        if (checkAccessOrderDetails(orderDetailId)) {
            OrderDetails orderDetails = orderDetailsRepository.findById(orderDetailId)
                    .orElseThrow(() -> new AppException("ORDER_DETAILS_NOT_FOUND"));
            if (orderDetails.getOrder().getOrderStatus().getOrderStatusID() > 2) {
                throw new AppException("ORDER_STATUS_NOT_ALLOW");
            }
            if (orderDetailsRepository.existsByOrderIdAndProductId(orderDetails.getOrder().getOrderId()
                    , orderDetailsRequest.getProductId(), orderDetails.getProduct().getProductId())) {
                throw new AppException("PRODUCT_EXISTS_IN_ORDER");
            }
            PriceBook priceBook = priceBookRepository.findById(orderDetailsRequest.getPriceBookId())
                    .orElseThrow(() -> new AppException("PRICEBOOK_NOT_FOUND"));
            Product product = productRepository.findById(orderDetailsRequest.getProductId())
                    .orElseThrow(() -> new AppException("PRODUCT_NOT_FOUND"));
            if (!priceBook.getWarehouse().getWarehouseID().equals(orderDetails.getOrder().getWarehouse().getWarehouseID())
                    || !orderDetails.getOrder().getWarehouse().getWarehouseID().equals(product.getWarehouse().getWarehouseID())) {
                throw new AppException("WAREHOUSE_OF_REQUIRED_FIELD_NOT_MATCH");
            }
            orderDetails.setPriceBook(priceBook);
            orderDetails.setProduct(product);
            orderDetails.setQuantity(!orderDetailsRequest.getQuantity().isNaN() ? orderDetailsRequest.getQuantity() : 0);
            if (checkOrderItemQtyAvailable(orderDetailsRequest.getProductId(), orderDetailsRequest.getQuantity())) {
                //Enough stock
                orderDetails.setOrderDetailsStatus(orderDetailSttRepository.getById(2L));
            } else {
                //Not Enough stock
                orderDetails.setOrderDetailsStatus(orderDetailSttRepository.getById(1L));
            }
            log.info("[Start Save OrderDetails " + orderDetailId + " to database]");
            orderDetailsRepository.saveAndFlush(orderDetails);
            log.info("[End Save OrderDetails " + orderDetailId + " to database]");
            log.info("[End OrderDetailsService - updateOrderDetails ID = " + orderDetailId + "]");
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
    }

    public void updateStatusOrderDetails(Long orderDetailId, Long status) {
        log.info("[Start OrderDetailsService - updateOrderDetails ID = " + orderDetailId + "]");

        log.info("[Start get current user]");
        User currentUser = userCommon.getCurrentUser();
        log.info("[End get current user : " + currentUser.getUsername() + "]");

        OrderDetails orderDetails = orderDetailsRepository.getById(orderDetailId);

        Product product = orderDetails.getProduct();
        PriceBook priceBook = orderDetails.getPriceBook();
        OrderDetailsStatus orderDetailsStatus = orderDetailSttRepository.getById(status);
        OrderDetails orderDetails1 = OrderDetails.builder()
                .orderDetailId(orderDetailId)
                .order(orderDetails.getOrder())
                .quantity(orderDetails.getQuantity())
                .orderDetailsStatus(orderDetailsStatus)
                .product(product)
                .priceBook(priceBook)
                .currentPrice(orderDetails.getCurrentPrice())
                .build();
        log.info("[Start Save OrderDetails " + orderDetailId + " to database]");
        orderDetailsRepository.saveAndFlush(orderDetails1);
        log.info("[End Save OrderDetails " + orderDetailId + " to database]");
        log.info("[End OrderDetailsService - updateOrderDetails ID = " + orderDetailId + "]");

    }

    @Override
    public void createOrderDetailsByOrderId(OrderDetailsRequest orderDetailsRequest, Long orderId) {
        log.info("[Start OrderDetailsService - createOrderDetailsByOrderId]");
        Order order = orderRepository.findById(orderId)
                .orElseThrow(() -> new AppException("ORDER_NOT_FOUND"));
        if (orderDetailsRequest.getProductId() == null || orderDetailsRequest.getPriceBookId() == null) {
            throw new AppException("NOT_FILL_REQUIRED_FIELD");
        }
        if (order.getOrderStatus().getOrderStatusID() > 2) {
            throw new AppException("ORDER_STATUS_NOT_ALLOW");
        }
        if (orderDetailsRepository.existsByOrderIdAndProductId(orderId, orderDetailsRequest.getProductId(), null)) {
            throw new AppException("PRODUCT_EXISTS_IN_ORDER");
        }
        OrderDetails orderDetails = new OrderDetails();
        orderDetails.setOrder(order);
        PriceBook priceBook = priceBookRepository.findById(orderDetailsRequest.getPriceBookId())
                .orElseThrow(() -> new AppException("PRICEBOOK_NOT_FOUND"));
        Product product = productRepository.findById(orderDetailsRequest.getProductId())
                .orElseThrow(() -> new AppException("PRODUCT_NOT_FOUND"));
        if (!priceBook.getWarehouse().getWarehouseID().equals(orderDetails.getOrder().getWarehouse().getWarehouseID())
                || !orderDetails.getOrder().getWarehouse().getWarehouseID().equals(product.getWarehouse().getWarehouseID())) {
            throw new AppException("WAREHOUSE_OF_REQUIRED_FIELD_NOT_MATCH");
        }
        orderDetails.setPriceBook(priceBook);
        orderDetails.setProduct(product);
        orderDetails.setQuantity(!orderDetailsRequest.getQuantity().isNaN() ? orderDetailsRequest.getQuantity() : 0);
        if (checkOrderItemQtyAvailable(orderDetailsRequest.getProductId(), orderDetailsRequest.getQuantity())) {
            //Enough stock
            orderDetails.setOrderDetailsStatus(orderDetailSttRepository.getById(2L));
        } else {
            //Not Enough stock
            orderDetails.setOrderDetailsStatus(orderDetailSttRepository.getById(1L));
        }
        orderDetails.setCurrentPrice(priceBookEntryRepository
                .getPriceByPriceBookIdAndProductId(orderDetailsRequest.getProductId(), orderDetailsRequest.getPriceBookId()));
        log.info("[Start Save OrderDetails to database]");
        orderDetailsRepository.saveAndFlush(orderDetails);
        log.info("[End Save OrderDetails to database]");
        log.info("[End OrderDetailsService - createOrderDetailsByOrderId]");
    }

    @Override
    public void deleteOrderDetailsById(Long orderDetailId) {
        log.info("[Start OrderDetailsService - deleteOrderDetailsById = " + orderDetailId + "]");
        if (checkAccessOrderDetails(orderDetailId)) {
            OrderDetails orderDetails = orderDetailsRepository.getById(orderDetailId);
            Long orderStatusId = orderDetails.getOrder().getOrderStatus().getOrderStatusID();
            if (orderStatusId > 2) {
                throw new AppException("ORDER_STATUS_NOT_ALLOW");
            }
            orderDetailsRepository.deleteById(orderDetailId);
            log.info("Delete OrderDetails Successfully");
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        log.info("[End OrderDetailsService - deleteOrderDetailsById = " + orderDetailId + "]");

    }

    @Override
    public OrderDetailsResponse getOrderDetailsById(Long orderDetailId) {
        log.info("[Start OrderDetailsService - getOrderDetailsById = " + orderDetailId + "]");
        OrderDetailsResponse orderDetailsResponse;
        if (checkAccessOrderDetails(orderDetailId)) {
            OrderDetails orderDetails = orderDetailsRepository.getById(orderDetailId);
            orderDetailsResponse = OrderDetailsResponse.builder()
                    .orderDetailId(orderDetailId)
                    .orderId(orderDetails.getOrder().getOrderId())
                    .priceBookId(orderDetails.getPriceBook().getPriceBookId())
                    .productId(orderDetails.getProduct().getProductId())
                    .quantity(orderDetails.getQuantity())
                    .orderDetailsStatus(orderDetails.getOrderDetailsStatus())
                    .price(orderDetails.getCurrentPrice())
                    .amount(orderDetails.getCurrentPrice() * orderDetails.getQuantity())
                    .build();
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        log.info("[End OrderDetailsService - getOrderDetailsById = " + orderDetailId + "]");
        return orderDetailsResponse;
    }

    @Override
    public Page<OrderDetailsResponse> getAllOrderDetailsByOrderId(Long orderId, String productName, Long orderDetailsStatusId, Pageable pageable) {
        log.info("[Start OrderDetailsService - getAllOrderDetailsByOrderId = " + orderId + "]");
        Page<OrderDetailsResponse> orderDetailsPage;
        orderDetailsPage = orderDetailsRepository.getAllOrderDetailsByOrderId(orderId, productName, orderDetailsStatusId, pageable);
//        Page<OrderDetailsResponse> orderDetailsResponsePage = orderDetailsPage.map(o ->
//                OrderDetailsResponse.builder()
//                        .orderDetailId(o.getOrderDetailId())
//                        .productName(o.getProduct().getProductName())
//                        .orderId(orderId)
//                        .priceBookId(o.getPriceBook().getPriceBookId())
//                        .priceBookName(o.getPriceBook().getPriceBookName())
//                        .productId(o.getProduct().getProductId())
//                        .quantity(o.getQuantity())
//                        .orderDetailsStatus(o.getOrderDetailsStatus())
//                        .price(o.getCurrentPrice())
//                        .amount(o.getCurrentPrice() * o.getQuantity())
//                        .build());
        log.info("[End OrderDetailsService - getAllOrderDetailsByOrderId = " + orderId + "]");
        return orderDetailsPage;
    }

    private boolean checkAccessOrderDetails(Long orderDetailId) {
        OrderDetails orderDetails = orderDetailsRepository.findById(orderDetailId)
                .orElseThrow(() -> new AppException("ORDER_DETAILS_NOT_FOUND"));
        User current = userCommon.getCurrentUser();
        if (current.getRole().getRoleID() == 1) {
            return true;
        }
        if (orderDetails.getOrder().getWarehouse().getWarehouseID() != null && current.getWarehouse() != null) {
            return orderDetails.getOrder().getWarehouse().getWarehouseID().equals(current.getWarehouse().getWarehouseID());
        }
        return false;
    }

    public boolean checkOrderItemQtyAvailable(Long productId, Double quantity) {
        return stockRepository.checkQtyAvailable(productId, quantity);
    }
}
