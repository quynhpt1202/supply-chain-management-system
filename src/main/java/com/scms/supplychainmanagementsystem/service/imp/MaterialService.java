package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.MaterialDto;
import com.scms.supplychainmanagementsystem.entity.Material;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.MaterialRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import com.scms.supplychainmanagementsystem.service.IMaterialService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@Transactional
@Slf4j
@Service
public class MaterialService implements IMaterialService {

    private MaterialRepository materialRepository;
    private final UserCommon userCommon;
    private WarehouseRepository warehouseRepository;

    @Override
    public Material getMaterialByIdInWarehouse(Long MaterialId) {
        User currentUser = userCommon.getCurrentUser();
        Material material = new Material();
        if (currentUser.getRole().getRoleID() != 1) {
            material = materialRepository.findByMaterialIdAnhInWarehouse(MaterialId, userCommon.getCurrentUser().getWarehouse().getWarehouseID());
        } else {
            material = materialRepository.findByMaterialId(MaterialId);
        }
        return material;
    }

    @Override
    public void updateMaterial(Long materialId, MaterialDto materialDto) {
        log.info("[Start MaterialService - saveMaterial with MaterialName: " + materialDto.getMaterialName() + "]");
        log.info("[Start get current user]");
        User currentUser = userCommon.getCurrentUser();
        log.info("[End get current user : " + currentUser.getUsername() + "]");
        Material materialOld=materialRepository.getById(materialId);

        if (currentUser.getRole().getRoleID() != 1) {
            if (currentUser.getWarehouse().getWarehouseID() != materialRepository.findByMaterialId(materialId).getWarehouse().getWarehouseID()) {
                throw new AppException("CAN_NOT_UPDATE_IN_ANOTHER_WAREHOUSE");
            }
        }

        if (!materialRepository.getById(materialId).getMaterialName().equals(materialDto.getMaterialName()) & materialRepository.getByMaterialNameAndWarehouseWarehouseID(materialDto.getMaterialName(),materialOld.getWarehouse().getWarehouseID())!=null) {
            throw new AppException("MATERIAL_NAME_EXISTS");
        }

        Material material = Material.builder()
                .materialID(materialId)
                .MaterialName(materialDto.getMaterialName())
                .quantityUnitOfMeasure(materialDto.getQuantityUnitOfMeasure())
                .warehouse(materialOld.getWarehouse())
                .createdDate(materialOld.getCreatedDate())
                .lastModifiedDate(Instant.now())
                .createdBy(materialOld.getCreatedBy())
                .lastModifiedBy(currentUser)
                .build();
        log.info("[Start update material " + material.getMaterialName() + " to database]");
        materialRepository.save(material);
        log.info("[End update material " + material.getMaterialName() + " to database]");
        log.info("[End MaterialService - updateMaterial with name: " + material.getMaterialName() + "]");
    }

    @Override
    public void saveMaterial(MaterialDto materialDto) {
        log.info("[Start MaterialService - saveMaterial with MaterialName: " + materialDto.getMaterialName() + "]");

        log.info("[Start get current user]");
        User currentUser = userCommon.getCurrentUser();
        log.info("[End get current user : " + currentUser.getUsername() + "]");

        Warehouse warehouse = new Warehouse();
        if (currentUser.getRole().getRoleID() != 1) {
            warehouse=warehouseRepository.getById(currentUser.getWarehouse().getWarehouseID());
        } else {
            warehouse= warehouseRepository.getById(materialDto.getWarehouseId());
        }

        if (materialRepository.getByMaterialNameAndWarehouseWarehouseID(materialDto.getMaterialName(),warehouse.getWarehouseID())!=null) {
            throw new AppException("MATERIAL_NAME_EXISTS");
        }

        Material material = Material.builder()
                .MaterialName(materialDto.getMaterialName())
                .quantityUnitOfMeasure(materialDto.getQuantityUnitOfMeasure())
                .warehouse(warehouse)
                .createdDate(Instant.now())
                .createdBy(currentUser)
                .build();
        log.info("[Start save Material " + materialDto.getMaterialName() + " to database]");
        materialRepository.saveAndFlush(material);
        log.info("[End save Material " + materialDto.getMaterialName() + " to database]");
        log.info("[End MaterialService - saveMaterial with MaterialName: " + materialDto.getMaterialName() + "]");
    }

    @Override
    public void deleteMaterial(Long materialId) {
        User currentUser = userCommon.getCurrentUser();
        if (currentUser.getRole().getRoleID() != 1) {
            if (currentUser.getWarehouse().getWarehouseID() != materialRepository.getById(materialId).getWarehouse().getWarehouseID()) {
                throw new AppException("CAN_NOT_DELETE_IN_ANOTHER_WAREHOUSE");
            }
            materialRepository.deleteMaterialAdmin(materialId);
        } else {
            materialRepository.deleteMaterialAdmin(materialId);
        }

    }

    @Override
    public List<Material> getMaterialInWareHouse(Long warehouseid) {
        return materialRepository.findAllByWarehouse(warehouseid);
    }

    @Override
    public Page<Material> getAllMaterial(String materialname, Long warehouseId, Pageable pageable) {
        log.info("[Start CustomerService - Get All Customer]");
        Page<Material> materialPage;
        User current = userCommon.getCurrentUser();
        Warehouse wh = current.getWarehouse();
        Long userId = current.getUserId();
        if (current.getRole().getRoleID() == 1) {
            materialPage = materialRepository.filterAllWarehouses(materialname, warehouseId, pageable);
        } else {
            materialPage = materialRepository.filterInOneWarehouse(materialname, wh.getWarehouseID(), pageable);
        }
        log.info("[End CustomerService - Get All Customer]");
        return materialPage;
    }
}
