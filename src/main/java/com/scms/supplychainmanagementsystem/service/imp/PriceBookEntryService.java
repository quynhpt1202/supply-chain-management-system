package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.dto.PriceBookEntryDto;
import com.scms.supplychainmanagementsystem.entity.PriceBookEntry;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.PriceBookEntryRepository;
import com.scms.supplychainmanagementsystem.repository.PriceBookRepository;
import com.scms.supplychainmanagementsystem.repository.ProductRepository;
import com.scms.supplychainmanagementsystem.service.IPriceBookEntryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class PriceBookEntryService implements IPriceBookEntryService {

    private final PriceBookEntryRepository priceBookEntryRepository;
    private final PriceBookRepository priceBookRepository;
    private final ProductRepository productRepository;

    @Override
    public void updatePriceBookEntry(PriceBookEntryDto priceBookEntryDto) {
        log.info("[Start PriceBookEntryService - Update PriceBookEntry ID = " + priceBookEntryDto.getPriceBookEntryId() + "]");
        PriceBookEntry priceBookEntry = priceBookEntryRepository.findById(priceBookEntryDto.getPriceBookEntryId())
                .orElseThrow(() -> new AppException("PriceBookEntry not found"));

        priceBookEntry.setPriceBook(priceBookRepository.findById(priceBookEntryDto.getPriceBookId())
                .orElseThrow(() -> new AppException("PRICEBOOK_NOT_FOUND")));

        priceBookEntry.setPrice(priceBookEntryDto.getPrice());

        priceBookEntry.setProduct(productRepository.findById(priceBookEntryDto.getProductId())
                .orElseThrow(() -> new AppException("PRODUCT_NOT_FOUND")));
        log.info("[Start Save PriceBookEntry  to database]");
        priceBookEntryRepository.saveAndFlush(priceBookEntry);
        log.info("[End Save PriceBookEntry  to database]");
        log.info("[End PriceBookEntryService -  Update PriceBookEntry ID = " + priceBookEntryDto.getPriceBookEntryId() + "]");

    }

    @Override
    public void createPriceBookEntry(PriceBookEntryDto priceBookEntryDto) {
        log.info("[Start PriceBookEntryService - Create PriceBookEntry ]");
        if (priceBookEntryDto.getProductId() == null || priceBookEntryDto.getPriceBookId() == null) {
            throw new AppException("NOT_FILL_REQUIRED_FIELD");
        }
        PriceBookEntry priceBookEntry = new PriceBookEntry();
        priceBookEntry.setPriceBook(priceBookRepository.findById(priceBookEntryDto.getPriceBookId())
                .orElseThrow(() -> new AppException("PRICEBOOK_NOT_FOUND")));

        priceBookEntry.setPrice(priceBookEntryDto.getPrice());

        priceBookEntry.setProduct(productRepository.findById(priceBookEntryDto.getProductId())
                .orElseThrow(() -> new AppException("PRODUCT_NOT_FOUND")));
        if (priceBookEntryRepository.existProductInPriceBook(priceBookEntryDto.getProductId(), priceBookEntryDto.getPriceBookId())) {
            throw new AppException("EXISTS_PRODUCT_IN_PRICEBOOK");
        }
        log.info("[Start Save PriceBookEntry to database]");
        priceBookEntryRepository.saveAndFlush(priceBookEntry);
        log.info("[End Save PriceBookEntry to database]");
        log.info("[End PriceBookEntryService - Create PriceBookEntry ]");
    }

    @Override
    public void deletePriceBookEntryById(Long priceBookEntryId) {
        log.info("[Start PriceBookEntryService - Delete PriceBookEntry ID = : " + priceBookEntryId + "]");
        priceBookEntryRepository.deleteById(priceBookEntryRepository.findById(priceBookEntryId)
                .orElseThrow(() -> new AppException("PRICEBOOK_ENTRY_NOT_FOUND")).getPriceBookEntryID());
        log.info("[End PriceBookEntryService - Delete PriceBookEntry ID = : " + priceBookEntryId + "]");
    }

    @Override
    public PriceBookEntryDto getPriceBookEntryById(Long priceBookEntryId) {
        log.info("[Start PriceBookEntryService - Get PriceBookEntry ID = : " + priceBookEntryId + "]");
        PriceBookEntry priceBookEntry = priceBookEntryRepository.findById(priceBookEntryId)
                .orElseThrow(() -> new AppException("PriceBookEntry not found"));

        PriceBookEntryDto priceBookEntryDto = new PriceBookEntryDto();
        priceBookEntryDto.setPriceBookEntryId(priceBookEntryId);
        priceBookEntryDto.setPriceBookId(priceBookEntry.getPriceBook().getPriceBookId());
        priceBookEntryDto.setProductId(priceBookEntry.getProduct().getProductId());
        priceBookEntryDto.setPrice(priceBookEntry.getPrice());

        log.info("[End  PriceBookEntryService - Get PriceBookEntry ID = : " + priceBookEntryId + "]");
        return priceBookEntryDto;
    }

    @Override
    public Page<PriceBookEntry> getAllPriceBookEntriesByProductId(Long productId, Pageable pageable) {
        log.info("[Start PriceBookEntryService - Get All PriceBook Entries ByProductId]");
        Page<PriceBookEntry> priceBookEntryPage = priceBookEntryRepository.findAllByProductId(productId, pageable);
        log.info("[End PriceBookEntryService - Get All PriceBook Entries ByProductId]");
        return priceBookEntryPage;
    }

    @Override
    public Page<PriceBookEntry> getAllPriceBookEntriesByPriceBookId(Long priceBookId, Pageable pageable) {
        log.info("[Start PriceBookEntryService - Get All PriceBook Entries By PriceBookId]");
        Page<PriceBookEntry> priceBookEntryPage = priceBookEntryRepository.findAllByPriceBookId(priceBookId, pageable);
        log.info("[End PriceBookEntryService - Get All PriceBook Entries By PriceBookId]");
        return priceBookEntryPage;
    }

}
