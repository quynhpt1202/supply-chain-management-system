package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.PriceBookDto;
import com.scms.supplychainmanagementsystem.entity.PriceBook;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.PriceBookRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import com.scms.supplychainmanagementsystem.service.IPriceBookService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class PriceBookService implements IPriceBookService {

    private final PriceBookRepository priceBookRepository;
    private final UserCommon userCommon;
    private final WarehouseRepository warehouseRepository;

    @Override
    public void updatePriceBook(PriceBookDto priceBookDto) {
        log.info("[Start PriceBookService - Update PriceBook : " + priceBookDto.getPriceBookName() + "]");
        if (checkAccessPriceBook(priceBookDto.getPriceBookId())) {
            PriceBook priceBook = priceBookRepository.findById(priceBookDto.getPriceBookId())
                    .orElseThrow(() -> new AppException("PRICEBOOK_NOT_FOUND"));
            if (!priceBook.getPriceBookName().equals(priceBookDto.getPriceBookName()) &&
                    priceBookRepository.existsPriceBookByPriceBookNameInWarehouse(priceBookDto.getPriceBookName(), priceBookDto.getWarehouseId())) {
                throw new AppException("PRICEBOOK_NAME_EXISTS");
            }
            priceBook.setPriceBookName(priceBookDto.getPriceBookName());
            User current = userCommon.getCurrentUser();
            if (current.getRole().getRoleID() == 1) {
                priceBook.setWarehouse(warehouseRepository.getById(priceBookDto.getWarehouseId()));
            }
            checkStandardPriceBook(priceBookDto.getIsStandardPriceBook(), priceBook.getWarehouse(), priceBook.getPriceBookId());
            priceBook.setIsStandardPriceBook(priceBookDto.getIsStandardPriceBook());
            log.info("[Start Save PriceBook " + priceBookDto.getPriceBookName() + " to database]");
            priceBookRepository.saveAndFlush(priceBook);
            log.info("[End Save PriceBook " + priceBookDto.getPriceBookName() + " to database]");
            log.info("[End PriceBookService - Update PriceBook : " + priceBookDto.getPriceBookName() + "]");
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
    }

    @Override
    public Long createPriceBook(PriceBookDto priceBookDto) {
        log.info("[Start PriceBookService - Create PriceBook : " + priceBookDto.getPriceBookName() + "]");
        User current = userCommon.getCurrentUser();
        PriceBook priceBook = new PriceBook();
        if (priceBookRepository.existsPriceBookByPriceBookNameInWarehouse(priceBookDto.getPriceBookName(), priceBookDto.getWarehouseId())) {
            throw new AppException("PRICEBOOK_NAME_EXISTS");
        }
        if (current.getRole().getRoleID() == 1) {
            priceBook.setWarehouse(warehouseRepository.findById(priceBookDto.getWarehouseId())
                    .orElseThrow(() -> new AppException("WAREHOUSE_NOT_FOUND")));
        } else {
            priceBook.setWarehouse(current.getWarehouse());
        }
        priceBook.setPriceBookName(priceBookDto.getPriceBookName());
        checkStandardPriceBook(priceBookDto.getIsStandardPriceBook(), priceBook.getWarehouse(), null);
        priceBook.setIsStandardPriceBook(priceBookDto.getIsStandardPriceBook());
        log.info("[Start Save PriceBook " + priceBookDto.getPriceBookName() + " to database]");
        priceBookRepository.save(priceBook);
        log.info("[End Save PriceBook " + priceBookDto.getPriceBookName() + " to database]");
        log.info("[End PriceBookService - Create PriceBook : " + priceBookDto.getPriceBookName() + "]");
        return priceBook.getPriceBookId();
    }

    @Override
    public void deletePriceBookById(Long priceBookId) {
        log.info("[Start PriceBookService - Delete PriceBook ID = : " + priceBookId + "]");
        if (checkAccessPriceBook(priceBookId)) {
            priceBookRepository.deleteById(priceBookId);
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        log.info("[End PriceBookService - Delete PriceBook ID = : " + priceBookId + "]");
    }

    @Override
    public PriceBookDto getPriceBookById(Long priceBookId) {
        log.info("[Start PriceBookService - Get PriceBook ID = : " + priceBookId + "]");
        PriceBookDto priceBookDto = new PriceBookDto();
        PriceBook priceBook = priceBookRepository.findById(priceBookId)
                .orElseThrow(() -> new AppException("PRICEBOOK_NOT_FOUND"));
        if (checkAccessPriceBook(priceBookId)) {
            priceBookDto.setPriceBookId(priceBookId);
            priceBookDto.setPriceBookName(priceBook.getPriceBookName());
            priceBookDto.setIsStandardPriceBook(priceBook.getIsStandardPriceBook());
            priceBookDto.setWarehouseId(priceBook.getWarehouse().getWarehouseID());
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        log.info("[End  PriceBookService - Get PriceBook ID = : " + priceBookId + "]");
        return priceBookDto;
    }

    @Override
    public Page<PriceBook> getAllPriceBooks(String priceBookName, Long warehouseId, Pageable pageable) {
        log.info("[Start PriceBookService - Get All PriceBooks]");
        Page<PriceBook> priceBookPage;
        User current = userCommon.getCurrentUser();
        Warehouse wh = current.getWarehouse();
        if (current.getRole().getRoleID() == 1) {
            priceBookPage = priceBookRepository.filterAllWarehouses(priceBookName, warehouseId, pageable);
        } else {
            priceBookPage = priceBookRepository.filterInOneWarehouse(priceBookName, wh.getWarehouseID(), pageable);
        }
        log.info("[End PriceBookService - Get All PriceBooks]");
        return priceBookPage;
    }

    @Override
    public List<PriceBookDto> getAllPriceBookByProducId(Long productId) {
        log.info("[Start PriceBookService - Get All PriceBook By Product ID = : " + productId + "]");
        List<PriceBook> priceBookList = priceBookRepository.getAllByProductId(productId);
        List<PriceBookDto> priceBookDtos = priceBookList.stream().map(p -> new PriceBookDto(p.getPriceBookId()
                , p.getPriceBookName(), p.getIsStandardPriceBook()
                , p.getWarehouse().getWarehouseID())).collect(Collectors.toList());
        log.info("[End PriceBookService - Get All PriceBook By Product ID = : " + productId + "]");
        return priceBookDtos;
    }

    public boolean checkAccessPriceBook(Long priceBookId) {
        PriceBook priceBook = priceBookRepository.findById(priceBookId)
                .orElseThrow(() -> new AppException("PRICEBOOK_NOT_FOUND"));
        User current = userCommon.getCurrentUser();
        if (current.getRole().getRoleID() == 1) {
            return true;
        }
        if (priceBook.getWarehouse() != null && current.getWarehouse() != null) {
            return priceBook.getWarehouse().getWarehouseID().equals(current.getWarehouse().getWarehouseID());
        }
        return false;
    }

    public void checkStandardPriceBook(Boolean isStandard, Warehouse warehouse, Long priceBookId) {
        if (isStandard && priceBookRepository.existsStandardPriceBook(warehouse, priceBookId)) {
            throw new AppException("EXISTS_STANDARD_PRICEBOOK");
        }
    }
}
