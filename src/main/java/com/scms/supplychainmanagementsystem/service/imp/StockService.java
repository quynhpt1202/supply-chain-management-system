package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.repository.OrderDetailsRepository;
import com.scms.supplychainmanagementsystem.repository.ProductRepository;
import com.scms.supplychainmanagementsystem.repository.StockRepository;
import com.scms.supplychainmanagementsystem.service.IStockService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@AllArgsConstructor
@Transactional
@Slf4j
@Service
public class StockService implements IStockService {

    private final UserCommon userCommon;
    private StockRepository stockRepository;
    private ProductRepository productRepository;
    private OrderDetailsRepository orderDetailsRepository;
    private OrderDetailsService orderDetailsService;

    @Override
    public Page<Stock> getAllStockAdmin(String productName, Long warehouseId, Pageable pageable) {
        log.info("[Start PurchaseService - Get All Purchase]");
        Page<Stock> StockPage;
        User current = userCommon.getCurrentUser();
        Warehouse wh = current.getWarehouse();
        Long userId = current.getUserId();
        if (current.getRole().getRoleID() == 1) {
            StockPage = stockRepository.filterAllWarehousesAdmin(productName, warehouseId, pageable);
        } else {
            StockPage = stockRepository.filteroneWarehouses(productName, wh.getWarehouseID(), pageable);
        }
        log.info("[End PurchaseService - Get All Purchase]");
        return StockPage;

    }

    public void updateStockQuantity(Long productId, Double quantity) {

        Stock strock = stockRepository.findByProductId(productId);

        Double stockQuantity = strock.getAvailableQuantity() + quantity;
        Product product = productRepository.getById(productId);
        if (quantity > 0) {

            Double sumQuantity = strock.getAvailableQuantity() + quantity;
            List<OrderDetails> list = orderDetailsRepository.getOrderDetailsByProductId(productId);

            for (OrderDetails orderDetails : list) {
                if (orderDetails.getOrderDetailsStatus().getOrderDetailStatusID() == 1 && sumQuantity >= orderDetails.getQuantity()) {
                    orderDetailsService.updateStatusOrderDetails(orderDetails.getOrderDetailId(), 2L);
                    sumQuantity -= orderDetails.getQuantity();
                }
            }
        }

        if (quantity < 0) {

            Double sumQuantity = strock.getAvailableQuantity() + quantity;  //100>>20
            List<OrderDetails> list = orderDetailsRepository.getOrderDetailsByProductIdDesc(productId);

            for (OrderDetails orderDetails : list) {
                if (orderDetails.getOrderDetailsStatus().getOrderDetailStatusID() == 2 && sumQuantity < orderDetails.getQuantity()) {
                    orderDetailsService.updateStatusOrderDetails(orderDetails.getOrderDetailId(), 1L);
                }
            }
        }
        Stock stock = Stock.builder()
                .stockId(strock.getStockId())
                .availableQuantity(stockQuantity)
                .product(product)
                .build();
        stockRepository.save(stock);
        log.info("[Start StockService - UpdateStock  to database ]");
        log.info("[End StockService - UpdateStock ]");
    }
}


