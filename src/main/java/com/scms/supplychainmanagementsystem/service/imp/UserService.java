package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.*;
import com.scms.supplychainmanagementsystem.entity.District;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.RoleRepository;
import com.scms.supplychainmanagementsystem.repository.UserRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import com.scms.supplychainmanagementsystem.service.IUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class UserService implements IUserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserCommon userCommon;
    private final PasswordEncoder passwordEncoder;
    private final WarehouseRepository warehouseRepository;

    @Override
    public void updateUser(UserRequest userRequest, Long userId) {
        log.info("[Start UserService - updateUser with username: " + userRequest.getUsername() + "]");
        log.info("[Start get current user]");
        User currentUser = userCommon.getCurrentUser();
        log.info("[End get current user : " + currentUser.getUsername() + "]");
        if (!userCommon.checkAccessUserInfoInWarehouse(userId)) {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        if (!currentUser.getUserId().equals(userId) && currentUser.getRole().getRoleID() != 1 && userRequest.getRoleId() != 3) {
            throw new AppException("NOT_ALLOW_UPDATE_ROLE");
        }
        User user = userRepository.findById(userId).orElseThrow(() -> new AppException("USER_NOT_FOUND"));
        user.setEmail(userRequest.getEmail());
        user.setRole(roleRepository.findById(userRequest.getRoleId())
                .orElseThrow(() -> new AppException("ROLE_NOT_FOUND")));
        user.setFirstName(userRequest.getFirstName());
        user.setLastName(userRequest.getLastName());
        user.setActive(userRequest.isActive());
        user.setPhone(userRequest.getPhone());
        user.setDateOfBirth(userRequest.getDateOfBirth());
        user.setDistrict(District.builder().districtID(userRequest.getDistrictId()).build());
        user.setStreetAddress(userRequest.getStreetAddress());
        user.setLastModifiedBy(currentUser);
        user.setLastModifiedDate(Instant.now());
        if (currentUser.getRole().getRoleID() == 1) {
            user.setWarehouse(warehouseRepository.findById(userRequest.getWarehouseId())
                    .orElseThrow(() -> new AppException("WAREHOUSE_NOT_FOUND")));
        } else {
            user.setWarehouse(currentUser.getWarehouse());
        }
        log.info("[Start save user " + user.getUsername() + " to database]");
        userRepository.saveAndFlush(user);
        log.info("[End save user " + user.getUsername() + " to database]");
        log.info("[End UserService - updateUser with username: " + userRequest.getUsername() + "]");
    }

    @Override
    public void saveUser(UserRequest userRequest) {
        log.info("[Start UserService - saveUser with username: " + userRequest.getUsername() + "]");
        if (userRequest.getRoleId() == null || userRequest.getDistrictId() == null || userRequest.getUsername() == null) {
            throw new AppException("NOT_FILL_REQUIRED_FIELD");
        }
        if (userRepository.existsByUsername(userRequest.getUsername())) {
            throw new AppException("USERNAME_EXISTS");
        }
        log.info("[Start get current user]");
        User currentUser = userCommon.getCurrentUser();
        log.info("[End get current user : " + currentUser.getUsername() + "]");
//        if (currentUser.getRole().getRoleID() != 1 && !UserRequest.getWarehouseId().equals(currentUser.getWarehouse().getWarehouseID())) {
//            throw new AppException("Not allow to choose this warehouse");
//        }
        if (currentUser.getRole().getRoleID() != 1 && userRequest.getRoleId() != 3) {
            throw new AppException("NOT_ALLOW_CREATE_ROLE");
        }
        User user = User.builder()
                .username(userRequest.getUsername())
                .password(passwordEncoder.encode("123@456"))
                .email(userRequest.getEmail())
                .role(roleRepository.findById(userRequest.getRoleId())
                        .orElseThrow(() -> new AppException("ROLE_NOT_FOUND")))
                .firstName(userRequest.getFirstName())
                .lastName(userRequest.getLastName())
                .isActive(userRequest.isActive())
                .phone(userRequest.getPhone())
                .dateOfBirth(userRequest.getDateOfBirth())
                .district(District.builder().districtID(userRequest.getDistrictId()).build())
                .streetAddress(userRequest.getStreetAddress())
                .createdDate(Instant.now())
                .createdBy(currentUser)
                .build();
        if (currentUser.getRole().getRoleID() == 1) {
            user.setWarehouse(warehouseRepository.findById(userRequest.getWarehouseId())
                    .orElseThrow(() -> new AppException("WAREHOUSE_NOT_FOUND")));
        } else {
            user.setWarehouse(currentUser.getWarehouse());
        }
        log.info("[Start save user " + user.getUsername() + " to database]");
        userRepository.saveAndFlush(user);
        log.info("[End save user " + user.getUsername() + " to database]");
        log.info("[End UserService - saveUser with username: " + userRequest.getUsername() + "]");
    }


    @Override
    @Transactional(readOnly = true)
    public UserResponseDetails getUserById(Long userId) {
        log.info("[Start UserService - find user by userID = " + userId + "]");
        User user = userRepository.findById(userId).orElseThrow(() -> new AppException("USER_NOT_FOUND"));
        if (!userCommon.checkAccessUserInfoInWarehouse(userId)) {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        UserResponseDetails userResponseDetails = UserResponseDetails.builder()
                .userId(userId)
                .username(user.getUsername())
                .email(user.getEmail())
                .roleId(user.getRole().getRoleID())
                .warehouseId(user.getWarehouse().getWarehouseID())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .isActive(user.isActive())
                .phone(user.getPhone())
                .dateOfBirth(user.getDateOfBirth())
                .districtId(user.getDistrict().getDistrictID())
                .streetAddress(user.getStreetAddress())
                .createdDate(user.getCreatedDate())
                .createdBy(user.getCreatedBy() != null ? user.getCreatedBy().getUsername() : null)
                .lastModifiedDate(user.getLastModifiedDate())
                .lastModifiedBy(user.getLastModifiedBy() != null ? user.getLastModifiedBy().getUsername() : null)
                .build();
        log.info("[End UserService - find user by userID = " + userId + "]");
        return userResponseDetails;
    }

    @Override
    public void changePassword(ChangePasswordRequest changePasswordRequest) {
        log.info("[Start UserService - Reset Password username " + userCommon.getCurrentUser().getUsername() + "]");

        User currentUser = userCommon.getCurrentUser();
        if (!passwordEncoder.matches(changePasswordRequest.getCurrentPassword(), currentUser.getPassword())) {
            throw new AppException("INVALID_CURRENT_PASSWORD");
        }
        currentUser.setPassword(passwordEncoder.encode(changePasswordRequest.getNewPassword()));
        userRepository.save(currentUser);
        log.info("[End UserService - Reset Password username = " + currentUser.getUsername() + "]");
    }

    @Override
    public void deleteUser(Long userId) {
        log.info("[Start UserService - Delete User By userID = " + userId + "]");
        if (!userCommon.checkAccessUserInfoInWarehouse(userId)) {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        userRepository.deleteById(userId);
        log.info("[End UserService - Delete User By userID = " + userId + "]");
    }

    @Override
    public List<RoleDto> getAllRoles() {
        log.info("[Start UserService - Get All Roles]");
        List<RoleDto> roleDtoList = roleRepository.findAll()
                .stream().map(x -> new RoleDto(x.getRoleID(), x.getRoleName())).collect(Collectors.toList());
        log.info("[End UserService - Get All Roles]");
        return roleDtoList;
    }

    @Override
    public Page<UserResponse> getAllUsers(String username, Long roleId, Long warehouseId, Pageable pageable) {
        log.info("[Start UserService - Get All Users]");
        Page<User> userPage;
        User current = userCommon.getCurrentUser();
        Warehouse wh = current.getWarehouse();
        Long userId = current.getUserId();
        if (current.getRole().getRoleID() == 1) {
            userPage = userRepository.filterAllWarehouses(username, roleId, warehouseId, userId, pageable);
        } else {
            userPage = userRepository.filterInOneWarehouse(username, roleId, wh.getWarehouseID(), userId, pageable);
        }
        Page<UserResponse> userResponsePage = userPage.map(u -> UserResponse.builder()
                .userId(u.getUserId())
                .username(u.getUsername())
                .email(u.getEmail())
                .role(u.getRole())
                .warehouse(u.getWarehouse())
                .firstName(u.getFirstName())
                .lastName(u.getLastName())
                .isActive(u.isActive())
                .phone(u.getPhone())
                .streetAddress(u.getStreetAddress())
                .createdDate(u.getCreatedDate())
                .lastModifiedDate(u.getLastModifiedDate())
                .createdBy(u.getCreatedBy() == null ? null : u.getCreatedBy().getUsername())
                .lastModifiedBy(u.getLastModifiedBy() == null ? null : u.getLastModifiedBy().getUsername())
                .build());
        log.info("[End UserService - Get All Users]");
        return userResponsePage;
    }


    @Override
    public void updateUserActive(Long userId, Boolean isActive) {
        log.info("[Start UserService - Update User Active " + userId + "]");
        if (!userCommon.checkAccessUserInfoInWarehouse(userId)) {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        User user = userRepository.findById(userId).orElseThrow(() -> new AppException("USER_NOT_FOUND"));
        user.setActive(isActive);
        userRepository.saveAndFlush(user);
        log.info("[End UserService - Update User Active " + userId + "]");
    }
}
