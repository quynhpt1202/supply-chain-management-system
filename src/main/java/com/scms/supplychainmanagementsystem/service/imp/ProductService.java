package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.ProductRequest;
import com.scms.supplychainmanagementsystem.dto.ProductResponse;
import com.scms.supplychainmanagementsystem.dto.ProductResponseDetails;
import com.scms.supplychainmanagementsystem.entity.Product;
import com.scms.supplychainmanagementsystem.entity.Stock;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.CategoryRepository;
import com.scms.supplychainmanagementsystem.repository.ProductRepository;
import com.scms.supplychainmanagementsystem.repository.StockRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import com.scms.supplychainmanagementsystem.service.IProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class ProductService implements IProductService {
    private final ProductRepository productRepository;
    private final UserCommon userCommon;
    private final WarehouseRepository warehouseRepository;
    private final CategoryRepository categoryRepository;
    private final StockRepository stockRepository;

    @Override
    public void updateProduct(ProductRequest productRequest) {
        log.info("[Start ProductService - updateProduct " + productRequest.getProductName() + "]");
        if (checkAccessProduct(productRequest.getProductId())) {
            Product product = productRepository.getById(productRequest.getProductId());
            User current = userCommon.getCurrentUser();
            if (current.getRole().getRoleID() == 1) {
                product.setWarehouse(warehouseRepository.findById(productRequest.getWarehouseId())
                        .orElseThrow(() -> new AppException("WAREHOUSE_NOT_FOUND")));
            }
            if (!product.getProductName().equals(productRequest.getProductName()) &&
                    productRepository.existsProductByProductNameInWarehouse(productRequest.getProductName(), productRequest.getWarehouseId())) {
                throw new AppException("PRODUCT_NAME_EXISTS");
            }
            product.setProductName(productRequest.getProductName());
            product.setCategory(categoryRepository.findById(productRequest.getCategoryId())
                    .orElseThrow(() -> new AppException("CATEGORY_NOT_FOUND")));

            product.setIsActive(productRequest.getIsActive());
            product.setQuantityUnitOfMeasure(productRequest.getQuantityUnitOfMeasure());
            product.setLastModifiedBy(current);

            log.info("[Start Save Product " + productRequest.getProductName() + " to database]");
            productRepository.saveAndFlush(product);
            log.info("[End Save Product " + productRequest.getProductName() + " to database]");
            log.info("[End ProductService - updateProduct " + productRequest.getProductName() + "]");
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
    }

    @Override
    public void createProduct(ProductRequest productRequest) {
        log.info("[Start ProductService - createProduct " + productRequest.getProductName() + "]");
        User current = userCommon.getCurrentUser();
        Product product = new Product();
        if (productRequest.getCategoryId() == null) {
            throw new AppException("NOT_FILL_REQUIRED_FIELD");
        }
        if (productRepository.existsProductByProductNameInWarehouse(productRequest.getProductName(), productRequest.getWarehouseId())) {
            throw new AppException("PRODUCT_NAME_EXISTS");
        }
        if (current.getRole().getRoleID() == 1) {
            product.setWarehouse(warehouseRepository.findById(productRequest.getWarehouseId())
                    .orElseThrow(() -> new AppException("WAREHOUSE_NOT_FOUND")));
        } else {
            product.setWarehouse(current.getWarehouse());
        }
        product.setProductName(productRequest.getProductName());
        product.setCategory(categoryRepository.findById(productRequest.getCategoryId())
                .orElseThrow(() -> new AppException("CATEGORY_NOT_FOUND")));
        product.setIsActive(productRequest.getIsActive());
        product.setQuantityUnitOfMeasure(productRequest.getQuantityUnitOfMeasure());
        product.setCreatedBy(current);
        log.info("[Start Save Product " + productRequest.getProductName() + " to database]");
        productRepository.saveAndFlush(product);
        log.info("[End Save Product " + productRequest.getProductName() + " to database]");
        Stock stock = Stock.builder().product(product).availableQuantity(0D).build();
        log.info("[Start Save Stock for Product ID = " + productRequest.getProductName() + " to database]");
        stockRepository.saveAndFlush(stock);
        log.info("[End Save Stock for Product ID = " + productRequest.getProductName() + " to database]");
        log.info("[End ProductService - createProduct " + productRequest.getProductName() + "]");
    }

    @Override
    public void deleteProductByProductId(Long productId) {
        log.info("[Start ProductService - deleteProductByProductId = " + productId + "]");
        if (checkAccessProduct(productId)) {
            stockRepository.deleteByProductId(productId);
            log.info("Delete stock into database Successfully");
            productRepository.deleteById(productId);
            log.info("Delete product into database Successfully");
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        log.info("[End ProductService - deleteProductByProductId = " + productId + "]");

    }

    @Override
    public ProductResponseDetails getProductById(Long productId) {
        log.info("[Start ProductService - getProductById = " + productId + "]");
        ProductResponseDetails productResponseDetails = new ProductResponseDetails();
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new AppException("PRODUCT_NOT_FOUND"));
        if (checkAccessProduct(productId)) {
            productResponseDetails.setProductId(productId);
            productResponseDetails.setProductName(product.getProductName());
            productResponseDetails.setIsActive(product.getIsActive());
            productResponseDetails.setCategoryId(product.getCategory().getCategoryId());
            productResponseDetails.setQuantityUnitOfMeasure(product.getQuantityUnitOfMeasure());
            productResponseDetails.setAvailableQuantity(stockRepository.findByProduct(product).getAvailableQuantity());
            productResponseDetails.setWarehouseId(product.getWarehouse().getWarehouseID());
            productResponseDetails.setCreatedBy(product.getCreatedBy() != null ? product.getCreatedBy().getUsername() : null);
            productResponseDetails.setLastModifiedBy(product.getLastModifiedBy() != null ? product.getLastModifiedBy().getUsername() : null);
        } else {
            throw new AppException("NOT_ALLOW_ACCESS");
        }
        log.info("[End ProductService - getProductById = " + productId + "]");
        return productResponseDetails;
    }

    @Override
    public Page<ProductResponse> getAllProducts(String productName, Long categoryId, Long warehouseId, Boolean isActive, Pageable pageable) {
        log.info("[Start ProductService - Get All Products]");
        Page<Product> productPage;
        User current = userCommon.getCurrentUser();
        Warehouse wh = current.getWarehouse();
        if (current.getRole().getRoleID() == 1) {
            productPage = productRepository.filterAllWarehouses(productName, categoryId, warehouseId, isActive, pageable);
        } else {
            productPage = productRepository.filterInOneWarehouse(productName, categoryId, wh.getWarehouseID(), isActive, pageable);
        }
        Page<ProductResponse> productResponseAllPage = productPage
                .map(p -> ProductResponse.builder()
                        .productId(p.getProductId())
                        .productName(p.getProductName())
                        .warehouseName(p.getWarehouse().getWarehouseName())
                        .categoryName(p.getCategory().getCategoryName())
                        .quantityUnitOfMeasure(p.getQuantityUnitOfMeasure())
                        .isActive(p.getIsActive())
                        .availableQuantity(stockRepository.findByProduct(p) == null
                                ? 0 : stockRepository.findByProduct(p).getAvailableQuantity())
                        .build());
        log.info("[End ProductService - Get All Products]");
        return productResponseAllPage;
    }

    @Override
    public boolean checkProductExist(Long productId) {
        return productRepository.existsById(productId);
    }

    @Override
    public void updateProductActive(Long productId, Boolean isActive) {
        log.info("[Start ProductService - Update Product Active " + productId + "]");
        if (!checkAccessProduct(productId)) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, "NOT_ALLOW_UPDATE_ACTIVATION");
        }
        Product product = productRepository.getById(productId);
        product.setIsActive(isActive);
        productRepository.saveAndFlush(product);
        log.info("[End ProductService - Update Product Active " + productId + "]");
    }

    public boolean checkAccessProduct(Long productId) {
        Product product = productRepository.findById(productId)
                .orElseThrow(() -> new AppException("PRODUCT_NOT_FOUND"));
        User current = userCommon.getCurrentUser();
        if (current.getRole().getRoleID() == 1) {
            return true;
        }
        if (product.getWarehouse() != null && current.getWarehouse() != null) {
            return product.getWarehouse().getWarehouseID().equals(current.getWarehouse().getWarehouseID());
        }
        return false;
    }
}
