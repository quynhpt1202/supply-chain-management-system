package com.scms.supplychainmanagementsystem.common;

public enum PrefixUrl {
    LOCALHOST("http://localhost:8080/api"),
    LOCALHOST_REACT("http://localhost:3000"),
    HEROKU_STAGING("https://scms-staging.herokuapp.com"),
    SERVER_REACT("https://supply-chain-manager-system.surge.sh");
    public final String URL;

    PrefixUrl(String URL) {
        this.URL = URL;
    }
}
