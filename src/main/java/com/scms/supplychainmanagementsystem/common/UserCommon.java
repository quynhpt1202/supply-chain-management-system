package com.scms.supplychainmanagementsystem.common;

import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class UserCommon {
    private final UserRepository userRepository;

    public User getCurrentUser() {
        org.springframework.security.core.userdetails.User principal
                = (org.springframework.security.core.userdetails.User) SecurityContextHolder
                .getContext().getAuthentication().getPrincipal();
        return userRepository.findByUsername(principal.getUsername())
                .orElseThrow(() -> new UsernameNotFoundException("USER_NOT_FOUND"));
    }

    public boolean checkAccessUserInfoInWarehouse(Long userId) {
        User u = userRepository.findById(userId).orElseThrow(() -> new AppException("USER_NOT_FOUND"));
        User current = getCurrentUser();
        if (current.getRole().getRoleID() == 1) {
            return true;
        }
        if (u.getRole() == null || u.getDistrict() == null) {
            throw new AppException("NOT_FILL_REQUIRED_FIELD");
        } else {
            return u.getWarehouse().getWarehouseID().equals(current.getWarehouse().getWarehouseID());
        }
    }
}
