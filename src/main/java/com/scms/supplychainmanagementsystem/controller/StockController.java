package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.entity.Stock;
import com.scms.supplychainmanagementsystem.service.IStockService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/manage/stock")
public class StockController {
    private IStockService iStockService;

    @GetMapping()
    public ResponseEntity<Map<String, Object>> getAllStock(@RequestParam(required = false) String productName,
                                                           @RequestParam(required = false) Long warehouseId,
                                                           @RequestParam(defaultValue = "0") int page,
                                                           @RequestParam(defaultValue = "10") int size) {
        log.info("[Start PurchaseController - Get All Purchase In Warehouse]");
        List<Stock> StockList;
        Page<Stock> StockPage;
        Pageable pageable = PageRequest.of(page, size);

        StockPage = iStockService.getAllStockAdmin(productName, warehouseId, pageable);

        StockList = StockPage.getContent();

        Map<String, Object> response = new HashMap<>();
        response.put("data", StockList);
        response.put("currentPage", StockPage.getNumber());
        response.put("totalItems", StockPage.getTotalElements());
        response.put("totalPages", StockPage.getTotalPages());
        if (!StockPage.isEmpty()) {
            response.put("message", HttpStatus.OK);
        } else {
            response.put("message", "EMPTY_RESULT");
        }
        log.info("[End CustomerController - Get All Customer In Warehouse]");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
