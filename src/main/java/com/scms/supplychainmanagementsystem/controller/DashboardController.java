package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.dto.DashboardResults;
import com.scms.supplychainmanagementsystem.service.IDashboardService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("api/dashboard")
@AllArgsConstructor
@Slf4j
public class DashboardController {
    private IDashboardService iDashboardService;

    @GetMapping("/expense/product-cost")
    @ApiOperation(value = "Chi phí sản xuất sản phẩm nhập kho - line")
    public ResponseEntity<Map<String, Object>> getProductCost(@RequestParam String fromDate, @RequestParam String toDate,
                                                              @RequestParam(required = false) Long warehouseId) {
        log.info("[Start DashboardController - Get Product Cost]");
        Map<String, Object> result = new HashMap<>();
        DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder()
                .appendPattern("M/yyyy").parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();
        LocalDate localFrom = LocalDate.parse(fromDate, dateTimeFormatter);
        LocalDate localTo = LocalDate.parse(toDate, dateTimeFormatter).plusMonths(1).minusDays(1);
        DashboardResults response = iDashboardService.getProductCost(localFrom, localTo, warehouseId);
        result.put("datasets", response);
        result.put("message", OK);
        log.info("[End DashboardController - Get Product Cost]");
        return status(HttpStatus.OK).body(result);
    }

    @GetMapping("/expense/product-defect")
    @ApiOperation(value = "Chi phí sản phẩm lỗi hỏng - line")
    public ResponseEntity<Map<String, Object>> getDefectiveProductCost(@RequestParam String fromDate, @RequestParam String toDate,
                                                                       @RequestParam(required = false) Long warehouseId) {
        log.info("[Start DashboardController - Get Defective Product Cost]");
        Map<String, Object> result = new HashMap<>();
        DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder()
                .appendPattern("M/yyyy").parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();
        LocalDate localFrom = LocalDate.parse(fromDate, dateTimeFormatter);
        LocalDate localTo = LocalDate.parse(toDate, dateTimeFormatter).plusMonths(1).minusDays(1);
        DashboardResults response = iDashboardService.getDefectiveProductCost(localFrom, localTo, warehouseId);
        result.put("datasets", response);
        result.put("message", OK);
        log.info("[End DashboardController - Get Defective Product Cost]");
        return status(HttpStatus.OK).body(result);
    }

    @GetMapping("/revenue")
    @ApiOperation(value = "Doanh thu - stacked bar")
    public ResponseEntity<Map<String, Object>> getRevenue(@RequestParam String fromDate, @RequestParam String toDate,
                                                          @RequestParam(required = false) Long warehouseId) {
        log.info("[Start DashboardController - Get Revenue]");
        Map<String, Object> result = new HashMap<>();
        DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder()
                .appendPattern("M/yyyy").parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();
        LocalDate localFrom = LocalDate.parse(fromDate, dateTimeFormatter);
        LocalDate localTo = LocalDate.parse(toDate, dateTimeFormatter).plusMonths(1).minusDays(1);
        DashboardResults response = iDashboardService.getRevenue(localFrom, localTo, warehouseId);
        result.put("datasets", response);
        result.put("message", OK);
        log.info("[End DashboardController - Get Revenue]");
        return status(HttpStatus.OK).body(result);
    }

    @GetMapping("/gross-profit")
    @ApiOperation(value = "Lợi nhuận gộp")
    public ResponseEntity<Map<String, Object>> getGrossProfit(@RequestParam String fromDate, @RequestParam String toDate,
                                                              @RequestParam(required = false) Long warehouseId) {
        log.info("[Start DashboardController - Get Gross Profit]");
        Map<String, Object> result = new HashMap<>();
        DateTimeFormatter dateTimeFormatter = new DateTimeFormatterBuilder()
                .appendPattern("M/yyyy").parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();
        LocalDate localFrom = LocalDate.parse(fromDate, dateTimeFormatter);
        LocalDate localTo = LocalDate.parse(toDate, dateTimeFormatter).plusMonths(1).minusDays(1);
        DashboardResults response = iDashboardService.getGrossProfit(localFrom, localTo, warehouseId);
        result.put("datasets", response);
        result.put("message", OK);
        log.info("[End DashboardController - Get Gross Profit]");
        return status(HttpStatus.OK).body(result);
    }
}
