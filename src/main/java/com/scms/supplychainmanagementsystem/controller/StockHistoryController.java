package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.StockHistoryDto;
import com.scms.supplychainmanagementsystem.entity.Product;
import com.scms.supplychainmanagementsystem.entity.StockHistory;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.ProductRepository;
import com.scms.supplychainmanagementsystem.service.IStockHistoryService;
import com.scms.supplychainmanagementsystem.utils.ExportExcelStockHistory;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@AllArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api/stockhistory")
public class StockHistoryController {
    private IStockHistoryService iStockHistoryService;
    private UserCommon userCommon;
    private ProductRepository productRepository;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN','MANAGER')")
    @ApiOperation(value = "Requires ADMIN or MANAGER Access")
    public ResponseEntity<String> createStockHistory(@Valid @RequestBody StockHistoryDto stockHistoryDto) {
        log.info("[Start PurchaseController -  createPurchase  ]");
        iStockHistoryService.saveStockHistory(stockHistoryDto);
        log.info("[End PurchaseController -  createPurchase  ]");
        return new ResponseEntity<>("Created Successfully", CREATED);
    }

    @GetMapping()
    public ResponseEntity<Map<String, Object>> getAllStockHistory(@RequestParam(required = false) String productName,
                                                                  @RequestParam(required = false) Long warehouseId,
                                                                  @RequestParam(defaultValue = "0") int page,
                                                                  @RequestParam(defaultValue = "10") int size) {
        log.info("[Start StockHistoryController - Get All StockHistory In Warehouse]");
        List<StockHistory> StockHistorylList;
        Page<StockHistory> StockHistoryPage;
        Pageable pageable = PageRequest.of(page, size);

        StockHistoryPage = iStockHistoryService.getAllStockHistory(productName, warehouseId, pageable);

        StockHistorylList = StockHistoryPage.getContent();

        Map<String, Object> response = new HashMap<>();
        response.put("data", StockHistorylList);
        response.put("currentPage", StockHistoryPage.getNumber());
        response.put("totalItems", StockHistoryPage.getTotalElements());
        response.put("totalPages", StockHistoryPage.getTotalPages());
        if (!StockHistoryPage.isEmpty()) {
            response.put("message", HttpStatus.OK);
        } else {
            response.put("message", "EMPTY_RESULT");
        }
        log.info("[End StockHistoryController - Get All StockHistory In Warehouse]");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @GetMapping("/{stockhistoryid}")
    public ResponseEntity<StockHistoryDto> getStockHistoryById(@PathVariable Long stockhistoryid) {
        log.info("[Start StockHistoryController - Get StockHistory By ID]");
        StockHistory stockHistory = iStockHistoryService.getStockHistoryByIdInWarehouse(stockhistoryid);
        if (stockHistory != null) {
            StockHistoryDto stockHistoryDto = new StockHistoryDto(stockHistory);
            log.info("[End StockHistoryController - Get StockHistory By ID]");
            return status(HttpStatus.OK).body(stockHistoryDto);
        } else {
            return null;
        }
    }

    @PutMapping("/{stockhistoryid}")
    @PreAuthorize("hasAnyAuthority('ADMIN','MANAGER')")
    @ApiOperation(value = "Requires ADMIN or MANAGER Access")
    public ResponseEntity<String> updateStockHistory(@PathVariable Long stockhistoryid, @Valid @RequestBody StockHistoryDto stockHistoryDto) {
        log.info("[Start StockHistoryController - Update StockHistory ]");
        iStockHistoryService.updateStockHistory(stockhistoryid, stockHistoryDto);
        log.info("[End StockHistoryController - Update StockHistory ]");
        return new ResponseEntity<>("Update StockHistory Successfully", OK);
    }

    @DeleteMapping("/{stockhistoryid}")
    @PreAuthorize("hasAnyAuthority('ADMIN','MANAGER')")
    @ApiOperation(value = "Requires ADMIN or MANAGER Access")
    public ResponseEntity<String> DeleteStockHistory(@PathVariable Long stockhistoryid) {
        log.info("[Start StockHistoryController - Delete StockHistory By ID]");
        iStockHistoryService.deleteStockHistory(stockhistoryid);
        log.info("[End StockHistoryController - Delete StockHistory By ID]");
        return new ResponseEntity<>("Delete StockHistory Successfully", OK);
    }

    @PostMapping("/{warehouseId}/import")
    public ResponseEntity<String> mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile, @PathVariable Long warehouseId) throws IOException {
        log.info("[Start CustomerController -  AddCustomer from Excel  ");
        iStockHistoryService.mapReapExcelDatatoDB(reapExcelDataFile, warehouseId);
        log.info("[End CustomerController - AddCustomer from Excel  ");
        return new ResponseEntity<>("Add Successfully", CREATED);

    }

    @GetMapping("/{warehouseId}/export")
    public void mapWriteExcelDatatoDB(HttpServletResponse response, @PathVariable Long warehouseId) throws IOException {

        log.info("[Start StockHistoryController -  export StockHistory from Excel  ");
        User currentUser = userCommon.getCurrentUser();
        if (currentUser.getRole().getRoleID() != 1) {
            if (currentUser.getWarehouse().getWarehouseID() != warehouseId) {
                throw new AppException("CAN_NOT_EXPORT_IN_ANOTHER_WAREHOUSE");
            }
        }
        response.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=StockHistory.xlsx";
        response.setHeader(headerKey, headerValue);
        List<Product> productList = productRepository.findAllByWarehouseID(warehouseId);
        ExportExcelStockHistory exportExcel = new ExportExcelStockHistory(productList);
        exportExcel.export(response);
        log.info("[End StockHistoryController -  export StockHistory from Excel  ");


    }
}
