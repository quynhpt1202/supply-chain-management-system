package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.OrderRequest;
import com.scms.supplychainmanagementsystem.dto.OrderResponse;
import com.scms.supplychainmanagementsystem.dto.OrderResponseDetails;
import com.scms.supplychainmanagementsystem.dto.OrderStatusDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.ContactDeliveryRepository;
import com.scms.supplychainmanagementsystem.repository.CustomerRepository;
import com.scms.supplychainmanagementsystem.repository.PriceBookEntryRepository;
import com.scms.supplychainmanagementsystem.repository.PriceBookRepository;
import com.scms.supplychainmanagementsystem.service.IOrderService;
import com.scms.supplychainmanagementsystem.utils.ExportExcelOrder;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/api/order")
@AllArgsConstructor
@Slf4j
public class OrderController {
    private final IOrderService iOrderService;
    private CustomerRepository customerRepository;
    private PriceBookRepository priceBookRepository;
    private ContactDeliveryRepository contactDeliveryRepository;
    private PriceBookEntryRepository priceBookEntryRepository;
    private UserCommon userCommon;

    @GetMapping
    public ResponseEntity<Map<String, Object>> getAllOrders(@RequestParam(required = false) String orderCode,
                                                            @RequestParam(required = false) String customerName,
                                                            @RequestParam(required = false) Long orderStatusId,
                                                            @RequestParam(required = false) Long warehouseId,
                                                            @RequestParam(defaultValue = "0") int page,
                                                            @RequestParam(defaultValue = "10") int size) {
        log.info("[Start OrderController - getAllOrders]");
        List<OrderResponse> orderResponseList;
        Page<OrderResponse> orderResponseAllPage;
        Pageable pageable = PageRequest.of(page, size);
        orderResponseAllPage = iOrderService.getAllOrders(orderCode, customerName, orderStatusId, warehouseId, pageable);

        orderResponseList = orderResponseAllPage.getContent();
        Map<String, Object> response = new HashMap<>();
        response.put("data", orderResponseList);
        response.put("currentPage", orderResponseAllPage.getNumber());
        response.put("totalItems", orderResponseAllPage.getTotalElements());
        response.put("totalPages", orderResponseAllPage.getTotalPages());
        if (!orderResponseAllPage.isEmpty()) {
            response.put("message", HttpStatus.OK);
        } else {
            response.put("message", "EMPTY_RESULT");
        }
        log.info("[End OrderController - getAllOrders]");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN','MANAGER')")
    @ApiOperation(value = "Requires ADMIN or MANAGER Access. Not required [orderId]")
    public ResponseEntity<Map<String, Object>> createOrder(@RequestBody OrderRequest orderRequest) {
        log.info("[Start OrderController - createOrder]");
        Map<String, Object> result = new HashMap<>();
        Long orderId = iOrderService.createOrder(orderRequest);
        result.put("data", orderId);
        result.put("message", "Order Created Successfully");
        log.info("[End OrderController - createOrder]");
        return status(CREATED).body(result);
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<Map<String, Object>> getOrderById(@PathVariable Long orderId) {
        log.info("[Start OrderController - getOrderById = " + orderId + "]");
        Map<String, Object> result = new HashMap<>();
        OrderResponseDetails orderResponseDetails = iOrderService.getOrderById(orderId);
        result.put("data", orderResponseDetails);
        result.put("message", OK);
        log.info("[End OrderController - getOrderById = " + orderId + "]");
        return status(HttpStatus.OK).body(result);
    }

    @GetMapping("/status")
    public ResponseEntity<Map<String, Object>> getAllOrderStatus() {
        log.info("[Start OrderController - getAllOrderStatus]");
        Map<String, Object> result = new HashMap<>();
        List<OrderStatusDto> orderStatusDtoList = iOrderService.getAllOrderStatus();
        result.put("data", orderStatusDtoList);
        result.put("message", OK);
        log.info("[End OrderController - getAllOrderStatus]");
        return status(HttpStatus.OK).body(result);
    }

    @PutMapping("/{orderId}")
    @PreAuthorize("hasAnyAuthority('ADMIN','MANAGER')")
    @ApiOperation(value = "Requires ADMIN or MANAGER Access.")
    public ResponseEntity<Map<String, Object>> updateOrder(@PathVariable Long orderId, @Valid @RequestBody OrderRequest orderRequest) {
        log.info("[Start OrderController - updateOrder ID = " + orderId + "]");
        Map<String, Object> result = new HashMap<>();
        iOrderService.updateOrder(orderRequest, orderId);
        result.put("message", "Order Updated Successfully");
        log.info("[End OrderController - updateOrder ID = " + orderId + "]");
        return status(HttpStatus.OK).body(result);
    }

    @DeleteMapping("/{orderId}")
    @PreAuthorize("hasAnyAuthority('ADMIN','MANAGER')")
    @ApiOperation(value = "Requires ADMIN or MANAGER Access")
    public ResponseEntity<Map<String, Object>> deleteOrderByOrderId(@PathVariable Long orderId) {
        log.info("[Start OrderController - deleteOrderByOrderId = " + orderId + "]");
        Map<String, Object> result = new HashMap<>();
        try {
            iOrderService.deleteOrderByOrderId(orderId);
        } catch (DataIntegrityViolationException e) {
            throw new AppException("SOME_RESOURCE_IS_LINKED");
        }
        result.put("message", "Order Deleted Successfully");
        log.info("[End OrderController - deleteOrderByOrderId = " + orderId + "]");
        return status(HttpStatus.OK).body(result);
    }

    @PutMapping("/{orderId}/{orderStatusId}")
    @PreAuthorize("hasAnyAuthority('ADMIN','MANAGER')")
    @ApiOperation(value = "Requires ADMIN or MANAGER Access")
    public ResponseEntity<Map<String, Object>> updateOrderStatus(@PathVariable Long orderId, @PathVariable Long orderStatusId) {
        log.info("[Start OrderController - updateOrderStatus - Order ID = " + orderId + "]");
        Map<String, Object> result = new HashMap<>();
        iOrderService.updateOrderStatus(orderId, orderStatusId);
        result.put("message", "Update Order Status Successfully");
        log.info("[End OrderController - updateOrderStatus - Order ID = " + orderId + "]");
        return status(HttpStatus.OK).body(result);
    }

    @PostMapping("/{warehouseId}/import")
    public ResponseEntity<String> mapReapExcelDatatoDB(@RequestParam("file") MultipartFile reapExcelDataFile, @PathVariable Long warehouseId) throws IOException {
        log.info("[Start OrderController -  Add Order from Excel  ");
        iOrderService.mapReapExcelDatatoDB(reapExcelDataFile,warehouseId);
        log.info("[End OrderController - Add Order from Excel  ");
        return new ResponseEntity<>("Add Successfully", CREATED);

    }

    @GetMapping("/{warehouseId}/export")
    public void mapWriteExcelDatatoDB(HttpServletResponse response,@PathVariable Long warehouseId) throws IOException {
        log.info("[Start OrderController -  Add Order from Excel  ");
        User currentUser = userCommon.getCurrentUser();
        if (currentUser.getRole().getRoleID() != 1) {
            if (currentUser.getWarehouse().getWarehouseID() !=warehouseId) {
                throw new AppException("CAN_NOT_EXPORT_IN_ANOTHER_WAREHOUSE");
            }
        }
        response.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Order.xlsx";
        response.setHeader(headerKey, headerValue);
        List<Customer> customerList = customerRepository.getAllCustomerInOneWarehouse(warehouseId);
        List<PriceBook> priceBookList = priceBookRepository.getAllPriceBookInOneWarehouse(warehouseId);
        List<ContactDelivery> contactDeliveryList = contactDeliveryRepository.findAll();
        List<PriceBookEntry> priceBookEntries =priceBookEntryRepository.findAll();
        ExportExcelOrder exportExcelOrder = new ExportExcelOrder(priceBookList,customerList,contactDeliveryList,priceBookEntries);
        exportExcelOrder.export(response);
        log.info("[End OrderController - Add Order from Excel  ");


    }

}
