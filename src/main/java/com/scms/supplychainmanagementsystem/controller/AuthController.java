package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.dto.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.service.IAuthService;
import com.scms.supplychainmanagementsystem.service.IRefreshTokenService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static com.scms.supplychainmanagementsystem.common.PrefixUrl.SERVER_REACT;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("/api/auth")
@AllArgsConstructor
@Slf4j
public class AuthController {

    private final IAuthService iAuthService;
    private final IRefreshTokenService iRefreshTokenService;

    @PostMapping("/signup")
    @ApiOperation(value = "[TEST] Create user")
    public ResponseEntity<String> signup(@RequestBody RegisterRequest registerRequest) {
        log.info("[Start AuthController - signup for user: " + registerRequest.getUsername() + "]");
        iAuthService.signup(registerRequest);
        log.info("[End AuthController - signup for user: " + registerRequest.getUsername() + "]");
        return new ResponseEntity<>("User Registration Successful", OK);
    }

    @GetMapping("accountVerification/{token}")
    public ResponseEntity<String> verifyAccount(@PathVariable String token) {
        log.info("[Start AuthController - accountVerification]");
        try {
            iAuthService.verifyAccount(token);
        } catch (AppException ex) {
            return status(EXPECTATION_FAILED).body("We could not verify your account</br>" +
                    "Please contact an administrator to reset your password.");
        }
        log.info("[Start AuthController - accountVerification]");
        return ResponseEntity.status(FOUND).location(URI.create(SERVER_REACT.URL + "/auth/login")).build();
    }

    @PostMapping("/login")
    public ResponseEntity<Map<String, Object>> login(@RequestBody LoginRequest loginRequest) {
        log.info("[Start AuthController - login with username: " + loginRequest.getUsername() + "]");
        Map<String, Object> result = new HashMap<>();
        AuthenticationResponse authenticationResponse = iAuthService.login(loginRequest);
        result.put("data", authenticationResponse);
        result.put("message", OK);
        log.info("[End AuthController - login with username: " + loginRequest.getUsername() + "]");
        return status(HttpStatus.OK).body(result);
    }

    @PostMapping("/refresh/token")
    public ResponseEntity<Map<String, Object>> refreshTokens(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        Map<String, Object> result = new HashMap<>();
        AuthenticationResponse authenticationResponse = iAuthService.refreshToken(refreshTokenRequest);
        result.put("data", authenticationResponse);
        result.put("message", OK);
        return status(HttpStatus.OK).body(result);
    }

    @PostMapping("/logout")
    public ResponseEntity<String> logout(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        iRefreshTokenService.deleteRefreshToken(refreshTokenRequest.getRefreshToken());
        return ResponseEntity.status(OK).body("Refresh Token Deleted Successfully");
    }

    @PostMapping("/forgot-password")
    public ResponseEntity<Map<String, Object>> forgotPassword(@Valid @RequestBody ForgotPasswordRequest forgotPasswordRequest) {
        log.info("[Start AuthController - Forgot Password Username " + forgotPasswordRequest.getUsername() + "]");
        iAuthService.forgotPassword(forgotPasswordRequest);
        Map<String, Object> result = new HashMap<>();
        result.put("message", "Send Request Successfully");
        log.info("[End AuthController - Forgot Password Username " + forgotPasswordRequest.getUsername() + "]");
        return status(HttpStatus.OK).body(result);
    }
}
