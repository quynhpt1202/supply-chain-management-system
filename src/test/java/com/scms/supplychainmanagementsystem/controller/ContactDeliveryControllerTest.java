package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.dto.ContactDeliveryDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.security.JwtProvider;
import com.scms.supplychainmanagementsystem.service.IContactDeliveryService;
import com.scms.supplychainmanagementsystem.service.imp.UserDetailsServiceImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ContactDeliveryController.class)
class ContactDeliveryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IContactDeliveryService mockIContactDeliveryService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtProvider jwtProvider;

    private User user;

    private Role role;


    private String jsonUserRequest;

    @BeforeEach
    void setUp() {

        role = new Role(1L, "ADMIN");
        user = new User(1L, "username", "password", "email"
                , role, new Warehouse(0L, "name", "address"), "firstName", "lastName"
                , true, "phone", LocalDate.now(), null,
                "streetAddress", null, null, null, null);
        jsonUserRequest= "{\n" +
                "  \"address\": \"string\",\n" +
                "  \"contactID\": 0,\n" +
                "  \"contactName\": \"string\",\n" +
                "  \"createdBy\": \"string\",\n" +
                "  \"createdDate\": \"2021-12-20T01:48:40.183Z\",\n" +
                "  \"customerId\": 0,\n" +
                "  \"districtId\": 0,\n" +
                "  \"email\": \"string\",\n" +
                "  \"phone\": \"string\"\n" +
                "}";

    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllContact() throws Exception {
        // Setup
        // Configure IContactDeliveryService.getAllContactDeliveryofcustomer(...).
        final Page<ContactDelivery> contactDeliveryPage = new PageImpl<>(List.of(new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),user)));
        when(mockIContactDeliveryService.getAllContactDeliveryofcustomer(0L, null, PageRequest.of(0, 10))).thenReturn(contactDeliveryPage);

        // Run the test
         mockMvc.perform(get("/api/manage/contact/customer/{customerid}", 0)
                 .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                 .andExpect(status().isOk());

        // Verify the results

    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllContact_IContactDeliveryServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockIContactDeliveryService.getAllContactDeliveryofcustomer(0L, null, PageRequest.of(0, 10))).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        mockMvc.perform(get("/api/manage/contact/customer/{customerid}", 0)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());

        // Verify the results

    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testCreateContact() throws Exception {
        // Setup
        // Run the test
         mockMvc.perform(post("/api/manage/contact")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                .content(jsonUserRequest).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        // Verify the results

        verify(mockIContactDeliveryService).saveContactDelivery(any(ContactDeliveryDto.class));
    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetContactById() throws Exception {
        // Setup
        // Configure IContactDeliveryService.getContactDeliveryById(...).
        final ContactDelivery contactDelivery = new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user);
        when(mockIContactDeliveryService.getContactDeliveryById(0L)).thenReturn(contactDelivery);

        // Run the test
        mockMvc.perform(get("/api/manage/contact/{contactid}", 0)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Verify the results
    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testUpdateContact() throws Exception {
        // Setup
        // Run the test
        mockMvc.perform(put("/api/manage/contact/{contactid}", 0)
                .content(jsonUserRequest).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        // Verify the results
        verify(mockIContactDeliveryService).updateIContactDelivery(eq(0L), any(ContactDeliveryDto.class));
    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testDeleteContact() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/manage/contact/{contactid}", 0)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andExpect(status().isOk())
                .andReturn().getResponse();

        // Verify the results
        verify(mockIContactDeliveryService).deleteContactDelivery(0L);
    }
}
