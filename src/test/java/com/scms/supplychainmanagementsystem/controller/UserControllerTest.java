package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.*;
import com.scms.supplychainmanagementsystem.entity.Role;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import com.scms.supplychainmanagementsystem.security.JwtProvider;
import com.scms.supplychainmanagementsystem.service.IUserService;
import com.scms.supplychainmanagementsystem.service.imp.UserDetailsServiceImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = UserController.class)
class UserControllerTest {

    @MockBean
    private IUserService iUserService;

    @MockBean
    private UserCommon userCommon;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtProvider jwtProvider;

    @InjectMocks
    private UserController userController;

    @Autowired
    private MockMvc mockMvc;

    private User user;

    private Role role;

    private String jsonUserRequest;

    private UserResponse userResponse;

    private UserResponseDetails userResponseDetails;

    private UserRequest userRequest;

    private String jsonChangePasswordRequest;

    private RoleDto roleDto;

    @Mock
    private Page<UserResponse> userResponsePage;

    @BeforeEach
    void setUp() {
        userController = new UserController(iUserService, userCommon);
        role = new Role(1L, "ADMIN");
        user = new User(1L, "username", "password", "email"
                , role, new Warehouse(0L, "name", "address"), "firstName", "lastName"
                , true, "phone", LocalDate.now(), null,
                "streetAddress", null, null, null, null);
        jsonUserRequest = "{\n" +
                "    \"username\": \"admin\",\n" +
                "    \"email\": \"scmsdev@gmail.com\",\n" +
                "    \"roleId\": 1,\n" +
                "    \"warehouseId\": 0,\n" +
                "    \"firstName\": \"Tuấn\",\n" +
                "    \"lastName\": \"Bùi\",\n" +
                "    \"phone\": \"0383007243\",\n" +
                "    \"dateOfBirth\": \"2021-11-24\",\n" +
                "    \"districtId\": 73,\n" +
                "    \"streetAddress\": \"Khâm Thiên\",\n" +
                "    \"active\": true\n" +
                "  }";
        userResponse = UserResponse.builder().userId(1L).username("username")
                .role(new Role(1L, "ADMIN"))
                .warehouse(new Warehouse(0L, "name", "address"))
                .firstName("firstName").build();
        userResponseDetails = UserResponseDetails.builder().userId(1L).username("username").build();
        userRequest = new UserRequest("username1", "email@ex.com"
                , 1L, 1L, "firstName", "lastName"
                , true, "phone", LocalDate.now(), 1L,
                "streetAddress");
        jsonChangePasswordRequest = "{\n" +
                "    \"currentPassword\": \"currentPassword\",\n" +
                "    \"newPassword\": \"newPassword\"\n" +
                "  }";
        roleDto = new RoleDto(1L, "role");
    }

    @SneakyThrows
    @Test
    @WithMockUser
    void getUserProfileSuccess() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/manage/user/me"))
                .andExpect(status().isOk());

    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    void createUserSuccess() {
        iUserService.saveUser(any(UserRequest.class));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/manage/user")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                        .content(jsonUserRequest).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @SneakyThrows
    @Test
    @WithAnonymousUser
    void createUserWhenNotLogin() {
        iUserService.saveUser(any(UserRequest.class));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/manage/user")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                        .content(jsonUserRequest).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    void getAllUsersInWarehouseSuccess() {
        when(iUserService.getAllUsers(null, null, null, PageRequest.of(0, 10)))
                .thenReturn(userResponsePage);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/manage/user")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    void getUserByIdSuccess() {
        when(iUserService.getUserById(anyLong())).thenReturn(userResponseDetails);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/manage/user/1")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.userId").value(1));
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    void updateUserSuccess() {
        iUserService.updateUser(userRequest, 1L);
        mockMvc.perform(MockMvcRequestBuilders.put("/api/manage/user/1")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                        .content(jsonUserRequest).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    void deleteUserSuccess() {
        iUserService.deleteUser(1L);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/manage/user/1")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    void deleteUserException() {
//        doThrow( DataIntegrityViolationException.class).when(iUserService).deleteUser(anyLong());
//        mockMvc.perform(MockMvcRequestBuilders.delete("/api/manage/user/1")
//                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
//                .andExpect(status().isInternalServerError())
//                .andExpect(jsonPath("$.message").value("SOME_RESOURCE_IS_LINKED"));
    }

    @SneakyThrows
    @Test
    @WithMockUser
    void changePassword() {
        iUserService.changePassword(any(ChangePasswordRequest.class));
        mockMvc.perform(MockMvcRequestBuilders.put("/api/manage/user/change-password")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                        .content(jsonChangePasswordRequest).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser
    void getAllRoles() {
        when(iUserService.getAllRoles()).thenReturn(List.of(roleDto));
        mockMvc.perform(MockMvcRequestBuilders.get("/api/manage/user/list-role")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    void updateUserActive() {
        iUserService.updateUserActive(1L, true);
        mockMvc.perform(MockMvcRequestBuilders.put("/api/manage/user/1/true")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"STAFF"})
    void updateUserActiveException() {
        iUserService.updateUserActive(1L, true);
        mockMvc.perform(MockMvcRequestBuilders.put("/api/manage/user/1/true")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isForbidden());
    }
}
