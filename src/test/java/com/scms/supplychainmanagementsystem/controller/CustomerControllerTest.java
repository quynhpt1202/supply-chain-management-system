package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.CustomerDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.security.JwtProvider;
import com.scms.supplychainmanagementsystem.service.ICustomerService;
import com.scms.supplychainmanagementsystem.service.imp.UserDetailsServiceImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CustomerController.class)
class CustomerControllerTest {

    @MockBean
    private ICustomerService iCustomerService;

    @MockBean
    private UserCommon userCommon;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtProvider jwtProvider;

    @InjectMocks
    private CustomerController customerController;

    private User user;

    private Customer customer;

    private Warehouse warehouse;

    private Warehouse warehouse2;

    private District district;

    private Role role;

    private CustomerDto customerDto;

    Pageable pageable;

    @Mock
    private Page<Customer> customerPage;


    @BeforeEach
    void setUp() {
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
        warehouse = new Warehouse(0L, "name", "address");
        customer = new Customer(1L,"KH0001","type","CustomerName",warehouse,"email@gmail.com","0987654321", LocalDate.now(),
                true,"HNN","fb","fpt","note",null,user,null);


    }


    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    void getAllCustomerInWarehouse() {

        when(iCustomerService.getAllCustomer(null, null,  PageRequest.of(0, 10)))
                .thenReturn(customerPage);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/manage/customer")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @Test
    void createCustomer() {
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = {"ADMIN"})
    void getCustomerByIdInWareHouse() {
        when(iCustomerService.getCustomerByIdInWarehouse(anyLong())).thenReturn(customer);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/manage/customer/1")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @Test
    void updateCustomer() {
    }

    @Test
    void deleteCustomer() {
    }

    @Test
    void mapReapExcelDatatoDB() {
    }

    @Test
    void mapWriteExcelDatatoDB() {
    }
}