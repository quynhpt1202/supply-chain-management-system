package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.dto.CategoryDto;
import com.scms.supplychainmanagementsystem.entity.Category;
import com.scms.supplychainmanagementsystem.security.JwtProvider;
import com.scms.supplychainmanagementsystem.service.ICategoryService;
import com.scms.supplychainmanagementsystem.service.imp.UserDetailsServiceImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CategoryController.class)
class CategoryControllerTest {

    @MockBean
    private ICategoryService iCategoryService;

    @InjectMocks
    private CategoryController categoryController;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtProvider jwtProvider;

    @Autowired
    private MockMvc mockMvc;

    private String jsonDto;

    @Mock
    private Page<Category> categoryPage;

    private CategoryDto categoryDto;

    @BeforeEach
    void setUp() {
        categoryController = new CategoryController(iCategoryService);
        jsonDto = "{\n" +
                "  \"categoryId\": 1,\n" +
                "  \"categoryName\": \"category\",\n" +
                "  \"warehouseId\": 1\n" +
                "}";
        categoryDto = new CategoryDto(1L, 1L, "category");
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = "ADMIN")
    void createCategorySuccess() {
        iCategoryService.createCategory(any(CategoryDto.class));
        mockMvc.perform(MockMvcRequestBuilders.post("/api/category")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                        .content(jsonDto).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @SneakyThrows
    @Test
    @WithMockUser
    void getAllCategoriesSuccess() {
        when(iCategoryService.getAllCategories(null, null, PageRequest.of(0, 10)))
                .thenReturn(categoryPage);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/category")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser
    void getAllCategoriesEmpty() {
        List<Category> list = Collections.emptyList();
        categoryPage = new PageImpl<>(list);
        when(iCategoryService.getAllCategories(null, null, PageRequest.of(0, 10)))
                .thenReturn(categoryPage);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/category")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").value("EMPTY_RESULT"));
    }

    @Test
    @SneakyThrows
    @WithMockUser
    void getCategoryByIdSuccess() {
        when(iCategoryService.getCategoryById(anyLong())).thenReturn(categoryDto);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/category/1")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.categoryId").value(1));

    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = "ADMIN")
    void updateCategorySuccess() {
        iCategoryService.updateCategory(any(CategoryDto.class));
        mockMvc.perform(MockMvcRequestBuilders.put("/api/category/1")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                        .content(jsonDto).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @SneakyThrows
    @Test
    @WithMockUser(authorities = "STAFF")
    void updateCategoryWhenForbidden() {
        iCategoryService.updateCategory(any(CategoryDto.class));
        mockMvc.perform(MockMvcRequestBuilders.put("/api/category/1")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                        .content(jsonDto).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }


    @Test
    @SneakyThrows
    @WithMockUser(authorities = "ADMIN")
    void deleteCategoryById() {
        iCategoryService.deleteCategoryById(1L);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/category/1")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                        .content(jsonDto).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    @WithMockUser(authorities = "STAFF")
    void deleteCategoryByIdWhenForbidden() {
        iCategoryService.deleteCategoryById(1L);
        mockMvc.perform(MockMvcRequestBuilders.delete("/api/category/1")
                        .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                        .content(jsonDto).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}
