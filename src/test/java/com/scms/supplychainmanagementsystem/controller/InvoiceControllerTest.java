package com.scms.supplychainmanagementsystem.controller;

import com.mysql.cj.x.protobuf.Mysqlx;
import com.scms.supplychainmanagementsystem.dto.InvoiceDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.security.JwtProvider;
import com.scms.supplychainmanagementsystem.service.IInvoiceService;
import com.scms.supplychainmanagementsystem.service.imp.UserDetailsServiceImpl;
import com.scms.supplychainmanagementsystem.utils.SendMailJob;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(InvoiceController.class)
class InvoiceControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IInvoiceService mockIInvoiceService;
    @MockBean
    private SendMailJob mockSendMailJob;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtProvider jwtProvider;

    private User user;

    private Role role;

    private  Page<Invoice> invoicePage;

    private String jsonUserRequest;

    @BeforeEach
    void setUp() {

        role = new Role(1L, "ADMIN");
        user = new User(1L, "username", "password", "email"
                , role, new Warehouse(0L, "name", "address"), "firstName", "lastName"
                , true, "phone", LocalDate.now(), null,
                "streetAddress", null, null, null, null);
        invoicePage = new PageImpl<>(List.of(new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"), user, user), new Warehouse(0L, "warehouseName", "address"), user)));
           jsonUserRequest= "{\n" +
                   "  \"paymentDueDate\": \"2021-12-20\",\n" +
                   "  \"totalAmountPaid\": 2\n" +
                   "}";

    }
    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllInvoice() throws Exception {
        // Setup
        // Configure IInvoiceService.getAllInvoice(...).
        final Page<Invoice> invoices = new PageImpl<>(List.of(new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC),  user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"),  user, user), new Warehouse(0L, "warehouseName", "address"), user)));
        when(mockIInvoiceService.getAllInvoice(null, null, PageRequest.of(0, 10))).thenReturn(invoices);

        // Run the test
         mockMvc.perform(get("/api/invoice")
                 .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                 .andExpect(status().isOk());



    }
    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllInvoice_IInvoiceServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockIInvoiceService.getAllInvoice(null, null,  PageRequest.of(0, 10))).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
         mockMvc.perform(get("/api/invoice")
                 .accept(MediaType.APPLICATION_JSON))
                 .andExpect(status().isOk());


    }
    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetInvoiceById() throws Exception {
        // Setup
        // Configure IInvoiceService.getInvoiceByIdInWarehouse(...).
        final Invoice invoice = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC),  user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"),  user, user), new Warehouse(0L, "warehouseName", "address"), user);
        when(mockIInvoiceService.getInvoiceByIdInWarehouse(0L)).thenReturn(invoice);

        // Run the test
         mockMvc.perform(get("/api/invoice/{invoiceId}", 0)
                 .accept(MediaType.APPLICATION_JSON))
                 .andExpect(status().isOk());


    }
    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetInvoiceByOrderId() throws Exception {
        // Setup
        // Configure IInvoiceService.getInvoiceByOrderIdInWarehouse(...).
        final Invoice invoice = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC),  user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"),  user, user), new Warehouse(0L, "warehouseName", "address"),user);
        when(mockIInvoiceService.getInvoiceByOrderIdInWarehouse(0L)).thenReturn(invoice);

        // Run the test
        mockMvc.perform(get("/api/invoice/order/{orderId}", 0)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


    }


    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testUpdateInvoice() throws Exception {
        // Setup
        // Run the test
         mockMvc.perform(put("/api/invoice/{invoiceId}", 0)
                 .content(jsonUserRequest).contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andExpect(status().isOk());

        verify(mockIInvoiceService).updateInvoice(eq(0L), any(InvoiceDto.class));
    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testSendInvoiceMail() throws Exception {
        // Setup
        // Configure SendMailJob.sendInvoiceMail(...).
        final HashMap<String, Boolean> stringBooleanHashMap = new HashMap<>(Map.ofEntries(Map.entry("value", false)));
        when(mockSendMailJob.sendInvoiceMail(0L, "jobName")).thenReturn(stringBooleanHashMap);

        // Run the test
         mockMvc.perform(get("/api/invoice/send-mail")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());


    }
}
