package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.dto.*;
import com.scms.supplychainmanagementsystem.entity.Material;
import com.scms.supplychainmanagementsystem.entity.Role;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import com.scms.supplychainmanagementsystem.security.JwtProvider;
import com.scms.supplychainmanagementsystem.service.IMaterialService;
import com.scms.supplychainmanagementsystem.service.imp.UserDetailsServiceImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MaterialController.class)
class MaterialControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IMaterialService mockIMaterialService;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtProvider jwtProvider;

    private User user;

    private Role role;

    private  Page<Material> materialPage;

    private String jsonUserRequest;



    @BeforeEach
    void setUp() {
//        userController = new UserController(iUserService, userCommon);
        jsonUserRequest ="{\n" +
                "  \"createdBy\": \"string\",\n" +
                "  \"createdDate\": \"2021-12-18T16:05:45.661Z\",\n" +
                "  \"lastModifiedBy\": \"string\",\n" +
                "  \"lastModifiedDate\": \"2021-12-18T16:05:45.661Z\",\n" +
                "  \"materialID\": 0,\n" +
                "  \"materialName\": \"string\",\n" +
                "  \"quantityUnitOfMeasure\": \"string\",\n" +
                "  \"warehouseId\": 0\n" +
                "}";
        role = new Role(1L, "ADMIN");
        user = new User(1L, "username", "password", "email"
                , role, new Warehouse(0L, "name", "address"), "firstName", "lastName"
                , true, "phone", LocalDate.now(), null,
                "streetAddress", null, null, null, null);
         materialPage = new PageImpl<>(List.of(new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), user, user)));

    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllMaterialInWarehouse() throws Exception {
        // Setup
        // Configure IMaterialService.getAllMaterial(...).
        when(mockIMaterialService.getAllMaterial(null, null, PageRequest.of(0, 10))).thenReturn(materialPage);

        // Run the test
         mockMvc.perform(get("/api/manage/material")
               .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());

        // Verify the results
//        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
//        assertThat(response.getContentAsString()).isEqualTo("expectedResponse");
    }
//    @SneakyThrows
//    @WithMockUser(authorities = {"ADMIN"})
//    @Test
//    void testGetAllMaterialInWarehouse_IMaterialServiceReturnsNoItems() throws Exception {
//        // Setup
//        when(mockIMaterialService.getAllMaterial(eq("materialname"), eq(0L), PageRequest.of(0, 10))).thenReturn((materialPage));
//
//        // Run the test
//         mockMvc.perform(get("/api/manage/material")
//                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andExpect(status().isOk())
//                .andReturn().getResponse();
//
//        // Verify the results
////        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
////        assertThat(response.getContentAsString()).isEqualTo("expectedResponse");
//    }
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetMaterialInWarehouse() throws Exception {
        // Setup
        // Configure IMaterialService.getMaterialInWareHouse(...).
        final List<Material> materials = List.of(new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new User(), new User()));
        when(mockIMaterialService.getMaterialInWareHouse(0L)).thenReturn(materials);

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/manage/material/warehouse/{warehouseid}", 0)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
//        assertThat(response.getContentAsString()).isEqualTo("expectedResponse");
    }
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetMaterialInWarehouse_IMaterialServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockIMaterialService.getMaterialInWareHouse(0L)).thenReturn(Collections.emptyList());

        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(get("/api/manage/material/warehouse/{warehouseid}", 0)
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
        assertThat(response.getContentAsString()).isEqualTo("[]");
    }
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testCreateMaterial() throws Exception {
        // Setup
        // Run the test
         mockMvc.perform(post("/api/manage/material")
                 .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                 .content(jsonUserRequest).accept(MediaType.APPLICATION_JSON))
                 .andExpect(status().isCreated());



        // Verify the results
        verify(mockIMaterialService).saveMaterial(any(MaterialDto.class));
    }
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetMaterialByIdInWareHouse() throws Exception {
        // Setup
        // Configure IMaterialService.getMaterialByIdInWarehouse(...).
        final Material material = new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new User(), new User());
        when(mockIMaterialService.getMaterialByIdInWarehouse(0L)).thenReturn(material);

        // Run the test
          mockMvc.perform(get("/api/manage/material/{materialId}", 0)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Verify the results

    }
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testUpdateMaterial() throws Exception {
        // Setup
        // Run the test
        mockMvc.perform(put("/api/manage/material/{materialId}", 0)
                .content(jsonUserRequest).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
        // Verify the results

        verify(mockIMaterialService).updateMaterial(eq(0L), any(MaterialDto.class));
    }
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testDeleteMaterial() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/manage/material/{materialId}", 0)
                 .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andExpect(status().isOk())
                .andReturn().getResponse();

        // Verify the results
        verify(mockIMaterialService).deleteMaterial(0L);
    }
}
