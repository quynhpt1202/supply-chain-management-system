package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.StockHistoryDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.repository.ProductRepository;
import com.scms.supplychainmanagementsystem.security.JwtProvider;
import com.scms.supplychainmanagementsystem.service.IStockHistoryService;
import com.scms.supplychainmanagementsystem.service.imp.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(StockHistoryController.class)
class StockHistoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IStockHistoryService mockIStockHistoryService;
    @MockBean
    private UserCommon mockUserCommon;
    @MockBean
    private ProductRepository mockProductRepository;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtProvider jwtProvider;

    private User user;

    private Role role;


    private String jsonUserRequest;

    @BeforeEach
    void setUp() {

        jsonUserRequest= "{\n" +
                "  \"createdBy\": \"string\",\n" +
                "  \"createdDate\": \"2021-12-20T03:12:55.344Z\",\n" +
                "  \"lastModifiedBy\": \"string\",\n" +
                "  \"lastModifiedDate\": \"2021-12-20T03:12:55.344Z\",\n" +
                "  \"productId\": 0,\n" +
                "  \"stockHistoryID\": 0,\n" +
                "  \"stockInQuantity\": 0,\n" +
                "  \"unitCostPrice\": 0,\n" +
                "  \"warehouseId\": 0\n" +
                "}";
        role = new Role(1L, "ADMIN");
        user = new User(1L, "username", "password", "email"
                , role, new Warehouse(0L, "name", "address"), "firstName", "lastName"
                , true, "phone", LocalDate.now(), null,
                "streetAddress", null, null, null, null);


    }


    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testCreateStockHistory() throws Exception {
        // Setup
        // Run the test
         mockMvc.perform(post("/api/stockhistory")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                .content(jsonUserRequest).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        // Verify the results
        verify(mockIStockHistoryService).saveStockHistory(any(StockHistoryDto.class));
    }

    @WithMockUser(authorities = {"MANAGER"})
    @Test
    void testCreateStockHistoryManager() throws Exception {
        // Setup
        // Run the test
        mockMvc.perform(post("/api/stockhistory")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                .content(jsonUserRequest).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        // Verify the results
        verify(mockIStockHistoryService).saveStockHistory(any(StockHistoryDto.class));
    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllStockHistory() throws Exception {
        // Setup
        // Configure IStockHistoryService.getAllStockHistory(...).
        final Page<StockHistory> stockHistories = new PageImpl<>(List.of(new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user)));
        when(mockIStockHistoryService.getAllStockHistory(null,null, PageRequest.of(0, 10))).thenReturn(stockHistories);

        // Run the test
         mockMvc.perform(get("/api/stockhistory")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());

        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllStockHistory_IStockHistoryServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockIStockHistoryService.getAllStockHistory(null,null, PageRequest.of(0, 10))).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
        mockMvc.perform(get("/api/stockhistory")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());

        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetStockHistoryById() throws Exception {
        // Setup
        // Configure IStockHistoryService.getStockHistoryByIdInWarehouse(...).
        final StockHistory stockHistory = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
        when(mockIStockHistoryService.getStockHistoryByIdInWarehouse(0L)).thenReturn(stockHistory);

        // Run the test
        mockMvc.perform(get("/api/stockhistory/{stockhistoryid}", 0)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testUpdateStockHistory() throws Exception {
        // Setup
        // Run the test
        mockMvc.perform(put("/api/stockhistory/{stockhistoryid}", 0)
                .content(jsonUserRequest).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        // Verify the results

        verify(mockIStockHistoryService).updateStockHistory(eq(0L), any(StockHistoryDto.class));
    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testDeleteStockHistory() throws Exception {
        // Setup
        // Run the test
         mockMvc.perform(delete("/api/stockhistory/{stockhistoryid}", 0)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andExpect(status().isOk())
                .andReturn().getResponse();

        // Verify the results

        verify(mockIStockHistoryService).deleteStockHistory(0L);
    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testMapReapExcelDatatoDB() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(multipart("/api/stockhistory/{warehouseId}/import", 0)
                .file(new MockMultipartFile("file", "originalFilename", MediaType.APPLICATION_JSON_VALUE, "content".getBytes()))
                .accept(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();

        // Verify the results

        verify(mockIStockHistoryService).mapReapExcelDatatoDB(any(MultipartFile.class), eq(0L));
    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testMapReapExcelDatatoDB_IStockHistoryServiceThrowsIOException() throws Exception {
        // Setup


        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testMapWriteExcelDatatoDB() throws Exception {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(new User());

        // Configure ProductRepository.findAllByWarehouseID(...).
        final List<Product> productList = List.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user));
        when(mockProductRepository.findAllByWarehouseID(0L)).thenReturn(productList);

        // Run the test


        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testMapWriteExcelDatatoDB_ProductRepositoryReturnsNoItems() throws Exception {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(new User());
        when(mockProductRepository.findAllByWarehouseID(0L)).thenReturn(Collections.emptyList());

        // Run the test


        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testMapWriteExcelDatatoDB_ThrowsIOException() throws Exception {
        // Setup
//        when(mockUserCommon.getCurrentUser()).thenReturn(new User());
//
//        // Configure ProductRepository.findAllByWarehouseID(...).
//        final List<Product> productList = List.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user));
//        when(mockProductRepository.findAllByWarehouseID(0L)).thenReturn(productList);
//
//        // Run the test
//        final MockHttpServletResponse response = mockMvc.perform(get("/api/stockhistory/{warehouseId}/export", 0)
//                .accept(MediaType.APPLICATION_JSON))
//                .andReturn().getResponse();

        // Verify the results

    }
}
