package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.dto.InventoryDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.repository.InvProductStatusRepository;
import com.scms.supplychainmanagementsystem.security.JwtProvider;
import com.scms.supplychainmanagementsystem.service.IInventoryService;
import com.scms.supplychainmanagementsystem.service.imp.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(InventoryController.class)
class InventoryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IInventoryService mockIInventoryService;
    @MockBean
    private InvProductStatusRepository mockInvProductStatusRepository;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtProvider jwtProvider;

    private User user;

    private Role role;


    private String jsonUserRequest;

    @BeforeEach
    void setUp() {

        jsonUserRequest= "{\n" +
                "  \"dateCheck\": \"2021-12-20\",\n" +
                "  \"description\": \"string\",\n" +
                "  \"personCheck\": \"string\",\n" +
                "  \"productId\": 0,\n" +
                "  \"shortageQuantity\": 3,\n" +
                "  \"statusId\": 1,\n" +
                "  \"warehouseId\": 0\n" +
                "}";
        role = new Role(1L, "ADMIN");
        user = new User(1L, "username", "password", "email"
                , role, new Warehouse(0L, "name", "address"), "firstName", "lastName"
                , true, "phone", LocalDate.now(), null,
                "streetAddress", null, null, null, null);


    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testCreateInventory() throws Exception {
        // Setup
        // Run the test
         mockMvc.perform(post("/api/inventory")
                 .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                 .content(jsonUserRequest).accept(MediaType.APPLICATION_JSON))
                 .andExpect(status().isCreated());


        // Verify the results
        verify(mockIInventoryService).saveInventory(any(InventoryDto.class));
    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllInventory() throws Exception {
        // Setup
        // Configure IInventoryService.getAllInventory(...).
        final Page<Inventory> inventories = new PageImpl<>(List.of(new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user)));

        when(mockIInventoryService.getAllInventory(null,null, PageRequest.of(0, 10))).thenReturn(inventories);

        // Run the test
         mockMvc.perform(get("/api/inventory")
                 .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                 .andExpect(status().isOk());

        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllInventory_IInventoryServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockIInventoryService.getAllInventory(null,null, PageRequest.of(0, 10))).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
       mockMvc.perform(get("/api/inventory")
               .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
               .andExpect(status().isOk());

        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetInventoryById() throws Exception {
        // Setup
        // Configure IInventoryService.getInventoryByIdInWarehouse(...).
        final Inventory inventory = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"),user, user);
        when(mockIInventoryService.getInventoryByIdInWarehouse(0L)).thenReturn(inventory);

        // Run the test
        mockMvc.perform(get("/api/inventory/{inventoryId}", 0)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testUpdateInventory() throws Exception {
        // Setup
        // Run the test
        mockMvc.perform(put("/api/inventory/{inventoryId}", 0)
                .content(jsonUserRequest).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        // Verify the results
        verify(mockIInventoryService).updateInventory(eq(0L), any(InventoryDto.class));
    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testDeleteInventory() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/inventory/{inventoryId}", 0)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andExpect(status().isOk())
                .andReturn().getResponse();

        // Verify the results
        verify(mockIInventoryService).deleteInventory(0L);
    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllStatus() throws Exception {
        // Setup
        // Configure InvProductStatusRepository.findAll(...).
        final List<InvProductStatus> list = List.of(new InvProductStatus(0L, "prodStatusDescription"));
        when(mockInvProductStatusRepository.findAll()).thenReturn(list);

        // Run the test
         mockMvc.perform(get("/api/inventory/liststatus")
                 .accept(MediaType.APPLICATION_JSON))
                 .andExpect(status().isOk());

        // Verify the results

    }

    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllStatus_InvProductStatusRepositoryReturnsNoItems() throws Exception {
        // Setup
        when(mockInvProductStatusRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        mockMvc.perform(get("/api/inventory/liststatus")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        // Verify the results

    }
}
