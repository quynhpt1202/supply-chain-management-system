package com.scms.supplychainmanagementsystem.controller;

import com.scms.supplychainmanagementsystem.dto.ProductRequest;
import com.scms.supplychainmanagementsystem.dto.ProductResponse;
import com.scms.supplychainmanagementsystem.entity.Material;
import com.scms.supplychainmanagementsystem.entity.Role;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import com.scms.supplychainmanagementsystem.security.JwtProvider;
import com.scms.supplychainmanagementsystem.service.IProductService;
import com.scms.supplychainmanagementsystem.service.imp.UserDetailsServiceImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ProductController.class)
class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IProductService mockIProductService;
    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtProvider jwtProvider;

    private User user;

    private Role role;


    private String jsonUserRequest;


    @BeforeEach
    void setUp() {

        jsonUserRequest ="{\n" +
                "  \"categoryId\": 0,\n" +
                "  \"isActive\": true,\n" +
                "  \"productId\": 0,\n" +
                "  \"productName\": \"string\",\n" +
                "  \"quantityUnitOfMeasure\": \"string\",\n" +
                "  \"warehouseId\": 0\n" +
                "}";
        role = new Role(1L, "ADMIN");
        user = new User(1L, "username", "password", "email"
                , role, new Warehouse(0L, "name", "address"), "firstName", "lastName"
                , true, "phone", LocalDate.now(), null,
                "streetAddress", null, null, null, null);

    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllProducts() throws Exception {
        // Setup
        // Configure IProductService.getAllProducts(...).
        final Page<ProductResponse> productResponses = new PageImpl<>(List.of(new ProductResponse(0L, "productName", "warehouseName", "categoryName", "quantityUnitOfMeasure", false, 0.0)));
        when(mockIProductService.getAllProducts(null, null, null, null, PageRequest.of(0, 10))).thenReturn(productResponses);

        // Run the test
         mockMvc.perform(get("/api/product")
                 .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                 .andExpect(status().isOk());

        // Verify the results

    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetAllProducts_IProductServiceReturnsNoItems() throws Exception {
        // Setup
        when(mockIProductService.getAllProducts(null, null, null, null, PageRequest.of(0, 10))).thenReturn(new PageImpl<>(Collections.emptyList()));

        // Run the test
         mockMvc.perform(get("/api/product")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());

        // Verify the results

    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testCreateProduct() throws Exception {
        // Setup
        // Run the test
         mockMvc.perform(post("/api/product")
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")
                .content(jsonUserRequest).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        // Verify the results

        verify(mockIProductService).createProduct(any(ProductRequest.class));
    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testGetProductById() throws Exception {
        // Setup
        // Configure IProductService.getProductById(...).
        final ProductRequest productRequest = new ProductRequest(0L, 0L, "productName", 0L, "quantityUnitOfMeasure", false);
        when(mockIProductService.getProductById(0L)).thenReturn(productRequest);

        // Run the test
        mockMvc.perform(get("/api/product/{productId}", 0)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
                .andExpect(status().isOk());

        // Verify the results

    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testUpdateProduct() throws Exception {
        // Setup
        // Run the test
         mockMvc.perform(put("/api/product/{productId}", 0)
                .content(jsonUserRequest).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        // Verify the results

        verify(mockIProductService).updateProduct(any(ProductRequest.class));
    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testDeleteProductByProductId() throws Exception {
        // Setup
        // Run the test
        final MockHttpServletResponse response = mockMvc.perform(delete("/api/product/{productId}", 0)
                .contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andExpect(status().isOk())
                .andReturn().getResponse();

        // Verify the results

        verify(mockIProductService).deleteProductByProductId(0L);
    }

    @SneakyThrows
    @WithMockUser(authorities = {"ADMIN"})
    @Test
    void testUpdateProductActive() throws Exception {
        // Setup
        when(mockIProductService.checkProductExist(0L)).thenReturn(true);

        // Run the test
         mockMvc.perform(put("/api/product/{productId}/{isActive}", 0, false)
                .content(jsonUserRequest).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        // Verify the results

        verify(mockIProductService).updateProductActive(0L, false);
    }
}
