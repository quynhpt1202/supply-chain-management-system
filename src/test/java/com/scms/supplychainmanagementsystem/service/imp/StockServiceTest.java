package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.repository.OrderDetailsRepository;
import com.scms.supplychainmanagementsystem.repository.ProductRepository;
import com.scms.supplychainmanagementsystem.repository.StockRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.never;

@ExtendWith(MockitoExtension.class)
class StockServiceTest {

    @Mock
    private UserCommon mockUserCommon;
    @Mock
    private StockRepository mockStockRepository;
    @Mock
    private ProductRepository mockProductRepository;
    @Mock
    private OrderDetailsRepository mockOrderDetailsRepository;
    @Mock
    private OrderDetailsService mockOrderDetailsService;

    private StockService stockServiceUnderTest;

    private Role role;

    private Warehouse warehouse;

    private User user;

    private District district;

    @BeforeEach
    void setUp() throws Exception {
        stockServiceUnderTest = new StockService(mockUserCommon, mockStockRepository, mockProductRepository, mockOrderDetailsRepository, mockOrderDetailsService);
        role = new Role(1L, "ADMIN");
        district = new District(1L, "name", "type", new Province());
        warehouse = new Warehouse(0L, "name", "address");
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
    }

    @Test
    void testGetAllStockAdminAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockRepository.filterAllWarehousesAdmin(...).
        final Page<Stock> stocks = new PageImpl<>(List.of(new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC))));
        when(mockStockRepository.filterAllWarehousesAdmin(eq("productName"), eq(0L), any(Pageable.class))).thenReturn(stocks);

        // Configure StockRepository.filteroneWarehouses(...).
//        final Page<Stock> stocks1 = new PageImpl<>(List.of(new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC))));
//        when(mockStockRepository.filteroneWarehouses(eq("productName"), eq(0L), any(Pageable.class))).thenReturn(stocks1);

        // Run the test
        final Page<Stock> result = stockServiceUnderTest.getAllStockAdmin("productName", 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockStockRepository, times(1)).filterAllWarehousesAdmin(eq("productName"), eq(0L), any(Pageable.class));
        verify(mockStockRepository, never()).filteroneWarehouses(eq("productName"), eq(0L), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }

    @Test
    void testGetAllStockAdminManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockRepository.filterAllWarehousesAdmin(...).
//        final Page<Stock> stocks = new PageImpl<>(List.of(new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC))));
//        when(mockStockRepository.filterAllWarehousesAdmin(eq("productName"), eq(0L), any(Pageable.class))).thenReturn(stocks);

        // Configure StockRepository.filteroneWarehouses(...).
        final Page<Stock> stocks1 = new PageImpl<>(List.of(new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC))));
        when(mockStockRepository.filteroneWarehouses(eq("productName"), eq(0L), any(Pageable.class))).thenReturn(stocks1);

        // Run the test
        final Page<Stock> result = stockServiceUnderTest.getAllStockAdmin("productName", 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockStockRepository, times(1)).filteroneWarehouses(eq("productName"), eq(0L), any(Pageable.class));
        verify(mockStockRepository, never()).filterAllWarehousesAdmin(eq("productName"), eq(0L), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }



    @Test
    void testUpdateStockQuantityIncrease() {
        // Setup
        // Configure StockRepository.findByProductId(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), 10.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.findByProductId(0L)).thenReturn(stock);

        // Configure ProductRepository.getById(...).
        final Product product = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user);
        when(mockProductRepository.getById(0L)).thenReturn(product);

        // Configure OrderDetailsRepository.getOrderDetailsByProductId(...).
        final List<OrderDetails> orderDetailsList = List.of(new OrderDetails(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), new User(), new User()), 0.0, 0.0, new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"), user, user), new PriceBook(0L, "priceBookName", false, new Warehouse(0L, "warehouseName", "address")), new OrderDetailsStatus(0L, "status")));
        when(mockOrderDetailsRepository.getOrderDetailsByProductId(0L)).thenReturn(orderDetailsList);

        // Configure OrderDetailsRepository.getOrderDetailsByProductIdDesc(...).
//        final List<OrderDetails> orderDetailsList1 = List.of(new OrderDetails(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), new User(), new User()), 0.0, 0.0, new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"), user, user), new PriceBook(0L, "priceBookName", false, new Warehouse(0L, "warehouseName", "address")), new OrderDetailsStatus(0L, "status")));
//        when(mockOrderDetailsRepository.getOrderDetailsByProductIdDesc(0L)).thenReturn(orderDetailsList1);

        // Configure StockRepository.save(...).
        final Stock stock1 = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), new User(), new User()), 10.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.save(any(Stock.class))).thenReturn(stock1);

        // Run the test
        stockServiceUnderTest.updateStockQuantity(0L, 10.0);

        // Verify the results
//        verify(mockOrderDetailsService).updateStatusOrderDetails(0L, 0L);
        verify(mockStockRepository).save(any(Stock.class));
    }
    @Test
    void testUpdateStockQuantityReduction() {
        // Setup
        // Configure StockRepository.findByProductId(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), -10.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.findByProductId(0L)).thenReturn(stock);

        // Configure ProductRepository.getById(...).
        final Product product = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user);
        when(mockProductRepository.getById(0L)).thenReturn(product);

        // Configure OrderDetailsRepository.getOrderDetailsByProductId(...).
//        final List<OrderDetails> orderDetailsList = List.of(new OrderDetails(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), new User(), new User()), 0.0, 0.0, new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"), user, user), new PriceBook(0L, "priceBookName", false, new Warehouse(0L, "warehouseName", "address")), new OrderDetailsStatus(0L, "status")));
//        when(mockOrderDetailsRepository.getOrderDetailsByProductId(0L)).thenReturn(orderDetailsList);

        // Configure OrderDetailsRepository.getOrderDetailsByProductIdDesc(...).
        final List<OrderDetails> orderDetailsList1 = List.of(new OrderDetails(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), new User(), new User()), 0.0, 0.0, new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"), user, user), new PriceBook(0L, "priceBookName", false, new Warehouse(0L, "warehouseName", "address")), new OrderDetailsStatus(0L, "status")));
        when(mockOrderDetailsRepository.getOrderDetailsByProductIdDesc(0L)).thenReturn(orderDetailsList1);

        // Configure StockRepository.save(...).
        final Stock stock1 = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), new User(), new User()), -10.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.save(any(Stock.class))).thenReturn(stock1);

        // Run the test
        stockServiceUnderTest.updateStockQuantity(0L, -10.0);

        // Verify the results
//        verify(mockOrderDetailsService).updateStatusOrderDetails(0L, 0L);
        verify(mockStockRepository).save(any(Stock.class));
    }


}
