package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.CustomerDto;
import com.scms.supplychainmanagementsystem.dto.MaterialDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.MaterialRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MaterialServiceTest {
    @Mock
    private MaterialRepository materialRepository;
    @Mock
    private  UserCommon userCommon;
    @Mock
    private WarehouseRepository warehouseRepository;

    @InjectMocks
    private MaterialService materialService;

    @Mock
    private Page<Material> materialPage;

    private List<Material> materialList;

    private User user;

    private Warehouse warehouse;

    private Warehouse warehouse2;

    private District district;

    private Role role;

    private MaterialDto materialDto;

    private Material material;

    Pageable pageable;

    @BeforeEach
    void setUp() {
        materialService = new MaterialService(materialRepository,userCommon,warehouseRepository);
        role = new Role(1L, "ADMIN");
        warehouse = new Warehouse(0L, "name", "address");
        warehouse2 = new Warehouse(3L, "name", "address");
        district = new District(1L, "name", "type", new Province());
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);

        material = new Material(1L,"materialName","kg",null,null,warehouse,user,null);
        materialDto = new MaterialDto(material);
        pageable = PageRequest.of(0, 10);
        materialPage = new PageImpl<>(List.of(material));
    }

    @Test
    void getMaterialByIdAdmin() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(materialRepository.findByMaterialId(1L)).thenReturn(material);
        materialService.getMaterialByIdInWarehouse(1L);
        verify(materialRepository, times(1)).findByMaterialId(1L);
        verify(materialRepository, never()).findByMaterialIdAnhInWarehouse(1L, 0L);
        assertNotNull(materialService.getMaterialByIdInWarehouse(1L));
    }

    @Test
    void getMaterialByIdManager() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(materialRepository.findByMaterialIdAnhInWarehouse(1L, 0L)).thenReturn(material);
        materialService.getMaterialByIdInWarehouse(1L);
        verify(materialRepository, times(1)).findByMaterialIdAnhInWarehouse(1L, 0L);
        verify(materialRepository, never()).findByMaterialId(1L);
        assertNotNull(materialService.getMaterialByIdInWarehouse(1L));
    }

    @Test
    void updateMaterial() {
        when(userCommon.getCurrentUser()).thenReturn(user);
//        when(warehouseRepository.getById(anyLong())).thenReturn(warehouse);
        when(materialRepository.getById(anyLong())).thenReturn(material);
        when(materialRepository.getByMaterialNameAndWarehouseWarehouseID(anyString(),anyLong())).thenReturn(null);
        materialService.updateMaterial(1L,materialDto);
        verify(materialRepository, times(1)).save(any(Material.class));
    }

    @Test
    void saveMaterial() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(warehouseRepository.getById(anyLong())).thenReturn(warehouse);
        when(materialRepository.getByMaterialNameAndWarehouseWarehouseID(anyString(),anyLong())).thenReturn(null);
        materialService.saveMaterial(materialDto);
        verify(materialRepository, times(1)).saveAndFlush(any(Material.class));
    }

    @Test
    void saveMaterialEx() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(warehouseRepository.getById(anyLong())).thenReturn(warehouse);
        when(materialRepository.getByMaterialNameAndWarehouseWarehouseID(anyString(),anyLong())).thenReturn(material);
        AppException exception = assertThrows(AppException.class, () -> materialService.saveMaterial(materialDto));
        assertEquals(exception.getMessage(), "MATERIAL_NAME_EXISTS");
        verify(materialRepository, never()).saveAndFlush(any(Material.class));
    }

    @Test
    void deleteMaterial() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        materialService.deleteMaterial(1L);
        verify(materialRepository, times(1)).deleteMaterialAdmin(1L);
    }

    @Test
    void deleteMaterialEx() {
        user.setRole(new Role(2L, "MANAGER"));
        material.setWarehouse(warehouse2);
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(materialRepository.getById(anyLong())).thenReturn(material);
        AppException exception = assertThrows(AppException.class, () -> materialService.deleteMaterial(1L));
        assertEquals(exception.getMessage(), "CAN_NOT_DELETE_IN_ANOTHER_WAREHOUSE");
        verify(materialRepository,never() ).deleteMaterialAdmin(1L);
    }

    @Test
    void getMaterialInWareHouse() {
        when(materialRepository.findAllByWarehouse(0L)).thenReturn(materialList);
        materialService.getMaterialInWareHouse(0L);
        verify(materialRepository, times(1)).findAllByWarehouse(0L);
    }

    @Test
    void getAllMaterialAdmin() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(materialRepository.filterAllWarehouses(null, null,  pageable)).thenReturn(materialPage);
        Page<Material> materialPage = materialService.getAllMaterial(null, null,pageable);
        verify(materialRepository, times(1)).filterAllWarehouses(null, null,  pageable);
        verify(materialRepository, never()).filterInOneWarehouse(null, null,  pageable);
        assertEquals(materialPage.getSize(), 1);
    }

    @Test
    void getAllMaterialManager() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(materialRepository.filterInOneWarehouse(null, 0L,  pageable)).thenReturn(materialPage);
        Page<Material> materialPage = materialService.getAllMaterial(null, 0L,pageable);
        verify(materialRepository, times(1)).filterInOneWarehouse(null, 0L,  pageable);
        verify(materialRepository, never()).filterAllWarehouses(null, 0L,  pageable);
        assertEquals(materialPage.getSize(), 1);
    }
}