package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.GenerateCode;
import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.MaterialDto;
import com.scms.supplychainmanagementsystem.dto.SupplierDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.MaterialRepository;
import com.scms.supplychainmanagementsystem.repository.SupplierRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SupplierServiceTest {
    @Mock
    private SupplierRepository supplierRepository;
    @Mock
    private  GenerateCode generateCode;

    @Mock
    private UserCommon userCommon;
    @Mock
    private WarehouseRepository warehouseRepository;

    @InjectMocks
    private SupplierService supplierService;

    @Mock
    private Page<Supplier> supplierPage;

    private List<Supplier> suppliers;

    private User user;

    private Warehouse warehouse;

    private Warehouse warehouse2;

    private District district;

    private Role role;

    private SupplierDto supplierDto;

    private Supplier supplier;

    private List<Supplier> supplierList;

    Pageable pageable;

    @BeforeEach
    void setUp() {
        supplierService = new SupplierService(supplierRepository,userCommon,warehouseRepository,generateCode);
        role = new Role(1L, "ADMIN");
        warehouse = new Warehouse(0L, "name", "address");
        warehouse2 = new Warehouse(3L, "name", "address");
        district = new District(1L, "name", "type", new Province());
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);


        supplier = new Supplier(1L,"NCC0001","supplierName","email","0987654321","FPT",true,warehouse,district,user,null);
        supplierDto = new SupplierDto(supplier);
        pageable = PageRequest.of(0, 10);
        supplierPage = new PageImpl<>(List.of(supplier));
        supplierList = List.of(supplier);

    }

    @Test
    void getAllSupplierAdmin() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(supplierRepository.filterAllWarehouses("username","true",0L,pageable)).thenReturn(supplierPage);
        Page<Supplier> customerResponses = supplierService.getAllSupplier("username","true",0L,pageable);
        verify(supplierRepository, times(1)).filterAllWarehouses("username","true",0L,pageable);
        verify(supplierRepository, never()).filterInOneWarehouse("username","true",0L,pageable);
        assertEquals(customerResponses.getSize(), 1);
    }

    @Test
    void getAllSupplierManager() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(supplierRepository.filterInOneWarehouse("username","true",0L,pageable)).thenReturn(supplierPage);
        Page<Supplier> customerResponses = supplierService.getAllSupplier("username","true",0L,pageable);
        verify(supplierRepository, times(1)).filterInOneWarehouse("username","true",0L,pageable);
        verify(supplierRepository, never()).filterAllWarehouses("username","true",0L,pageable);
        assertEquals(customerResponses.getSize(), 1);
    }

    @Test
    void getSupplierInWareHouse() {
        when(supplierRepository.findAllByWarehouse(0L)).thenReturn(supplierList);
        List<Supplier> supplierList = supplierService.getSupplierInWareHouse(0L);
        verify(supplierRepository, times(1)).findAllByWarehouse(0L);
        assertEquals(supplierList.size(), 1);

    }

    @Test
    void getSupplierByIdInWarehouseAdmin() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(supplierRepository.findBySupplierId(1L)).thenReturn(supplier);
        supplierService.getSupplierByIdInWarehouse(1L);
        verify(supplierRepository, times(1)).findBySupplierId(1L);
        verify(supplierRepository, never()).findBySupplierIdAnhInWarehouse(1L, 0L);
        assertNotNull(supplierService.getSupplierByIdInWarehouse(1L));
    }

    @Test
    void getSupplierByIdInWarehouseManager() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(supplierRepository.findBySupplierIdAnhInWarehouse(1L,0L)).thenReturn(supplier);
        supplierService.getSupplierByIdInWarehouse(1L);
        verify(supplierRepository, times(1)).findBySupplierIdAnhInWarehouse(1L,0L);
        verify(supplierRepository, never()).findBySupplierId(1L);
        assertNotNull(supplierService.getSupplierByIdInWarehouse(1L));
    }

    @Test
    void updateSupplier() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(supplierRepository.getById(anyLong())).thenReturn(supplier);
        when(supplierRepository.getBySupplierNameAndWarehouseWarehouseID("supplierName",0L)).thenReturn(null);
        supplierService.updateSupplier(1L,supplierDto);
        verify(supplierRepository, times(1)).save(any(Supplier.class));
    }

    @Test
    void updateSuppliercase() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(supplierRepository.getById(anyLong())).thenReturn(supplier);
        when(supplierRepository.getBySupplierNameAndWarehouseWarehouseID("supplierName",0L)).thenReturn(supplier);
        supplierService.updateSupplier(1L,supplierDto);
        verify(supplierRepository, times(1)).save(any(Supplier.class));
    }

    @Test
    void saveSupplier() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(warehouseRepository.getById(anyLong())).thenReturn(warehouse);
        when(supplierRepository.getBySupplierNameAndWarehouseWarehouseID("supplierName",0L)).thenReturn(null);

        supplierService.saveSupplier(supplierDto);
        verify(supplierRepository, times(2)).saveAndFlush(any(Supplier.class));
    }

    @Test
    void saveSupplierEx() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(warehouseRepository.getById(anyLong())).thenReturn(warehouse);
        when(supplierRepository.getBySupplierNameAndWarehouseWarehouseID("supplierName",0L)).thenReturn(supplier);
        AppException exception = assertThrows(AppException.class, () -> supplierService.saveSupplier(supplierDto));
        assertEquals(exception.getMessage(), "SUPPLIER_NAME_EXISTS");

        verify(supplierRepository, never()).save(any(Supplier.class));
    }

    @Test
    void deleteSupplierAdmin() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        supplierService.deleteSupplier(1L);
        verify(supplierRepository, times(1)).deleteSupplierAdmin(1L);
        verify(supplierRepository, never()).deleteSupplier(1L, 0L);
    }

    @Test
    void deleteSupplierManager() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(supplierRepository.getById(1L)).thenReturn(supplier);
        supplierService.deleteSupplier(1L);
        verify(supplierRepository, times(1)).deleteSupplierAdmin(1L);

    }
}