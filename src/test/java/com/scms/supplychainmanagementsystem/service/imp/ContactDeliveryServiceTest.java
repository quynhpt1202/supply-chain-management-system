package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.ContactDeliveryDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.ContactDeliveryRepository;
import com.scms.supplychainmanagementsystem.repository.CustomerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ContactDeliveryServiceTest {
    @Mock
    private UserCommon userCommon;
    @Mock
    private ContactDeliveryRepository contactDeliveryRepository;
    @Mock
    private CustomerRepository customerRepository;

    @InjectMocks
    private ContactDeliveryService contactDeliveryService;

    @Mock
    private Page<ContactDelivery> contactDeliveryPage;

    private ContactDelivery contactDelivery;

    private User user;

    private Warehouse warehouse;

    private Warehouse warehouse2;

    private District district;

    private Role role;

    private Customer customer;

    private ContactDeliveryDto contactDeliveryDto;

    Pageable pageable;


    @BeforeEach
    void setUp() {

        contactDeliveryService = new ContactDeliveryService(userCommon,contactDeliveryRepository,customerRepository);
        role = new Role(1L, "ADMIN");
        warehouse = new Warehouse(0L, "name", "address");
        warehouse2 = new Warehouse(2L, "name", "address");
        district = new District(1L, "name", "type", new Province());
        pageable = PageRequest.of(0, 10);
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
        customer = new Customer(1L,"KH0001","type","CustomerName",warehouse,"email@gmail.com","0987654321",LocalDate.now(),
                true,"HNN","fb","fpt","note",null,user,null);
        contactDelivery= new ContactDelivery(1L,customer,"contactName","abc@gmail.com","0987654321","FPT",null,district,user);
        contactDeliveryDto = new ContactDeliveryDto(contactDelivery);
    }

    @Test
    void getAllContactDeliveryofcustomer() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(contactDeliveryRepository.filterAllCustomer(1L, null,  pageable)).thenReturn(contactDeliveryPage);
        Page<ContactDelivery> contactDeliveryPage = contactDeliveryService.getAllContactDeliveryofcustomer(1L, null,pageable);
        verify(contactDeliveryRepository, times(1)).filterAllCustomer(1L, null, pageable);
        verify(contactDeliveryRepository, never()).filterInOneCustomer(1L, null, pageable);

    }

    @Test
    void getContactDeliveryByIdAdmin() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(contactDeliveryRepository.findByContactIDAdmin(1L)).thenReturn(contactDelivery);
        contactDeliveryService.getContactDeliveryById(1L);
        verify(contactDeliveryRepository, times(1)).findByContactIDAdmin(1L);
        verify(contactDeliveryRepository, never()).findByContactIDManager(1L);
        assertNotNull(contactDeliveryService.getContactDeliveryById(1L));
    }

    @Test
    void getContactDeliveryByIdManager() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(contactDeliveryRepository.findByContactIDManager(1L)).thenReturn(contactDelivery);
        contactDeliveryService.getContactDeliveryById(1L);
        verify(contactDeliveryRepository, times(1)).findByContactIDManager(1L);
        verify(contactDeliveryRepository, never()).findByContactIDAdmin(1L);
        assertNotNull(contactDeliveryService.getContactDeliveryById(1L));
    }

    @Test
    void updateIContactDeliverySuccess() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.getById((anyLong()))).thenReturn(customer);
        when(contactDeliveryRepository.getById(anyLong())).thenReturn(contactDelivery);
        when(contactDeliveryRepository.getByContactNameAndCustomerId(anyString(),anyLong())).thenReturn(null);
        contactDeliveryService.updateIContactDelivery(1L,contactDeliveryDto);
        verify(contactDeliveryRepository, times(1)).save(any(ContactDelivery.class));
    }

    @Test
    void updateIContactDeliveryEx() {
        contactDeliveryDto.setContactName("differentName");
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.getById((anyLong()))).thenReturn(customer);
        when(contactDeliveryRepository.getById(anyLong())).thenReturn(contactDelivery);
        when(contactDeliveryRepository.getByContactNameAndCustomerId(anyString(),anyLong())).thenReturn(contactDelivery);
        AppException exception = assertThrows(AppException.class, () -> contactDeliveryService.updateIContactDelivery(1L,contactDeliveryDto));
        assertEquals(exception.getMessage(), "CONTACT_EXISTS");
        verify(contactDeliveryRepository,never()).save(any(ContactDelivery.class));
    }

    @Test
    void saveContactDelivery() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.getById(anyLong())).thenReturn(customer);
        when(contactDeliveryRepository.getByContactNameAndCustomerId(anyString(),anyLong())).thenReturn(null);
        contactDeliveryService.saveContactDelivery(contactDeliveryDto);
        verify(contactDeliveryRepository, times(1)).saveAndFlush(any(ContactDelivery.class));
    }

    @Test
    void saveContactDeliveryEx() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.getById(anyLong())).thenReturn(customer);
        when(contactDeliveryRepository.getByContactNameAndCustomerId(anyString(),anyLong())).thenReturn(contactDelivery);
        AppException exception = assertThrows(AppException.class, () -> contactDeliveryService.saveContactDelivery(contactDeliveryDto));
        assertEquals(exception.getMessage(), "CONTACT_EXISTS");
        verify(contactDeliveryRepository, never()).saveAndFlush(any(ContactDelivery.class));
    }

    @Test
    void deleteContactDelivery() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        contactDeliveryService.deleteContactDelivery(1L);
        verify(contactDeliveryRepository, times(1)).deleteContactDeliverie(1L);
    }
}