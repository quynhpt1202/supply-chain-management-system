package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.PurchaseDetailDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.repository.MaterialRepository;
import com.scms.supplychainmanagementsystem.repository.PurchaseDetailRepository;
import com.scms.supplychainmanagementsystem.repository.PurchaseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.never;

@ExtendWith(MockitoExtension.class)
class PurchaseDetailServiceTest {

    @Mock
    private UserCommon mockUserCommon;
    @Mock
    private PurchaseDetailRepository mockPurchaseDetailRepository;
    @Mock
    private MaterialRepository mockMaterialRepository;
    @Mock
    private PurchaseRepository mockPurchaseRepository;

    private PurchaseDetailService purchaseDetailServiceUnderTest;

    private Role role;

    private Warehouse warehouse;

    private User user;

    private District district;

    @BeforeEach
    void setUp() throws Exception {
        purchaseDetailServiceUnderTest = new PurchaseDetailService(mockUserCommon, mockPurchaseDetailRepository, mockMaterialRepository, mockPurchaseRepository);
        role = new Role(1L, "ADMIN");
        district = new District(1L, "name", "type", new Province());
        warehouse = new Warehouse(0L, "name", "address");
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
    }

    @Test
    void testGetAllPurchaseDetailAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseDetailRepository.filterAllWarehouses(...).
        final Page<PurchaseDetails> purchaseDetails = new PageImpl<>(List.of(new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user))));
        when(mockPurchaseDetailRepository.filterAllWarehouses(eq(0L), eq(0L), any(Pageable.class))).thenReturn(purchaseDetails);

//        // Configure PurchaseDetailRepository.filterInOneWarehouse(...).
//        final Page<PurchaseDetails> purchaseDetails1 = new PageImpl<>(List.of(new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"), user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user))));
//        when(mockPurchaseDetailRepository.filterInOneWarehouse(eq(0L), eq(0L), any(Pageable.class))).thenReturn(purchaseDetails1);

        // Run the test
        final Page<PurchaseDetails> result = purchaseDetailServiceUnderTest.getAllPurchaseDetail(0L, 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockPurchaseDetailRepository, times(1)).filterAllWarehouses(eq(0L), eq(0L), any(Pageable.class));
        verify(mockPurchaseDetailRepository, never()).filterInOneWarehouse(eq(0L), eq(0L), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }

    @Test
    void testGetAllPurchaseDetailManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseDetailRepository.filterAllWarehouses(...).
//        final Page<PurchaseDetails> purchaseDetails = new PageImpl<>(List.of(new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user))));
//        when(mockPurchaseDetailRepository.filterAllWarehouses(eq(0L), eq(0L), any(Pageable.class))).thenReturn(purchaseDetails);

//        // Configure PurchaseDetailRepository.filterInOneWarehouse(...).
        final Page<PurchaseDetails> purchaseDetails1 = new PageImpl<>(List.of(new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"), user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user))));
        when(mockPurchaseDetailRepository.filterInOneWarehouse(eq(0L), eq(0L), any(Pageable.class))).thenReturn(purchaseDetails1);

        // Run the test
        final Page<PurchaseDetails> result = purchaseDetailServiceUnderTest.getAllPurchaseDetail(0L, 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockPurchaseDetailRepository, times(1)).filterInOneWarehouse(eq(0L), eq(0L), any(Pageable.class));
        verify(mockPurchaseDetailRepository, never()).filterAllWarehouses(eq(0L), eq(0L), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }



    @Test
    void testGetPurchaseDetailByIdInWarehouseAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseDetailRepository.findByPurchaseDetailId(...).
//        final PurchaseDetails purchaseDetails = new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user));
//        when(mockPurchaseDetailRepository.findByPurchaseDetailId(0L, 0L)).thenReturn(purchaseDetails);

        // Configure PurchaseDetailRepository.findByPurchaseDetailIdAdmin(...).
        final PurchaseDetails purchaseDetails1 = new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user));
        when(mockPurchaseDetailRepository.findByPurchaseDetailIdAdmin(0L)).thenReturn(purchaseDetails1);

        // Run the test
        final PurchaseDetails result = purchaseDetailServiceUnderTest.getPurchaseDetailByIdInWarehouse(0L);

        // Verify the results
        verify(mockPurchaseDetailRepository, times(1)).findByPurchaseDetailIdAdmin(0L);
        verify(mockPurchaseDetailRepository, never()).findByPurchaseDetailId(0L, 0L);
        assertNotNull(purchaseDetailServiceUnderTest.getPurchaseDetailByIdInWarehouse(0L));
    }

    @Test
    void testGetPurchaseDetailByIdInWarehouseManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseDetailRepository.findByPurchaseDetailId(...).
        final PurchaseDetails purchaseDetails = new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user));
        when(mockPurchaseDetailRepository.findByPurchaseDetailId(0L, 0L)).thenReturn(purchaseDetails);

        // Configure PurchaseDetailRepository.findByPurchaseDetailIdAdmin(...).
//        final PurchaseDetails purchaseDetails1 = new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user));
//        when(mockPurchaseDetailRepository.findByPurchaseDetailIdAdmin(0L)).thenReturn(purchaseDetails1);

        // Run the test
        final PurchaseDetails result = purchaseDetailServiceUnderTest.getPurchaseDetailByIdInWarehouse(0L);

        // Verify the results
        verify(mockPurchaseDetailRepository, times(1)).findByPurchaseDetailId(0L, 0L);
        verify(mockPurchaseDetailRepository, never()).findByPurchaseDetailIdAdmin(0L);
        assertNotNull(purchaseDetailServiceUnderTest.getPurchaseDetailByIdInWarehouse(0L));
    }

    @Test
    void testUpdatePurchaseDetail() {
        // Setup
        final PurchaseDetailDto purchaseDetailDto = new PurchaseDetailDto(0L, 0.0, 0.0, 0L, 0L);
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure MaterialRepository.getById(...).
        final Material material = new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockMaterialRepository.getById(0L)).thenReturn(material);

        // Configure PurchaseRepository.getById(...).
        final Purchase purchase = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockPurchaseRepository.getById(0L)).thenReturn(purchase);

        // Configure PurchaseDetailRepository.save(...).
        final PurchaseDetails purchaseDetails = new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), user, user));
        when(mockPurchaseDetailRepository.save(any(PurchaseDetails.class))).thenReturn(purchaseDetails);

        // Run the test
        purchaseDetailServiceUnderTest.updatePurchaseDetail(0L, purchaseDetailDto);

        // Verify the results
        verify(mockPurchaseDetailRepository).save(any(PurchaseDetails.class));
    }

    @Test
    void testSavePurchaseDetail() {
        // Setup
        final PurchaseDetailDto purchaseDetailDto = new PurchaseDetailDto(0L, 0.0, 0.0, 0L, 0L);
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure MaterialRepository.getById(...).
        final Material material = new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockMaterialRepository.getById(0L)).thenReturn(material);

        // Configure PurchaseRepository.getById(...).
        final Purchase purchase = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockPurchaseRepository.getById(0L)).thenReturn(purchase);

        // Configure PurchaseDetailRepository.saveAndFlush(...).
        final PurchaseDetails purchaseDetails = new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user));
        when(mockPurchaseDetailRepository.saveAndFlush(any(PurchaseDetails.class))).thenReturn(purchaseDetails);

        // Run the test
        purchaseDetailServiceUnderTest.savePurchaseDetail(purchaseDetailDto);

        // Verify the results
        verify(mockPurchaseDetailRepository).saveAndFlush(any(PurchaseDetails.class));
    }

    @Test
    void testDeletePurchaseDetailAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseDetailRepository.getById(...).
//        final PurchaseDetails purchaseDetails = new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user));
//        when(mockPurchaseDetailRepository.getById(0L)).thenReturn(purchaseDetails);

        // Run the test
        purchaseDetailServiceUnderTest.deletePurchaseDetail(0L);

        // Verify the results
        verify(mockPurchaseDetailRepository).deletePurchaseAdmin(0L);
    }

    @Test
    void testDeletePurchaseDetailManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseDetailRepository.getById(...).
        final PurchaseDetails purchaseDetails = new PurchaseDetails(0L, 0.0, 0.0, new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user), new Material(0L, "MaterialName", "quantityUnitOfMeasure", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"),  user, user));
        when(mockPurchaseDetailRepository.getById(0L)).thenReturn(purchaseDetails);

        // Run the test
        purchaseDetailServiceUnderTest.deletePurchaseDetail(0L);

        // Verify the results
        verify(mockPurchaseDetailRepository).deletePurchaseAdmin(0L);
    }
}
