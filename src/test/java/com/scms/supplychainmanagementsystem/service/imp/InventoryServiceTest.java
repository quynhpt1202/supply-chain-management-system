package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.InventoryDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InventoryServiceTest {

    @Mock
    private InventoryRepository mockInventoryRepository;
    @Mock
    private UserCommon mockUserCommon;
    @Mock
    private WarehouseRepository mockWarehouseRepository;
    @Mock
    private ProductRepository mockProductRepository;
    @Mock
    private StockService mockStockService;
    @Mock
    private StockRepository mockStockRepository;
    @Mock
    private InvProductStatusRepository mockInvProductStatusRepository;

    private InventoryService inventoryServiceUnderTest;

    private Role role;

    private Warehouse warehouse;

    private User user;

    private District district;

    @BeforeEach
    void setUp() {
        inventoryServiceUnderTest = new InventoryService(mockInventoryRepository, mockUserCommon, mockWarehouseRepository, mockProductRepository, mockStockService, mockStockRepository, mockInvProductStatusRepository);
        role = new Role(1L, "ADMIN");
        district = new District(1L, "name", "type", new Province());
        warehouse = new Warehouse(0L, "name", "address");
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
    }

    @Test
    void testGetAllInventoryAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InventoryRepository.filterAllWarehouses(...).
        final Page<Inventory> inventories = new PageImpl<>(List.of(new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user)));
        when(mockInventoryRepository.filterAllWarehouses(eq(0L), eq("productName"), any(Pageable.class))).thenReturn(inventories);

        // Configure InventoryRepository.filterInOneWarehouse(...).
//        final Page<Inventory> inventories1 = new PageImpl<>(List.of(new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user)));
//        when(mockInventoryRepository.filterInOneWarehouse(eq(0L), eq("productName"), any(Pageable.class))).thenReturn(inventories1);

        // Run the test
        final Page<Inventory> result = inventoryServiceUnderTest.getAllInventory("productName", 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockInventoryRepository, times(1)).filterAllWarehouses(eq(0L), eq("productName"),any(Pageable.class));
        verify(mockInventoryRepository, never()).filterInOneWarehouse(eq(0L), eq("productName"),any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }

    @Test
    void testGetAllInventoryManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InventoryRepository.filterAllWarehouses(...).
//        final Page<Inventory> inventories = new PageImpl<>(List.of(new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user)));
//        when(mockInventoryRepository.filterAllWarehouses(eq(0L), eq("productName"), any(Pageable.class))).thenReturn(inventories);

        // Configure InventoryRepository.filterInOneWarehouse(...).
        final Page<Inventory> inventories1 = new PageImpl<>(List.of(new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user)));
        when(mockInventoryRepository.filterInOneWarehouse(eq(0L), eq("productName"), any(Pageable.class))).thenReturn(inventories1);

        // Run the test
        final Page<Inventory> result = inventoryServiceUnderTest.getAllInventory("productName", 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockInventoryRepository, times(1)).filterInOneWarehouse(eq(0L), eq("productName"),any(Pageable.class));
        verify(mockInventoryRepository, never()).filterAllWarehouses(eq(0L), eq("productName"),any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }


    @Test
    void testGetInventoryByIdInWarehouseAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InventoryRepository.findByinventoryIdAnhInWarehouse(...).
//        final Inventory inventory = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"),user, user);
//        when(mockInventoryRepository.findByinventoryIdAnhInWarehouse(0L, 0L)).thenReturn(inventory);

        // Configure InventoryRepository.getById(...).
        final Inventory inventory1 = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user);
        when(mockInventoryRepository.getById(0L)).thenReturn(inventory1);

        // Run the test
        final Inventory result = inventoryServiceUnderTest.getInventoryByIdInWarehouse(0L);

        // Verify the results
        verify(mockInventoryRepository, times(1)).getById(0L);
        verify(mockInventoryRepository, never()).findByinventoryIdAnhInWarehouse(0L,0L);
        assertNotNull(inventoryServiceUnderTest.getInventoryByIdInWarehouse(0L));
    }

    @Test
    void testGetInventoryByIdInWarehouseManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InventoryRepository.findByinventoryIdAnhInWarehouse(...).
        final Inventory inventory = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"),user, user);
        when(mockInventoryRepository.findByinventoryIdAnhInWarehouse(0L, 0L)).thenReturn(inventory);

        // Configure InventoryRepository.getById(...).
//        final Inventory inventory1 = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user);
//        when(mockInventoryRepository.getById(0L)).thenReturn(inventory1);

        // Run the test
        final Inventory result = inventoryServiceUnderTest.getInventoryByIdInWarehouse(0L);

        // Verify the results
        verify(mockInventoryRepository, times(1)).findByinventoryIdAnhInWarehouse(0L,0L);
        verify(mockInventoryRepository, never()).getById(0L);
        assertNotNull(inventoryServiceUnderTest.getInventoryByIdInWarehouse(0L));
    }

    @Test
    void testUpdateInventory() {
        // Setup
        final InventoryDto inventoryDto = new InventoryDto(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), 0L, 0L, 0L, "createdBy", "lastModifiedBy");
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InventoryRepository.getById(...).
        final Inventory inventory = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user);
        when(mockInventoryRepository.getById(0L)).thenReturn(inventory);

        // Configure StockRepository.findByProduct(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.findByProduct(any(Product.class))).thenReturn(stock);

        // Configure ProductRepository.getById(...).
        final Product product = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user);
        when(mockProductRepository.getById(0L)).thenReturn(product);

        // Configure InvProductStatusRepository.getById(...).
        final InvProductStatus invProductStatus = new InvProductStatus(0L, "prodStatusDescription");
        when(mockInvProductStatusRepository.getById(0L)).thenReturn(invProductStatus);

        // Configure InventoryRepository.save(...).
        final Inventory inventory1 = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user);
        when(mockInventoryRepository.save(any(Inventory.class))).thenReturn(inventory1);

        // Run the test
        inventoryServiceUnderTest.updateInventory(0L, inventoryDto);

        // Verify the results
        verify(mockInventoryRepository,times(1)).save(any(Inventory.class));
        verify(mockStockService,times(1)).updateStockQuantity(0L, -0.0);
    }

    @Test
    void testSaveInventory() {
        // Setup
        final InventoryDto inventoryDto = new InventoryDto(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), 0L, 0L, 0L, "createdBy", "lastModifiedBy");
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockRepository.findByProduct(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.findByProduct(any(Product.class))).thenReturn(stock);

        // Configure ProductRepository.getById(...).
        final Product product = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user);
        when(mockProductRepository.getById(0L)).thenReturn(product);

        // Configure InvProductStatusRepository.getById(...).
        final InvProductStatus invProductStatus = new InvProductStatus(0L, "prodStatusDescription");
        when(mockInvProductStatusRepository.getById(0L)).thenReturn(invProductStatus);

        // Configure InventoryRepository.saveAndFlush(...).
        final Inventory inventory = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user);
        when(mockInventoryRepository.saveAndFlush(any(Inventory.class))).thenReturn(inventory);

        // Run the test
        inventoryServiceUnderTest.saveInventory(inventoryDto);

        // Verify the results
        verify(mockInventoryRepository,times(1)).saveAndFlush(any(Inventory.class));
        verify(mockStockService,times(1)).updateStockQuantity(0L, -0.0);
    }

    @Test
    void testDeleteInventory() {
        // Setup
        // Configure InventoryRepository.getById(...).
        final Inventory inventory = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user);
        when(mockInventoryRepository.getById(0L)).thenReturn(inventory);

        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Run the test
        inventoryServiceUnderTest.deleteInventory(0L);

        // Verify the results
        verify(mockInventoryRepository,never()).deleteinventory(0L, 0L);
        verify(mockStockService,times(1)).updateStockQuantity(0L, 0.0);
        verify(mockInventoryRepository,times(1)).deleteinventoryAdmin(0L);
    }
    @Test
    void testDeleteInventoryManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        // Configure InventoryRepository.getById(...).

        final Inventory inventory = new Inventory(0L, "personCheck", LocalDate.of(2020, 1, 1), "description", 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Warehouse(0L, "warehouseName", "address"), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new InvProductStatus(0L, "prodStatusDescription"), user, user);
        when(mockInventoryRepository.getById(0L)).thenReturn(inventory);

        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Run the test
        inventoryServiceUnderTest.deleteInventory(0L);

        // Verify the results
        verify(mockInventoryRepository,times(1)).deleteinventory(0L, 0L);
        verify(mockStockService,times(1)).updateStockQuantity(0L, 0.0);
        verify(mockInventoryRepository,never()).deleteinventoryAdmin(0L);
    }
}
