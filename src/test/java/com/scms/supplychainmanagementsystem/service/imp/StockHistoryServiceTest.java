package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.StockHistoryDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.repository.ProductRepository;
import com.scms.supplychainmanagementsystem.repository.StockHistoryRepository;
import com.scms.supplychainmanagementsystem.repository.StockRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StockHistoryServiceTest {

    @Mock
    private UserCommon mockUserCommon;
    @Mock
    private StockHistoryRepository mockStockHistoryRepository;
    @Mock
    private StockService mockStockService;
    @Mock
    private StockRepository mockStockRepository;
    @Mock
    private ProductRepository mockProductRepository;
    @Mock
    private WarehouseRepository mockWarehouseRepository;

    private StockHistoryService stockHistoryServiceUnderTest;

    private Role role;

    private Warehouse warehouse;

    private User user;

    private District district;

    @BeforeEach
    void setUp() throws Exception {
        stockHistoryServiceUnderTest = new StockHistoryService(mockUserCommon, mockStockHistoryRepository, mockStockService, mockStockRepository, mockProductRepository, mockWarehouseRepository);
        role = new Role(1L, "ADMIN");
        district = new District(1L, "name", "type", new Province());
        warehouse = new Warehouse(0L, "name", "address");
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
    }

    @Test
    void testGetAllStockHistoryAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockHistoryRepository.filterAllWarehouses(...).
        final Page<StockHistory> stockHistories = new PageImpl<>(List.of(new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user)));
        when(mockStockHistoryRepository.filterAllWarehouses(eq("productName"), eq(0L), any(Pageable.class))).thenReturn(stockHistories);

        // Configure StockHistoryRepository.filterInOneWarehouse(...).
//        final Page<StockHistory> stockHistories1 = new PageImpl<>(List.of(new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user)));
//        when(mockStockHistoryRepository.filterInOneWarehouse(eq("productName"), eq(0L), any(Pageable.class))).thenReturn(stockHistories1);

        // Run the test
        final Page<StockHistory> result = stockHistoryServiceUnderTest.getAllStockHistory("productName", 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockStockHistoryRepository, times(1)).filterAllWarehouses(eq("productName"), eq(0L), any(Pageable.class));
        verify(mockStockHistoryRepository, never()).filterInOneWarehouse(eq("productName"), eq(0L), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }

    @Test
    void testGetAllStockHistoryManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockHistoryRepository.filterAllWarehouses(...).
//        final Page<StockHistory> stockHistories = new PageImpl<>(List.of(new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user)));
//        when(mockStockHistoryRepository.filterAllWarehouses(eq("productName"), eq(0L), any(Pageable.class))).thenReturn(stockHistories);

        // Configure StockHistoryRepository.filterInOneWarehouse(...).
        final Page<StockHistory> stockHistories1 = new PageImpl<>(List.of(new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user)));
        when(mockStockHistoryRepository.filterInOneWarehouse(eq("productName"), eq(0L), any(Pageable.class))).thenReturn(stockHistories1);

        // Run the test
        final Page<StockHistory> result = stockHistoryServiceUnderTest.getAllStockHistory("productName", 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockStockHistoryRepository, times(1)).filterInOneWarehouse(eq("productName"), eq(0L), any(Pageable.class));
        verify(mockStockHistoryRepository, never()).filterAllWarehouses(eq("productName"), eq(0L), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }





    @Test
    void testGetStockHistoryByIdInWarehouseAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockHistoryRepository.findByStockHistoryId(...).
//        final StockHistory stockHistory = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
//        when(mockStockHistoryRepository.findByStockHistoryId(0L, 0L)).thenReturn(stockHistory);

        // Configure StockHistoryRepository.findByStockHistoryIdAdmin(...).
        final StockHistory stockHistory1 = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockStockHistoryRepository.findByStockHistoryIdAdmin(0L)).thenReturn(stockHistory1);

        // Run the test
        final StockHistory result = stockHistoryServiceUnderTest.getStockHistoryByIdInWarehouse(0L);

        // Verify the results
        verify(mockStockHistoryRepository, times(1)).findByStockHistoryIdAdmin(0L);
        verify(mockStockHistoryRepository, never()).findByStockHistoryId(0L, 0L);
        assertNotNull(stockHistoryServiceUnderTest.getStockHistoryByIdInWarehouse(0L));
    }

    @Test
    void testGetStockHistoryByIdInWarehouseManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockHistoryRepository.findByStockHistoryId(...).
        final StockHistory stockHistory = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockStockHistoryRepository.findByStockHistoryId(0L, 0L)).thenReturn(stockHistory);

        // Configure StockHistoryRepository.findByStockHistoryIdAdmin(...).
//        final StockHistory stockHistory1 = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
//        when(mockStockHistoryRepository.findByStockHistoryIdAdmin(0L)).thenReturn(stockHistory1);

        // Run the test
        final StockHistory result = stockHistoryServiceUnderTest.getStockHistoryByIdInWarehouse(0L);

        // Verify the results
        verify(mockStockHistoryRepository, times(1)).findByStockHistoryId(0L, 0L);
        verify(mockStockHistoryRepository, never()).findByStockHistoryIdAdmin(0L);
        assertNotNull(stockHistoryServiceUnderTest.getStockHistoryByIdInWarehouse(0L));
    }

    @Test
    void testUpdateStockHistory() {
        // Setup
        final StockHistoryDto stockHistoryDto = new StockHistoryDto(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), 0L, "createdBy", "lastModifiedBy", 0L);
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockHistoryRepository.getById(...).
        final StockHistory stockHistory = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockStockHistoryRepository.getById(0L)).thenReturn(stockHistory);

        // Configure ProductRepository.getById(...).
        final Product product = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
        when(mockProductRepository.getById(0L)).thenReturn(product);

        // Configure StockRepository.findByProduct(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.findByProduct(any(Product.class))).thenReturn(stock);

        // Configure StockHistoryRepository.findByStockHistoryIdAdmin(...).
//        final StockHistory stockHistory1 = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
//        when(mockStockHistoryRepository.findByStockHistoryIdAdmin(0L)).thenReturn(stockHistory1);

        // Configure StockHistoryRepository.save(...).
        final StockHistory stockHistory2 = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockStockHistoryRepository.save(any(StockHistory.class))).thenReturn(stockHistory2);

        // Run the test
        stockHistoryServiceUnderTest.updateStockHistory(0L, stockHistoryDto);

        // Verify the results
        verify(mockStockHistoryRepository,times(1)).save(any(StockHistory.class));
        verify(mockStockService).updateStockQuantity(0L, 0.0);
    }

    @Test
    void testSaveStockHistory() {
        // Setup
        final StockHistoryDto stockHistoryDto = new StockHistoryDto(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), 0L, "createdBy", "lastModifiedBy", 0L);
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure ProductRepository.getById(...).
        final Product product = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
        when(mockProductRepository.getById(0L)).thenReturn(product);

        // Configure WarehouseRepository.getById(...).
        final Warehouse warehouse = new Warehouse(0L, "warehouseName", "address");
        when(mockWarehouseRepository.getById(0L)).thenReturn(warehouse);

        // Configure StockHistoryRepository.saveAndFlush(...).
        final StockHistory stockHistory = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockStockHistoryRepository.saveAndFlush(any(StockHistory.class))).thenReturn(stockHistory);

        // Run the test
        stockHistoryServiceUnderTest.saveStockHistory(stockHistoryDto);

        // Verify the results
        verify(mockStockHistoryRepository,times(1)).saveAndFlush(any(StockHistory.class));
        verify(mockStockService).updateStockQuantity(0L, 0.0);
    }

    @Test
    void testDeleteStockHistory() {
        // Setup
        // Configure StockHistoryRepository.getById(...).
        final StockHistory stockHistory = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
        when(mockStockHistoryRepository.getById(0L)).thenReturn(stockHistory);

        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockRepository.findByProduct(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.findByProduct(any(Product.class))).thenReturn(stock);

        // Run the test
        stockHistoryServiceUnderTest.deleteStockHistory(0L);

        // Verify the results
        verify(mockStockHistoryRepository,times(1)).deleteStockHistoryAdmin(0L);
        verify(mockStockService).updateStockQuantity(0L, -0.0);
    }

//    @Test
//    void testMapReapExcelDatatoDB() throws Exception {
//        // Setup
//        final MultipartFile reapExcelDataFile = null;
//        when(mockUserCommon.getCurrentUser()).thenReturn(user);
//
//        // Configure ProductRepository.getByProductNameAndWarehouseWarehouseID(...).
//        final Product product = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
//        when(mockProductRepository.getByProductNameAndWarehouseWarehouseID("name", 0L)).thenReturn(product);
//
//        // Configure WarehouseRepository.getById(...).
//        final Warehouse warehouse = new Warehouse(0L, "warehouseName", "address");
//        when(mockWarehouseRepository.getById(0L)).thenReturn(warehouse);
//
//        // Configure StockHistoryRepository.saveAndFlush(...).
//        final StockHistory stockHistory = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
//        when(mockStockHistoryRepository.saveAndFlush(any(StockHistory.class))).thenReturn(stockHistory);
//
//        // Run the test
//        stockHistoryServiceUnderTest.mapReapExcelDatatoDB(reapExcelDataFile, 0L);
//
//        // Verify the results
//        verify(mockStockHistoryRepository).saveAndFlush(any(StockHistory.class));
//        verify(mockStockService).updateStockQuantity(0L, 0.0);
//    }

//    @Test
//    void testMapReapExcelDatatoDB_ThrowsIOException() {
//        // Setup
//        final MultipartFile reapExcelDataFile = null;
//        when(mockUserCommon.getCurrentUser()).thenReturn(user);
//
//        // Configure ProductRepository.getByProductNameAndWarehouseWarehouseID(...).
//        final Product product = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
//        when(mockProductRepository.getByProductNameAndWarehouseWarehouseID("name", 0L)).thenReturn(product);
//
//        // Configure WarehouseRepository.getById(...).
//        final Warehouse warehouse = new Warehouse(0L, "warehouseName", "address");
//        when(mockWarehouseRepository.getById(0L)).thenReturn(warehouse);
//
//        // Configure StockHistoryRepository.saveAndFlush(...).
//        final StockHistory stockHistory = new StockHistory(0L, 0.0, 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), new Warehouse(0L, "warehouseName", "address"),  user, user);
//        when(mockStockHistoryRepository.saveAndFlush(any(StockHistory.class))).thenReturn(stockHistory);
//
//        // Run the test
//        assertThatThrownBy(() -> stockHistoryServiceUnderTest.mapReapExcelDatatoDB(reapExcelDataFile, 0L)).isInstanceOf(IOException.class);
//        verify(mockStockHistoryRepository).saveAndFlush(any(StockHistory.class));
//        verify(mockStockService).updateStockQuantity(0L, 0.0);
//    }


}
