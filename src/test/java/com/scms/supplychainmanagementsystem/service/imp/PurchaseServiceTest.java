package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.PurchaseDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.repository.PurchaseDetailRepository;
import com.scms.supplychainmanagementsystem.repository.PurchaseRepository;
import com.scms.supplychainmanagementsystem.repository.SupplierRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.never;

@ExtendWith(MockitoExtension.class)
class PurchaseServiceTest {

    @Mock
    private UserCommon mockUserCommon;
    @Mock
    private PurchaseRepository mockPurchaseRepository;
    @Mock
    private PurchaseDetailRepository mockPurchaseDetailRepository;
    @Mock
    private WarehouseRepository mockWarehouseRepository;
    @Mock
    private SupplierRepository mockSupplierRepository;

    private PurchaseService purchaseServiceUnderTest;

    private Role role;

    private Warehouse warehouse;

    private User user;

    private District district;

    @BeforeEach
    void setUp() throws Exception {
        purchaseServiceUnderTest = new PurchaseService(mockUserCommon, mockPurchaseRepository, mockPurchaseDetailRepository, mockWarehouseRepository, mockSupplierRepository);
        role = new Role(1L, "ADMIN");
        district = new District(1L, "name", "type", new Province());
        warehouse = new Warehouse(0L, "name", "address");
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
    }

    @Test
    void testGetAllPurchaseAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseRepository.filterAllWarehouses(...).
        final Page<Purchase> purchases = new PageImpl<>(List.of(new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user)));
        when(mockPurchaseRepository.filterAllWarehouses(eq(0L), any(Pageable.class))).thenReturn(purchases);

        // Configure PurchaseRepository.filterInOneWarehouse(...).
//        final Page<Purchase> purchases1 = new PageImpl<>(List.of(new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"),user, user)));
//        when(mockPurchaseRepository.filterInOneWarehouse(eq(0L), any(Pageable.class))).thenReturn(purchases1);

        // Run the test
        final Page<Purchase> result = purchaseServiceUnderTest.getAllPurchase(0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockPurchaseRepository, times(1)).filterAllWarehouses(eq(0L), any(Pageable.class));
        verify(mockPurchaseRepository, never()).filterInOneWarehouse(eq(0L), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }

    @Test
    void testGetAllPurchaseManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseRepository.filterAllWarehouses(...).
//        final Page<Purchase> purchases = new PageImpl<>(List.of(new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user)));
//        when(mockPurchaseRepository.filterAllWarehouses(eq(0L), any(Pageable.class))).thenReturn(purchases);

        // Configure PurchaseRepository.filterInOneWarehouse(...).
        final Page<Purchase> purchases1 = new PageImpl<>(List.of(new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"),user, user)));
        when(mockPurchaseRepository.filterInOneWarehouse(eq(0L), any(Pageable.class))).thenReturn(purchases1);

        // Run the test
        final Page<Purchase> result = purchaseServiceUnderTest.getAllPurchase(0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockPurchaseRepository, times(1)).filterInOneWarehouse(eq(0L), any(Pageable.class));
        verify(mockPurchaseRepository, never()).filterAllWarehouses(eq(0L), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }



    @Test
    void testGetPurchaseByIdInWarehouseAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseRepository.findByPurchaseIdInWarehouse(...).
//        final Purchase purchase = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"),user, user);
//        when(mockPurchaseRepository.findByPurchaseIdInWarehouse(0L, 0L)).thenReturn(purchase);

        // Configure PurchaseRepository.findByPurchaseId(...).
        final Purchase purchase1 = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"),user, user);
        when(mockPurchaseRepository.findByPurchaseId(0L)).thenReturn(purchase1);

        // Run the test
        final Purchase result = purchaseServiceUnderTest.getPurchaseByIdInWarehouse(0L);

        // Verify the results
        verify(mockPurchaseRepository, times(1)).findByPurchaseId(0L);
        verify(mockPurchaseRepository, never()).findByPurchaseIdInWarehouse(0L, 0L);
        assertNotNull(purchaseServiceUnderTest.getPurchaseByIdInWarehouse(0L));
    }

    @Test
    void testGetPurchaseByIdInWarehouseManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseRepository.findByPurchaseIdInWarehouse(...).
        final Purchase purchase = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"),user, user);
        when(mockPurchaseRepository.findByPurchaseIdInWarehouse(0L, 0L)).thenReturn(purchase);

        // Configure PurchaseRepository.findByPurchaseId(...).
//        final Purchase purchase1 = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"),user, user);
//        when(mockPurchaseRepository.findByPurchaseId(0L)).thenReturn(purchase1);

        // Run the test
        final Purchase result = purchaseServiceUnderTest.getPurchaseByIdInWarehouse(0L);

        // Verify the results
        verify(mockPurchaseRepository, times(1)).findByPurchaseIdInWarehouse(0L, 0L);
        verify(mockPurchaseRepository, never()).findByPurchaseId(0L);
        assertNotNull(purchaseServiceUnderTest.getPurchaseByIdInWarehouse(0L));

    }

    @Test
    void testUpdatePurchaseAdmin() {
        // Setup
        final PurchaseDto purchaseDto = new PurchaseDto(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), 0L, 0L, "createdBy", "lastModifiedBy");
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseRepository.getById(...).
        final Purchase purchase = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
        when(mockPurchaseRepository.getById(0L)).thenReturn(purchase);

        // Configure PurchaseRepository.findByPurchaseId(...).
//        final Purchase purchase1 = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
//        when(mockPurchaseRepository.findByPurchaseId(0L)).thenReturn(purchase1);

        // Configure SupplierRepository.getById(...).
        final Supplier supplier = new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user);
        when(mockSupplierRepository.getById(0L)).thenReturn(supplier);

        // Configure PurchaseRepository.save(...).
        final Purchase purchase2 = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
        when(mockPurchaseRepository.save(any(Purchase.class))).thenReturn(purchase2);

        // Run the test
        purchaseServiceUnderTest.updatePurchase(0L, purchaseDto);

        // Verify the results
        verify(mockPurchaseRepository,times(1)).save(any(Purchase.class));
    }

    @Test
    void testUpdatePurchaseManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        final PurchaseDto purchaseDto = new PurchaseDto(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), 0L, 0L, "createdBy", "lastModifiedBy");
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseRepository.getById(...).
        final Purchase purchase = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
        when(mockPurchaseRepository.getById(0L)).thenReturn(purchase);

        // Configure PurchaseRepository.findByPurchaseId(...).
        final Purchase purchase1 = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
        when(mockPurchaseRepository.findByPurchaseId(0L)).thenReturn(purchase1);

        // Configure SupplierRepository.getById(...).
        final Supplier supplier = new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user);
        when(mockSupplierRepository.getById(0L)).thenReturn(supplier);

        // Configure PurchaseRepository.save(...).
        final Purchase purchase2 = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
        when(mockPurchaseRepository.save(any(Purchase.class))).thenReturn(purchase2);

        // Run the test
        purchaseServiceUnderTest.updatePurchase(0L, purchaseDto);

        // Verify the results
        verify(mockPurchaseRepository,times(1)).save(any(Purchase.class));
    }

    @Test
    void testSavePurchase() {
        // Setup
        final PurchaseDto purchaseDto = new PurchaseDto(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), 0L, 0L, "createdBy", "lastModifiedBy");
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure SupplierRepository.getById(...).
        final Supplier supplier = new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User(), new User());
        when(mockSupplierRepository.getById(0L)).thenReturn(supplier);

        // Configure WarehouseRepository.getById(...).
        final Warehouse warehouse = new Warehouse(0L, "warehouseName", "address");
        when(mockWarehouseRepository.getById(0L)).thenReturn(warehouse);

        // Configure PurchaseRepository.saveAndFlush(...).
        final Purchase purchase = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
        when(mockPurchaseRepository.saveAndFlush(any(Purchase.class))).thenReturn(purchase);

        // Run the test
        final Long result = purchaseServiceUnderTest.savePurchase(purchaseDto);

        // Verify the results
        verify(mockPurchaseRepository,times(1)).saveAndFlush(any(Purchase.class));

    }

    @Test
    void testDeletePurchaseAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseRepository.getById(...).
//        final Purchase purchase = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
//        when(mockPurchaseRepository.getById(0L)).thenReturn(purchase);

        // Run the test
        purchaseServiceUnderTest.deletePurchase(0L);

        // Verify the results
        verify(mockPurchaseDetailRepository).deletePurchaseDetailByPurchaseId(0L);
        verify(mockPurchaseRepository).deletePurchaseAdmin(0L);
    }
    @Test
    void testDeletePurchaseManger() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure PurchaseRepository.getById(...).
        final Purchase purchase = new Purchase(0L, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Supplier(0L, "SupplierCode", "SupplierName", "email", "phone", "streetAddress", false, new Warehouse(0L, "warehouseName", "address"), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user, user), new Warehouse(0L, "warehouseName", "address"), user, user);
        when(mockPurchaseRepository.getById(0L)).thenReturn(purchase);

        // Run the test
        purchaseServiceUnderTest.deletePurchase(0L);

        // Verify the results
        verify(mockPurchaseDetailRepository).deletePurchaseDetailByPurchaseId(0L);
        verify(mockPurchaseRepository).deletePurchaseAdmin(0L);
    }
}
