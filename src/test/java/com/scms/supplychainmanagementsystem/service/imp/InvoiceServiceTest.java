package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.GenerateCode;
import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.InvoiceDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.repository.InvoiceRepository;
import com.scms.supplychainmanagementsystem.repository.OrderRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import com.scms.supplychainmanagementsystem.utils.MailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InvoiceServiceTest {

    @Mock
    private UserCommon mockUserCommon;
    @Mock
    private InvoiceRepository mockInvoiceRepository;
    @Mock
    private OrderRepository mockOrderRepository;
    @Mock
    private GenerateCode mockGenerateCode;
    @Mock
    private WarehouseRepository mockWarehouseRepository;
    @Mock
    private MailService mockMailService;

    private InvoiceService invoiceServiceUnderTest;

    private Role role;

    private Warehouse warehouse;

    private User user;

    private District district;

    @BeforeEach
    void setUp() {
        invoiceServiceUnderTest = new InvoiceService(mockUserCommon, mockInvoiceRepository, mockOrderRepository, mockGenerateCode, mockWarehouseRepository, mockMailService);
        role = new Role(1L, "ADMIN");
        district = new District(1L, "name", "type", new Province());
        warehouse = new Warehouse(0L, "name", "address");
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
    }

    @Test
    void testGetAllInvoiceAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InvoiceRepository.filterAllWarehouses(...).
        Page<Invoice> invoices = new PageImpl<>(List.of(new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"), user, user), new Warehouse(0L, "warehouseName", "address"), user)));
        when(mockInvoiceRepository.filterAllWarehouses(eq(0L), eq("invoiceCode"), any(Pageable.class))).thenReturn(invoices);

        // Configure InvoiceRepository.filterInOneWarehouse(...).
//         Page<Invoice> invoices1 = new PageImpl<>(List.of(new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"), user, user), new Warehouse(0L, "warehouseName", "address"), user)));
//        when(mockInvoiceRepository.filterInOneWarehouse(eq(0L), eq("invoiceCode"), any(Pageable.class))).thenReturn(invoices1);

        // Run the test
         Page<Invoice> result = invoiceServiceUnderTest.getAllInvoice("invoiceCode", 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockInvoiceRepository, times(1)).filterAllWarehouses(eq(0L), eq("invoiceCode"), any(Pageable.class));
        verify(mockInvoiceRepository, never()).filterInOneWarehouse(eq(0L), eq("invoiceCode"), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }

    @Test
    void testGetAllInvoiceManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InvoiceRepository.filterAllWarehouses(...).
//        Page<Invoice> invoices = new PageImpl<>(List.of(new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"), user, user), new Warehouse(0L, "warehouseName", "address"), user)));
//        when(mockInvoiceRepository.filterAllWarehouses(eq(0L), eq("invoiceCode"), any(Pageable.class))).thenReturn(invoices);

        // Configure InvoiceRepository.filterInOneWarehouse(...).
         Page<Invoice> invoices1 = new PageImpl<>(List.of(new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), user, user), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), user), new OrderStatus(0L, "status"), user, user), new Warehouse(0L, "warehouseName", "address"), user)));
        when(mockInvoiceRepository.filterInOneWarehouse(eq(0L), eq("invoiceCode"), any(Pageable.class))).thenReturn(invoices1);

        // Run the test
        Page<Invoice> result = invoiceServiceUnderTest.getAllInvoice("invoiceCode", 0L, PageRequest.of(0, 1));

        // Verify the results
        verify(mockInvoiceRepository, times(1)).filterInOneWarehouse(eq(0L), eq("invoiceCode"), any(Pageable.class));
        verify(mockInvoiceRepository, never()).filterAllWarehouses(eq(0L), eq("invoiceCode"), any(Pageable.class));
        assertEquals(result.getSize(), 1);
    }




    @Test
    void testGetInvoiceByIdInWarehouseAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

//        // Configure InvoiceRepository.findByinvoiceIdAnhInWarehouse(...).
//        final Invoice invoice = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
//        when(mockInvoiceRepository.findByinvoiceIdAnhInWarehouse(0L, 0L)).thenReturn(invoice);

        // Configure InvoiceRepository.getById(...).
        final Invoice invoice1 = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
        when(mockInvoiceRepository.getById(0L)).thenReturn(invoice1);

        // Run the test
        final Invoice result = invoiceServiceUnderTest.getInvoiceByIdInWarehouse(0L);

        // Verify the results
        verify(mockInvoiceRepository, times(1)).getById(0L);
        verify(mockInvoiceRepository, never()).findByinvoiceIdAnhInWarehouse(0L, 0L);
        assertNotNull(invoiceServiceUnderTest.getInvoiceByIdInWarehouse(0L));
    }

    @Test
    void testGetInvoiceByIdInWarehouseManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

//        // Configure InvoiceRepository.findByinvoiceIdAnhInWarehouse(...).
        final Invoice invoice = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
        when(mockInvoiceRepository.findByinvoiceIdAnhInWarehouse(0L, 0L)).thenReturn(invoice);

        // Configure InvoiceRepository.getById(...).
//        final Invoice invoice1 = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
//        when(mockInvoiceRepository.getById(0L)).thenReturn(invoice1);

        // Run the test
        final Invoice result = invoiceServiceUnderTest.getInvoiceByIdInWarehouse(0L);

        // Verify the results
        verify(mockInvoiceRepository, times(1)).findByinvoiceIdAnhInWarehouse(0L, 0L);
        verify(mockInvoiceRepository, never()).getById(0L);
        assertNotNull(invoiceServiceUnderTest.getInvoiceByIdInWarehouse(0L));
    }


    @Test
    void testGetInvoiceByOrderIdInWarehouseAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InvoiceRepository.findByOrderIdAnhInWarehouse(...).
//        final Invoice invoice = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
//        when(mockInvoiceRepository.findByOrderIdAnhInWarehouse(0L, 0L)).thenReturn(invoice);

        // Configure InvoiceRepository.findByOrderId(...).
        final Invoice invoice1 = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
        when(mockInvoiceRepository.findByOrderId(0L)).thenReturn(invoice1);

        // Run the test
        final Invoice result = invoiceServiceUnderTest.getInvoiceByOrderIdInWarehouse(0L);

        // Verify the results
        verify(mockInvoiceRepository, times(1)).findByOrderId(0L);
        verify(mockInvoiceRepository, never()).findByOrderIdAnhInWarehouse(0L, 0L);
        assertNotNull(invoiceServiceUnderTest.getInvoiceByOrderIdInWarehouse(0L));
    }

    @Test
    void testGetInvoiceByOrderIdInWarehouseManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InvoiceRepository.findByOrderIdAnhInWarehouse(...).
        final Invoice invoice = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
        when(mockInvoiceRepository.findByOrderIdAnhInWarehouse(0L, 0L)).thenReturn(invoice);

        // Configure InvoiceRepository.findByOrderId(...).
//        final Invoice invoice1 = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
//        when(mockInvoiceRepository.findByOrderId(0L)).thenReturn(invoice1);

        // Run the test
        final Invoice result = invoiceServiceUnderTest.getInvoiceByOrderIdInWarehouse(0L);

        // Verify the results
        verify(mockInvoiceRepository, times(1)).findByOrderIdAnhInWarehouse(0L, 0L);
        verify(mockInvoiceRepository, never()).findByOrderId(0L);
        assertNotNull(invoiceServiceUnderTest.getInvoiceByOrderIdInWarehouse(0L));
    }

    @Test
    void testUpdateInvoice() {
        // Setup
        final InvoiceDto invoiceDto = new InvoiceDto(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), 0L, "lastModifiedBy", 0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure InvoiceRepository.getById(...).
        final Invoice invoice = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
        when(mockInvoiceRepository.getById(0L)).thenReturn(invoice);

        // Configure WarehouseRepository.getById(...).
        final Warehouse warehouse = new Warehouse(0L, "warehouseName", "address");
        when(mockWarehouseRepository.getById(0L)).thenReturn(warehouse);

        // Configure InvoiceRepository.save(...).
        final Invoice invoice1 = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice1);

        // Run the test
        invoiceServiceUnderTest.updateInvoice(0L, invoiceDto);

        // Verify the results
        verify(mockInvoiceRepository).save(any(Invoice.class));
    }

    @Test
    void testSaveInvoice() {
        // Setup
        final InvoiceDto invoiceDto = new InvoiceDto(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), 0L, "lastModifiedBy", 0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure OrderRepository.getById(...).
        final Order order = new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User());
        when(mockOrderRepository.getById(0L)).thenReturn(order);

        // Configure WarehouseRepository.getById(...).
        final Warehouse warehouse = new Warehouse(0L, "warehouseName", "address");
        when(mockWarehouseRepository.getById(0L)).thenReturn(warehouse);

        // Configure InvoiceRepository.save(...).
        final Invoice invoice = new Invoice(0L, "invoiceCode", 0.0, 0.0, LocalDate.of(2020, 1, 1), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User()), new Warehouse(0L, "warehouseName", "address"), new User());
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

//        when(mockGenerateCode.genCodeByDate("prefix", 0L)).thenReturn("result");

        // Run the test
        invoiceServiceUnderTest.saveInvoice(invoiceDto);

        // Verify the results
        verify(mockInvoiceRepository, times(2)).save(any(Invoice.class));
    }
}
