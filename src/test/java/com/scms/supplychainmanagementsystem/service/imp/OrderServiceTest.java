package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.GenerateCode;
import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.*;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.*;
import com.scms.supplychainmanagementsystem.utils.MailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

    @Mock
    private OrderRepository mockOrderRepository;
    @Mock
    private UserCommon mockUserCommon;
    @Mock
    private WarehouseRepository mockWarehouseRepository;
    @Mock
    private ContactDeliveryRepository mockContactDeliveryRepository;
    @Mock
    private OrderStatusRepository mockOrderStatusRepository;
    @Mock
    private OrderDetailsRepository mockOrderDetailsRepository;
    @Mock
    private StockRepository mockStockRepository;
    @Mock
    private GenerateCode mockGenerateCode;
    @Mock
    private InvoiceService mockInvoiceService;
    @Mock
    private InvoiceRepository mockInvoiceRepository;
    @Mock
    private MailService mockMailService;
    @Mock
    private ProductRepository mockProductRepository;
    @Mock
    private PriceBookRepository mockPriceBookRepository;
    @Mock
    private PriceBookEntryRepository mockPriceBookEntryRepository;
    @Mock
    private OrderDetailsService mockOrderDetailsService;
    @Mock
    private OrderDetailSttRepository mockOrderDetailSttRepository;
    @Mock
    private CustomerRepository mockCustomerRepository;

    private OrderService orderServiceUnderTest;

    private Role role;

    private Warehouse warehouse;

    private User user;

    private District district;


    @BeforeEach
    void setUp() throws Exception {
        orderServiceUnderTest = new OrderService(mockOrderRepository, mockUserCommon, mockWarehouseRepository, mockContactDeliveryRepository, mockOrderStatusRepository, mockOrderDetailsRepository, mockStockRepository, mockGenerateCode, mockInvoiceService, mockInvoiceRepository, mockMailService, mockProductRepository, mockPriceBookRepository, mockPriceBookEntryRepository, mockOrderDetailsService, mockOrderDetailSttRepository, mockCustomerRepository);
        role = new Role(1L, "ADMIN");
        district = new District(1L, "name", "type", new Province());
        warehouse = new Warehouse(0L, "name", "address");
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
    }



    @Test
    void testGetAllOrderStatus() {
        // Setup
        when(mockOrderStatusRepository.findAll()).thenReturn(List.of(new OrderStatus(0L, "status")));

        // Run the test
        final List<OrderStatusDto> result = orderServiceUnderTest.getAllOrderStatus();

        // Verify the results
    }

    @Test
    void testGetAllOrderStatus_OrderStatusRepositoryReturnsNoItems() {
        // Setup
        when(mockOrderStatusRepository.findAll()).thenReturn(Collections.emptyList());

        // Run the test
        final List<OrderStatusDto> result = orderServiceUnderTest.getAllOrderStatus();

        // Verify the results
        assertThat(result).isEqualTo(Collections.emptyList());
    }



    @Test
    void testSendOrderMail() {
        // Setup
        final Order order = new Order(0L, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), "note", "orderCode", new Warehouse(0L, "warehouseName", "address"), new ContactDelivery(0L, new Customer(0L, "customerCode", "CustomerType", "customerName", new Warehouse(0L, "warehouseName", "address"), "email", "phone", LocalDate.of(2020, 1, 1), false, "TaxCode", "Facebook", "CompanyName", "Note", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new User(), new User()), "contactName", "email", "phone", "address", LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC), new District(0L, "districtName", "districtType", new Province(0L, "provinceName", "provinceType")), new User()), new OrderStatus(0L, "status"), new User(), new User());
        final List<OrderInformationDto> orderInformationDtoList = List.of(new OrderInformationDto("productName", "category", "unit", "quantity", 0.0, 0.0));

        // Run the test
        orderServiceUnderTest.sendOrderMail(order, orderInformationDtoList, 0.0);

        // Verify the results
        verify(mockMailService).sendMailOrderInformation(any(OrderInformationEmail.class));
    }



    @Test
    void testEx() throws Exception {

        AppException exception = assertThrows(AppException.class, () -> orderServiceUnderTest.ex("loi", 0));
        assertEquals(exception.getMessage(), "ERROR_INPUT_DATA_loi_IN_ROW_0");

    }

    @Test
    void testExDuplicate() throws Exception {
        AppException exception = assertThrows(AppException.class, () -> orderServiceUnderTest.exDuplicate("loi", 0, 0));
        assertEquals(exception.getMessage(), "ERROR_INPUT_DATA : loi_IN_ROW_0_AND_ROW_0");

    }

    @Test
    void testExport() throws Exception {
        // Setup
        final HttpServletResponse response = new MockHttpServletResponse();

        // Run the test
        orderServiceUnderTest.export(response);

        // Verify the results
    }


}
