package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.ProductRequest;
import com.scms.supplychainmanagementsystem.dto.ProductResponse;
import com.scms.supplychainmanagementsystem.dto.ProductResponseDetails;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.CategoryRepository;
import com.scms.supplychainmanagementsystem.repository.ProductRepository;
import com.scms.supplychainmanagementsystem.repository.StockRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository mockProductRepository;
    @Mock
    private UserCommon mockUserCommon;
    @Mock
    private WarehouseRepository mockWarehouseRepository;
    @Mock
    private CategoryRepository mockCategoryRepository;
    @Mock
    private StockRepository mockStockRepository;

    private ProductService productServiceUnderTest;

    private Role role;

    private Warehouse warehouse;

    private User user;

    private District district;

    @BeforeEach
    void setUp() throws Exception {
        productServiceUnderTest = new ProductService(mockProductRepository, mockUserCommon, mockWarehouseRepository, mockCategoryRepository, mockStockRepository);
        role = new Role(1L, "ADMIN");
        district = new District(1L, "name", "type", new Province());
        warehouse = new Warehouse(0L, "name", "address");
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
    }

    @Test
    void testUpdateProduct() {
        // Setup
        final ProductRequest productRequest = new ProductRequest(0L, 0L, "productName", 0L, "quantityUnitOfMeasure", false);

        // Configure ProductRepository.findById(...).
        final Optional<Product> product = Optional.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user));
        when(mockProductRepository.findById(0L)).thenReturn(product);

        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure ProductRepository.getById(...).
        final Product product1 = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
        when(mockProductRepository.getById(0L)).thenReturn(product1);

        // Configure WarehouseRepository.findById(...).
        final Optional<Warehouse> warehouse = Optional.of(new Warehouse(0L, "warehouseName", "address"));
        when(mockWarehouseRepository.findById(0L)).thenReturn(warehouse);

//        when(mockProductRepository.existsProductByProductNameInWarehouse("productName", 0L)).thenReturn(false);

        // Configure CategoryRepository.findById(...).
        final Optional<Category> category = Optional.of(new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")));
        when(mockCategoryRepository.findById(0L)).thenReturn(category);

        // Configure ProductRepository.saveAndFlush(...).
        final Product product2 = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
        when(mockProductRepository.saveAndFlush(any(Product.class))).thenReturn(product2);

        // Run the test
        productServiceUnderTest.updateProduct(productRequest);

        // Verify the results
        verify(mockProductRepository).saveAndFlush(any(Product.class));
    }

    @Test
    void testUpdateProductManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        final ProductRequest productRequest = new ProductRequest(0L, 0L, "productName", 0L, "quantityUnitOfMeasure", false);

        // Configure ProductRepository.findById(...).
        final Optional<Product> product = Optional.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user));
        when(mockProductRepository.findById(0L)).thenReturn(product);

        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure ProductRepository.getById(...).
        final Product product1 = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
        when(mockProductRepository.getById(0L)).thenReturn(product1);

        // Configure WarehouseRepository.findById(...).
//        final Optional<Warehouse> warehouse = Optional.of(new Warehouse(0L, "warehouseName", "address"));
//        when(mockWarehouseRepository.findById(0L)).thenReturn(warehouse);

//        when(mockProductRepository.existsProductByProductNameInWarehouse("productName", 0L)).thenReturn(false);

        // Configure CategoryRepository.findById(...).
        final Optional<Category> category = Optional.of(new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")));
        when(mockCategoryRepository.findById(0L)).thenReturn(category);

        // Configure ProductRepository.saveAndFlush(...).
        final Product product2 = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
        when(mockProductRepository.saveAndFlush(any(Product.class))).thenReturn(product2);

        // Run the test
        productServiceUnderTest.updateProduct(productRequest);

        // Verify the results
        verify(mockProductRepository).saveAndFlush(any(Product.class));
    }



    @Test
    void testCreateProduct() {
        // Setup
        final ProductRequest productRequest = new ProductRequest(0L, 0L, "productName", 0L, "quantityUnitOfMeasure", false);
        when(mockUserCommon.getCurrentUser()).thenReturn(user);
        when(mockProductRepository.existsProductByProductNameInWarehouse("productName", 0L)).thenReturn(false);

        // Configure WarehouseRepository.findById(...).
        final Optional<Warehouse> warehouse = Optional.of(new Warehouse(0L, "warehouseName", "address"));
        when(mockWarehouseRepository.findById(0L)).thenReturn(warehouse);

        // Configure CategoryRepository.findById(...).
        final Optional<Category> category = Optional.of(new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")));
        when(mockCategoryRepository.findById(0L)).thenReturn(category);

        // Configure ProductRepository.saveAndFlush(...).
        final Product product = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
        when(mockProductRepository.saveAndFlush(any(Product.class))).thenReturn(product);

        // Configure StockRepository.saveAndFlush(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.saveAndFlush(any(Stock.class))).thenReturn(stock);

        // Run the test
        productServiceUnderTest.createProduct(productRequest);

        // Verify the results
        verify(mockProductRepository).saveAndFlush(any(Product.class));
        verify(mockStockRepository).saveAndFlush(any(Stock.class));
    }



    @Test
    void testDeleteProductByProductId() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        // Configure ProductRepository.findById(...).
        final Optional<Product> product = Optional.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user));
        when(mockProductRepository.findById(0L)).thenReturn(product);

        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Run the test
        productServiceUnderTest.deleteProductByProductId(0L);

        // Verify the results
        verify(mockStockRepository).deleteByProductId(0L);
        verify(mockProductRepository).deleteById(0L);
    }

    @Test
    void testDeleteProductByProductId_ProductRepositoryFindByIdReturnsAbsent() {
        // Setup
        when(mockProductRepository.findById(0L)).thenReturn(Optional.empty());
//        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Run the test

        AppException exception = assertThrows(AppException.class, () ->  productServiceUnderTest.deleteProductByProductId(0L));
        assertEquals(exception.getMessage(), "PRODUCT_NOT_FOUND");

        // Verify the results
        verify(mockStockRepository,never()).deleteByProductId(0L);
        verify(mockProductRepository,never()).deleteById(0L);
    }

    @Test
    void testGetProductById() {
        // Setup
        // Configure ProductRepository.findById(...).
        final Optional<Product> product = Optional.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user));
        when(mockProductRepository.findById(0L)).thenReturn(product);

        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure StockRepository.findByProduct(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.findByProduct(any(Product.class))).thenReturn(stock);

        // Run the test
        final ProductResponseDetails result = productServiceUnderTest.getProductById(0L);

        // Verify the results
    }



    @Test
    void testGetAllProductsAdmin() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure ProductRepository.filterAllWarehouses(...).
        final Page<Product> products = new PageImpl<>(List.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user)));
        when(mockProductRepository.filterAllWarehouses(eq("productName"), eq(0L), eq(0L), eq(false), any(Pageable.class))).thenReturn(products);

        // Configure ProductRepository.filterInOneWarehouse(...).
//        final Page<Product> products1 = new PageImpl<>(List.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user)));
//        when(mockProductRepository.filterInOneWarehouse(eq("productName"), eq(0L), eq(0L), eq(false), any(Pageable.class))).thenReturn(products1);

        // Configure StockRepository.findByProduct(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), new User(), new User()), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.findByProduct(any(Product.class))).thenReturn(stock);

        // Run the test
        final Page<ProductResponse> result = productServiceUnderTest.getAllProducts("productName", 0L, 0L, false, PageRequest.of(0, 1));

        // Verify the results
    }

    @Test
    void testGetAllProductsManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure ProductRepository.filterAllWarehouses(...).
//        final Page<Product> products = new PageImpl<>(List.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user)));
//        when(mockProductRepository.filterAllWarehouses(eq("productName"), eq(0L), eq(0L), eq(false), any(Pageable.class))).thenReturn(products);

        // Configure ProductRepository.filterInOneWarehouse(...).
        final Page<Product> products1 = new PageImpl<>(List.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user)));
        when(mockProductRepository.filterInOneWarehouse(eq("productName"), eq(0L), eq(0L), eq(false), any(Pageable.class))).thenReturn(products1);

        // Configure StockRepository.findByProduct(...).
        final Stock stock = new Stock(0L, new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), new User(), new User()), 0.0, LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0).toInstant(ZoneOffset.UTC));
        when(mockStockRepository.findByProduct(any(Product.class))).thenReturn(stock);

        // Run the test
        final Page<ProductResponse> result = productServiceUnderTest.getAllProducts("productName", 0L, 0L, false, PageRequest.of(0, 1));

        // Verify the results
    }



    @Test
    void testCheckProductExist() {
        // Setup
        when(mockProductRepository.existsById(0L)).thenReturn(true);

        // Run the test
        final boolean result = productServiceUnderTest.checkProductExist(0L);

        // Verify the results
        assertThat(result).isTrue();
    }

    @Test
    void testCheckProductExistFalse() {
        // Setup
        when(mockProductRepository.existsById(0L)).thenReturn(false);

        // Run the test
        final boolean result = productServiceUnderTest.checkProductExist(0L);


        // Verify the results
        assertThat(result).isFalse();

    }

    @Test
    void testUpdateProductActive() {
        // Setup
        // Configure ProductRepository.findById(...).
        final Optional<Product> product = Optional.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user));
        when(mockProductRepository.findById(0L)).thenReturn(product);

        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure ProductRepository.getById(...).
        final Product product1 = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
        when(mockProductRepository.getById(0L)).thenReturn(product1);

        // Configure ProductRepository.saveAndFlush(...).
        final Product product2 = new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")),  user, user);
        when(mockProductRepository.saveAndFlush(any(Product.class))).thenReturn(product2);

        // Run the test
        productServiceUnderTest.updateProductActive(0L, false);

        // Verify the results
        verify(mockProductRepository).saveAndFlush(any(Product.class));
    }



    @Test
    void testCheckAccessProduct() {
        // Setup
        // Configure ProductRepository.findById(...).
        final Optional<Product> product = Optional.of(new Product(0L, "productName", "quantityUnitOfMeasure", false, new Warehouse(0L, "warehouseName", "address"), new Category(0L, "categoryName", new Warehouse(0L, "warehouseName", "address")), user, user));
        when(mockProductRepository.findById(0L)).thenReturn(product);

        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Run the test
        final boolean result = productServiceUnderTest.checkAccessProduct(0L);

        // Verify the results
        assertThat(result).isTrue();
    }


}
