package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.GenerateCode;
import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.CustomerDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.CustomerRepository;
import com.scms.supplychainmanagementsystem.repository.UserRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import com.scms.supplychainmanagementsystem.validation.MyValidation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {
    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private  UserCommon userCommon;
    @Mock
    private  WarehouseRepository warehouseRepository;
    @Mock
    private MyValidation validation;
    @Mock
    private  GenerateCode generateCode;
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private CustomerServiceImpl customerService;

    @Mock
    private Page<Customer> customersPage;

    private User user;

    private Customer customer;

    private Warehouse warehouse;

    private Warehouse warehouse2;

    private District district;

    private Role role;

    private CustomerDto customerDto;

    Pageable pageable;

    @BeforeEach
    void setUp() {
        customerService = new CustomerServiceImpl(customerRepository, userCommon, warehouseRepository,validation,generateCode);
        role = new Role(1L, "ADMIN");
        warehouse = new Warehouse(0L, "name", "address");
        warehouse2 = new Warehouse(2L, "name", "address");
        district = new District(1L, "name", "type", new Province());
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
        customer = new Customer(1L,"KH0001","type","CustomerName",warehouse,"email@gmail.com","0987654321",LocalDate.now(),
                true,"HNN","fb","fpt","note",null,user,null);

        customerDto = new CustomerDto(customer);
        pageable = PageRequest.of(0, 10);
        customersPage = new PageImpl<>(List.of(customer));
    }

    @Test
    void getCustomerByIdInWarehouseAdmin() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.findByCustomerId(1L)).thenReturn(customer);
        customerService.getCustomerByIdInWarehouse(1L);
        verify(customerRepository, times(1)).findByCustomerId(1L);
        verify(customerRepository, never()).findByCustomerIdAnhInWarehouse(1L, 0L);
        assertNotNull(customerService.getCustomerByIdInWarehouse(1L));
    }

    @Test
    void getCustomerByIdInWarehouseManger() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.findByCustomerIdAnhInWarehouse(1L,0L)).thenReturn(customer);
        customerService.getCustomerByIdInWarehouse(1L);
        verify(customerRepository, times(1)).findByCustomerIdAnhInWarehouse(1L,0L);
        verify(customerRepository, never()).findByCustomerId(1L);
        assertNotNull(customerService.getCustomerByIdInWarehouse(1L));
    }

    @Test
    void updateCustomerSuccess() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.getById((anyLong()))).thenReturn(customer);
        when(customerRepository.existsByEmail(customer.getEmail())).thenReturn(false);
        customerService.updateCustomer(1L,customerDto);
        verify(customerRepository, times(1)).save(any(Customer.class));
    }

//    @Test
//    void updateCustomerException() {
//        when(userCommon.getCurrentUser()).thenReturn(user);
//        when(customerRepository.getById((anyLong()))).thenReturn(customer);
//        when(customerRepository.existsByEmail(customer.getEmail())).thenReturn(true);
//        AppException exception = assertThrows(AppException.class, () -> customerService.updateCustomer(1L, customerDto));
//        assertEquals(exception.getMessage(), "EMAIL_EXISTS");
//        verify(customerRepository, never()).save(any(Customer.class));
//    }

    @Test
    void saveCustomer() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.existsByEmail(anyString())).thenReturn(false);
        when(warehouseRepository.getById(anyLong())).thenReturn(warehouse);
        customerService.saveCustomer(customerDto);
        verify(customerRepository, times(2)).save(any(Customer.class));
    }

    @Test
    void deleteCustomerAdmin() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        customerService.deleteCustomer(1L);
        verify(customerRepository, times(1)).deleteCustomerAdmin(1L);
        verify(customerRepository, never()).deleteCustomer(1L, 0L);

    }

    @Test
    void deleteCustomerManager() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.getById((anyLong()))).thenReturn(customer);
        customerService.deleteCustomer(1L);
        verify(customerRepository, times(1)).deleteCustomerAdmin(1L);
    }

    @Test
    void deleteCustomerEx() {
        user.setRole(new Role(2L, "MANAGER"));
        user.setWarehouse(warehouse2);
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.getById((anyLong()))).thenReturn(customer);
        AppException exception = assertThrows(AppException.class, () -> customerService.deleteCustomer(1L));
        assertEquals(exception.getMessage(), "CAN_NOT_DELETE_IN_ANOTHER_WAREHOUSE");
        verify(customerRepository, never()).deleteCustomerAdmin(1L);
    }

    @Test
    void getAllCustomerAllWh() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.filterAllWarehouses("username", 0L,  pageable)).thenReturn(customersPage);
        Page<Customer> customerResponses = customerService.getAllCustomer("username", 0L,pageable);
        verify(customerRepository, times(1)).filterAllWarehouses("username", 0L, pageable);
        verify(customerRepository, never()).filterInOneWarehouse("username", 0L, pageable);
        assertEquals(customerResponses.getSize(), 1);
    }

    @Test
    void getAllCustomerOneWh() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(customerRepository.filterInOneWarehouse("username", 0L,  pageable)).thenReturn(customersPage);
        customerService.getAllCustomer("username", 0L,pageable);
        verify(customerRepository, times(1)).filterInOneWarehouse("username", 0L, pageable);
        verify(customerRepository, never()).filterAllWarehouses("username", 1L, pageable);

    }


//    @Test
//    void mapReapExcelDatatoDB() {
//    }


}