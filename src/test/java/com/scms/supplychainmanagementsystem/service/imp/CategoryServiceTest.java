package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.CategoryDto;
import com.scms.supplychainmanagementsystem.entity.Category;
import com.scms.supplychainmanagementsystem.entity.Role;
import com.scms.supplychainmanagementsystem.entity.User;
import com.scms.supplychainmanagementsystem.entity.Warehouse;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.CategoryRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CategoryServiceTest {
    Pageable pageable;
    @Mock
    private UserCommon userCommon;
    @Mock
    private CategoryRepository categoryRepository;
    @Mock
    private WarehouseRepository warehouseRepository;
    @InjectMocks
    private CategoryService categoryService;
    private Category category;
    private User user;
    private Warehouse warehouse;
    private CategoryDto categoryDto;
    private CategoryDto categoryDto2;
    @Mock
    private Page<Category> categoryPage;

    @BeforeEach
    void setUp() {
        categoryService = new CategoryService(userCommon, categoryRepository, warehouseRepository);
        warehouse = new Warehouse(1L, "warehouse", "address");
        category = new Category(1L, "category", warehouse);
        user = new User(1L, "username", "password", "email"
                , new Role(1L, "ADMIN"), Warehouse.builder().warehouseID(0L).build(), "firstName", "lastName"
                , true, "phone", LocalDate.now(), null,
                "streetAddress", null, null, null, null);
        categoryDto = new CategoryDto(1L, 1L, "category");
        categoryDto2 = new CategoryDto(2L, 1L, "category2");
        pageable = PageRequest.of(0, 10);
        categoryPage = new PageImpl<>(List.of(category));
    }

    @Test
    void updateCategorySuccess() {
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.ofNullable(category));
        when(userCommon.getCurrentUser()).thenReturn(user);
        categoryService.updateCategory(categoryDto);
        verify(categoryRepository, times(1)).saveAndFlush(category);
    }

    @Test
    void updateCategoryException() {
        user.setRole(new Role(2L, "MANAGER"));
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.ofNullable(category));
        when(userCommon.getCurrentUser()).thenReturn(user);
        AppException exception = assertThrows(AppException.class, () -> categoryService.updateCategory(categoryDto));
        assertEquals(exception.getMessage(), "NOT_ALLOW_ACCESS");
        verify(categoryRepository, never()).saveAndFlush(category);
    }

    @Test
    void updateCategorySuccessWhenUpdateCategoryName() {
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.ofNullable(category));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(categoryRepository.existsCategoriesByProductNameInWarehouse(anyString(), anyLong())).thenReturn(false);
        categoryService.updateCategory(categoryDto2);
        verify(categoryRepository, times(1)).saveAndFlush(category);
    }

    @Test
    void updateCategoryExceptionWhenNameExists() {
        when(categoryRepository.findById(anyLong())).thenReturn(Optional.ofNullable(category));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(categoryRepository.existsCategoriesByProductNameInWarehouse(anyString(), anyLong())).thenReturn(true);
        AppException exception = assertThrows(AppException.class, () -> categoryService.updateCategory(categoryDto2));
        assertEquals(exception.getMessage(), "CATEGORY_NAME_EXISTS");
        verify(categoryRepository, never()).saveAndFlush(category);
    }

    @Test
    void createCategorySuccess() {
        when(warehouseRepository.findById(1L)).thenReturn(Optional.ofNullable(warehouse));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(categoryRepository.existsCategoriesByProductNameInWarehouse(anyString(), anyLong())).thenReturn(false);
        categoryService.createCategory(categoryDto);
        verify(categoryRepository, times(1)).saveAndFlush(any(Category.class));
    }

    @Test
    void deleteCategoryByIdSuccess() {
        when(categoryRepository.findById(1L)).thenReturn(Optional.ofNullable(category));
        when(userCommon.getCurrentUser()).thenReturn(user);
        categoryService.deleteCategoryById(1L);
        verify(categoryRepository, times(1)).deleteById(1L);
    }

    @Test
    void getCategoryByIdSuccess() {
        when(categoryRepository.findById(1L)).thenReturn(Optional.ofNullable(category));
        when(userCommon.getCurrentUser()).thenReturn(user);
        assertNotNull(categoryService.getCategoryById(1L));
    }

    @Test
    void getAllCategoriesInAllWh() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(categoryRepository.filterAllWarehouses(null, null, pageable)).thenReturn(categoryPage);
        Page<Category> categoryPage1 = categoryService.getAllCategories(null, null, pageable);
        verify(categoryRepository, times(1)).filterAllWarehouses(null, null, pageable);
        verify(categoryRepository, never()).filterInOneWarehouse(null, null, pageable);
        assertEquals(categoryPage1.getSize(), 1);
    }

    @Test
    void getAllCategoriesInOneWh() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(categoryRepository.filterInOneWarehouse(null, user.getWarehouse().getWarehouseID(), pageable)).thenReturn(categoryPage);
        categoryService.getAllCategories(null, user.getWarehouse().getWarehouseID(), pageable);
        verify(categoryRepository, never()).filterAllWarehouses(null, 1L, pageable);
        verify(categoryRepository, times(1)).filterInOneWarehouse(null, user.getWarehouse().getWarehouseID(), pageable);
    }
}
