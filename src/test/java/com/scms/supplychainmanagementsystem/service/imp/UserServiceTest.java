package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.ChangePasswordRequest;
import com.scms.supplychainmanagementsystem.dto.UserRequest;
import com.scms.supplychainmanagementsystem.dto.UserResponse;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.RoleRepository;
import com.scms.supplychainmanagementsystem.repository.UserRepository;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private UserCommon userCommon;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private WarehouseRepository warehouseRepository;

    @Mock
    private Page<User> userPage;

    @InjectMocks
    private UserService userService;

    private User user;

    private Role role;

    private Warehouse warehouse;

    private District district;

    private UserRequest userRequest;

    Pageable pageable;

    @BeforeEach
    void setUp() {
        userService = new UserService(userRepository, roleRepository, userCommon, passwordEncoder, warehouseRepository);
        role = new Role(1L, "ADMIN");
        warehouse = new Warehouse(0L, "name", "address");
        district = new District(1L, "name", "type", new Province());
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);

        userRequest = new UserRequest("username1", "email@ex.com"
                , 1L, 1L, "firstName", "lastName"
                , true, "phone", LocalDate.now(), 1L,
                "streetAddress");
        pageable = PageRequest.of(0, 10);
        userPage = new PageImpl<>(List.of(user));
    }

    @Test
    void updateUserSuccess() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(userCommon.checkAccessUserInfoInWarehouse(anyLong())).thenReturn(true);
        when(userRepository.findById(anyLong())).thenReturn(Optional.ofNullable(user));
        when(roleRepository.findById(anyLong())).thenReturn(Optional.ofNullable(role));
        when(warehouseRepository.findById(anyLong())).thenReturn(Optional.ofNullable(warehouse));
        userService.updateUser(userRequest, 1L);
        verify(userRepository, times(1)).saveAndFlush(any(User.class));
    }

    @Test
    void updateUserException() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(userCommon.checkAccessUserInfoInWarehouse(anyLong())).thenReturn(false);
        AppException exception = assertThrows(AppException.class, () -> userService.updateUser(userRequest, 1L));
        assertEquals(exception.getMessage(), "NOT_ALLOW_ACCESS");
        verify(userRepository, never()).saveAndFlush(any(User.class));
    }

    @Test
    void saveUser() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(userRepository.existsByUsername(anyString())).thenReturn(false);
        when(roleRepository.findById(anyLong())).thenReturn(Optional.ofNullable(role));
        when(warehouseRepository.findById(anyLong())).thenReturn(Optional.ofNullable(warehouse));
        userService.saveUser(userRequest);
        verify(userRepository, times(1)).saveAndFlush(any(User.class));
    }

    @Test
    void getUserById() {
        when(userRepository.findById(anyLong())).thenReturn(Optional.ofNullable(user));
        when(userCommon.checkAccessUserInfoInWarehouse(anyLong())).thenReturn(true);
        userService.getUserById(1L);
        assertNotNull(userService.getUserById(1L));
    }

    @Test
    void changePasswordSuccess() {
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest("password", "newPw");
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(passwordEncoder.encode(anyString())).thenReturn("newPw");
        userService.changePassword(changePasswordRequest);
        verify(userRepository, times(1)).save(user);

    }

    @Test
    void changePasswordException() {
        ChangePasswordRequest changePasswordRequest = new ChangePasswordRequest("password", "newPw");
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);
        AppException exception = assertThrows(AppException.class, () -> userService.changePassword(changePasswordRequest));
        assertEquals(exception.getMessage(), "INVALID_CURRENT_PASSWORD");
        verify(userRepository, never()).save(user);

    }


    @Test
    void deleteUser() {
        when(userCommon.checkAccessUserInfoInWarehouse(anyLong())).thenReturn(true);
        userService.deleteUser(1L);
        verify(userRepository, times(1)).deleteById(1L);
    }

    @Test
    void deleteUserException() {
        when(userCommon.checkAccessUserInfoInWarehouse(anyLong())).thenReturn(false);
        AppException exception = assertThrows(AppException.class, () -> userService.deleteUser(1L));
        assertEquals(exception.getMessage(), "NOT_ALLOW_ACCESS");
        verify(userRepository, never()).deleteById(1L);
    }

    @Test
    void getAllRoles() {
        when(roleRepository.findAll()).thenReturn(List.of(role));
        assertEquals(1, userService.getAllRoles().size());
    }

    @Test
    void getAllUsersAllWh() {
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(userRepository.filterAllWarehouses("username", 1L, 0L, 1L, pageable)).thenReturn(userPage);
        Page<UserResponse> userResponses = userService.getAllUsers("username", 1L, 0L, pageable);
        verify(userRepository, times(1)).filterAllWarehouses("username", 1L, 0L, 1L, pageable);
        verify(userRepository, never()).filterInOneWarehouse("username", 1L, 0L, 1L, pageable);
        assertEquals(userResponses.getSize(), 1);
    }

    @Test
    void getAllUsersOneWh() {
        user.setRole(new Role(2L, "MANAGER"));
        when(userCommon.getCurrentUser()).thenReturn(user);
        when(userRepository.filterInOneWarehouse("username", 1L, 0L, 1L, pageable)).thenReturn(userPage);
        userService.getAllUsers("username", 1L, 0L, pageable);
        verify(userRepository, times(1)).filterInOneWarehouse("username", 1L, 0L, 1L, pageable);
        verify(userRepository, never()).filterAllWarehouses("username", 1L, 0L, 1L, pageable);
    }

    @Test
    void updateUserActive() {
        when(userCommon.checkAccessUserInfoInWarehouse(anyLong())).thenReturn(true);
        when(userRepository.findById(anyLong())).thenReturn(Optional.ofNullable(user));
        userService.updateUserActive(1L, true);
        verify(userRepository, times(1)).saveAndFlush(user);
    }

    @Test
    void updateUserActiveException() {
        when(userCommon.checkAccessUserInfoInWarehouse(anyLong())).thenReturn(false);
        AppException exception = assertThrows(AppException.class, () -> userService.updateUserActive(1L, true));
        assertEquals(exception.getMessage(), "NOT_ALLOW_ACCESS");
        verify(userRepository, never()).saveAndFlush(user);
    }
}
