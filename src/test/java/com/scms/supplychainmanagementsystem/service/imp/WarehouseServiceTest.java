package com.scms.supplychainmanagementsystem.service.imp;

import com.scms.supplychainmanagementsystem.common.UserCommon;
import com.scms.supplychainmanagementsystem.dto.WarehouseDto;
import com.scms.supplychainmanagementsystem.entity.*;
import com.scms.supplychainmanagementsystem.exception.AppException;
import com.scms.supplychainmanagementsystem.repository.WarehouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WarehouseServiceTest {

    @Mock
    private UserCommon mockUserCommon;
    @Mock
    private WarehouseRepository mockWarehouseRepository;

    private WarehouseService warehouseServiceUnderTest;

    private Role role;

    private Warehouse warehouse;

    private User user;

    private District district;

    @BeforeEach
    void setUp() {
        warehouseServiceUnderTest = new WarehouseService(mockUserCommon, mockWarehouseRepository);
        role = new Role(1L, "ADMIN");
        district = new District(1L, "name", "type", new Province());
        warehouse = new Warehouse(0L, "name", "address");
        user = new User(1L, "username", "password", "email"
                , role, warehouse, "firstName", "lastName"
                , true, "phone", LocalDate.now(), district,
                "streetAddress", null, null, null, null);
    }

    @Test
    void testGetAllWarehouse() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);
        // Configure WarehouseRepository.filterAll(...).
         Page<Warehouse> warehouses = new PageImpl<>(List.of(new Warehouse(0L, "warehouseName", "address")));
        when(mockWarehouseRepository.filterAll(eq("warehouseName"), eq("address"), any(Pageable.class))).thenReturn(warehouses);
        // Run the test
        Page<Warehouse> warehouses1= warehouseServiceUnderTest.getAllWarehouse("warehouseName", "address", PageRequest.of(0, 1));
        // Verify the results
    }

    @Test
    void testGetAllWarehouse_WarehouseRepositoryManager() {
        // Setup
        user.setRole(new Role(2L, "MANAGER"));
        when(mockUserCommon.getCurrentUser()).thenReturn(user);
        // Run the test
        AppException exception = assertThrows(AppException.class, () -> warehouseServiceUnderTest.getAllWarehouse("warehouseName", "address", PageRequest.of(0, 1)));
        assertEquals(exception.getMessage(), "YOU_ARE_NOT_AN_ADMIN");
        // Verify the results
        verify(mockWarehouseRepository, never()).deleteById(0L);
    }

    @Test
    void testUpdateWarehouse() {
        // Setup
        final WarehouseDto warehouseDto = new WarehouseDto(0L, "warehouseName", "address");
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure WarehouseRepository.getById(...).
        final Warehouse warehouse = new Warehouse(0L, "warehouseName", "address");
        when(mockWarehouseRepository.getById(0L)).thenReturn(warehouse);

        // Configure WarehouseRepository.getByWarehouseName(...).
        final Warehouse warehouse1 = new Warehouse(0L, "warehouseName", "address");
        when(mockWarehouseRepository.getByWarehouseName("warehouseName")).thenReturn(warehouse1);

        // Configure WarehouseRepository.save(...).
        final Warehouse warehouse2 = new Warehouse(0L, "warehouseName", "address");
        when(mockWarehouseRepository.save(any(Warehouse.class))).thenReturn(warehouse2);

        // Run the test
        warehouseServiceUnderTest.updateWarehouse(0L, warehouseDto);

        // Verify the results

        verify(mockWarehouseRepository, times(1)).save(any(Warehouse.class));
    }

    @Test
    void testSaveWarehouse() {
        // Setup
        final WarehouseDto warehouseDto = new WarehouseDto(1L, "warehouseName", "address");
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure WarehouseRepository.getByWarehouseName(...).
        final Warehouse warehouse = new Warehouse(0L, "warehouseName", "address");
        when(mockWarehouseRepository.getByWarehouseName("warehouseName")).thenReturn(null);

        // Configure WarehouseRepository.saveAndFlush(...).
        final Warehouse warehouse1 = new Warehouse(2L, "warehouseName", "address");
        when(mockWarehouseRepository.saveAndFlush(any(Warehouse.class))).thenReturn(warehouse1);

        // Run the test
        warehouseServiceUnderTest.saveWarehouse(warehouseDto);

        // Verify the results

        verify(mockWarehouseRepository, times(1)).saveAndFlush(any(Warehouse.class));
    }

    @Test
    void testDeleteWarehouse() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Run the test
        warehouseServiceUnderTest.deleteWarehouse(0L);

        // Verify the results
        verify(mockWarehouseRepository).deleteById(0L);

        verify(mockWarehouseRepository, times(1)).deleteById(0L);

    }

    @Test
    void testGetWarehouseById() {
        // Setup
        when(mockUserCommon.getCurrentUser()).thenReturn(user);

        // Configure WarehouseRepository.getById(...).
        final Warehouse warehouse = new Warehouse(0L, "warehouseName", "address");
        when(mockWarehouseRepository.getById(0L)).thenReturn(warehouse);

        // Run the test
        final Warehouse result = warehouseServiceUnderTest.getWarehouseById(0L);

        // Verify the results
        verify(mockWarehouseRepository, times(1)).getById(0L);
//        assertNotNull(warehouseService.g(1L));
    }
}
